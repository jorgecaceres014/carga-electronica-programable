/*
 * Implementacion.h
 *
 *  Created on: Jan 4, 2021
 *      Author: Jorge
 */

#ifndef INC_IMPLEMENTACION_H_
#define INC_IMPLEMENTACION_H_
#include<stdbool.h>
#include "stm32f1xx_hal.h"
#include "main.h"
#include "lcd20x4.h"
#include "Teclado4x4.h"
#include <stdlib.h>
#include"usbd_cdc_if.h"
#include<stdint.h>

#define usbD_Pin GPIO_PIN_12
#define usbD_GPIO_Port GPIOA

#define ADC_RESOLUCION_BITS 16
#define ADC_REF_MV 3000
#define ADC_LSB 30000			// se escala por 2 a la potencia de 16 (en mV)
#define ESCALA 16
#define ADC_MUESTRAS_BITS 8
#define ADC_SAMPLE_DELETION 30	//cantidad de muestras del ADC a omitir para el calculo del promedio



typedef enum{
	UNIDAD,
	DECENA,
	CENTENA,
	UNIDAD_MIL
}digit;

typedef enum {
	SYSTEM_INIT,
	LCD_HOME_TEMPLATE,
	LCD_TEMPLATE,
	ADC_READ_CURRENT,
	ADC_READ_VOLTAGE,
	LCD_UPDATE,
	DAC_SET_CURRENT,
	KEYPAD_DECODE,
	CALC_ISET,
	EDIT
}state;

typedef enum {
	TRAN,
	CC
}mode;

extern USBD_HandleTypeDef hUsbDeviceFS;
extern volatile digit digito;

extern uint16_t ADC_MUESTRAS;
uint8_t unidad;
uint8_t decena;
uint8_t centena;
uint8_t unidad_mil;

uint16_t leerADC(uint8_t ch);
void homeDisplay(void);
void systemInit(void);
void ccDisplay(void);
void tranDisplay(void);
void loadControl(bool);
void setDac(uint16_t cor);
uint16_t fixedPoint (uint16_t val);
uint8_t CDC_FreeToTransmit();
void usbSendInt(uint16_t num);
void usbSendString(char *msg);
void usbReset();
void updateMeas(uint16_t iRead, uint8_t modo);
void editKeysHandle(char tecla, mode modo);
void cursorMove(mode modo, bool isNumber, bool isEncoder);
void digitCursorInc (digit);
void digitCursorDec (digit);
void sort (uint16_t* data, uint16_t lenght);
uint16_t promedioN_X(uint16_t N , uint16_t X);
uint16_t mediaMovil (uint16_t *valores);





#endif /* INC_IMPLEMENTACION_H_ */
