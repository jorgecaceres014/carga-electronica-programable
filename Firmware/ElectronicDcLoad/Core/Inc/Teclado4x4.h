/*
 * Teclado4x4.h
 *
 *  Created on: Oct 4, 2020
 *      Author: Jorge Cáceres
 */

#ifndef INC_TECLADO4X4_H_
#define INC_TECLADO4X4_H_

#include "stm32f1xx_hal.h"

uint8_t tecladoScan();
char tecladoDeco(uint8_t ordenTecla);



#endif /* INC_TECLADO4X4_H_ */
