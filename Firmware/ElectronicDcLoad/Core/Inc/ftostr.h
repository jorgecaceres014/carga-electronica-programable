

#ifndef FLOATTOSTRING_H
#define	FLOATTOSTRING_H

#include <stdint.h>


uint8_t Word2Dec(char* dest, uint16_t a,uint8_t left);
uint8_t int2frac(uint16_t frac, unsigned char ndec, char* dest);
uint8_t float2str(char* dest, float x, unsigned short dec);

// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>Function prototype:</b></p>
  
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* FLOATTOSTRING_H */

