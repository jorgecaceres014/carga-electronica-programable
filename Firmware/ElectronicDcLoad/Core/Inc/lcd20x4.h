/*
 * lcd1604.h
 *
 *  Created on: Sep 30, 2020
 *      Author: Jorge Cáceres
 */

#ifndef INC_LCD20X4_H_
#define INC_LCD20X4_H_

#include "stdint.h"

void lcdInit (void);   // incicializa lcd
void lcdCmd (char cmd);  // envia comando al lcd
void lcdDato (char data);  // envia dato al lcd
void lcdString (char *str);  // envia cadena de caracteres al lcd
void lcdCursor(uint8_t linea, uint8_t caracter);  // coloca el cursor en la posicion indicada fil (0-3), col (0-20);
void lcdClear (void);
void lcdEscribeFloat(long numero);
void lcdEscribeInt(uint32_t numero);
void lcdCaracter(char caracter);
void lcdLinea4();
void lcdLinea3();
void lcdLinea2();
void lcdLinea1();
void lcdHome();
void lcdON();
void lcdCursorIncr();
void lcdCursorON();
void lcdCursorOFF();
void lcdOff();



#endif /* INC_LCD20X4_H_ */
