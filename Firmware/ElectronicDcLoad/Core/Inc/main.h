/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ledHeartBeat_Pin GPIO_PIN_13
#define ledHeartBeat_GPIO_Port GPIOC
#define OCP_Pin GPIO_PIN_15
#define OCP_GPIO_Port GPIOC
#define temp_Pin GPIO_PIN_0
#define temp_GPIO_Port GPIOA
#define Fan_Pin GPIO_PIN_1
#define Fan_GPIO_Port GPIOA
#define ADC_CONVST_Pin GPIO_PIN_2
#define ADC_CONVST_GPIO_Port GPIOA
#define DAC_CS_Pin GPIO_PIN_3
#define DAC_CS_GPIO_Port GPIOA
#define ADC_EOC_Pin GPIO_PIN_4
#define ADC_EOC_GPIO_Port GPIOA
#define ADC_CS_Pin GPIO_PIN_0
#define ADC_CS_GPIO_Port GPIOB
#define ENC_CH_A_Pin GPIO_PIN_1
#define ENC_CH_A_GPIO_Port GPIOB
#define ENC_CH_A_EXTI_IRQn EXTI1_IRQn
#define RVP_Pin GPIO_PIN_2
#define RVP_GPIO_Port GPIOB
#define ENC_CH_B_Pin GPIO_PIN_10
#define ENC_CH_B_GPIO_Port GPIOB
#define ENC_BUTTON_Pin GPIO_PIN_11
#define ENC_BUTTON_GPIO_Port GPIOB
#define ENC_BUTTON_EXTI_IRQn EXTI15_10_IRQn
#define LoadOnOff_Pin GPIO_PIN_12
#define LoadOnOff_GPIO_Port GPIOB
#define RS_Pin GPIO_PIN_13
#define RS_GPIO_Port GPIOB
#define E_Pin GPIO_PIN_14
#define E_GPIO_Port GPIOB
#define D4_Pin GPIO_PIN_15
#define D4_GPIO_Port GPIOB
#define D5_Pin GPIO_PIN_8
#define D5_GPIO_Port GPIOA
#define D6_Pin GPIO_PIN_9
#define D6_GPIO_Port GPIOA
#define D7_Pin GPIO_PIN_10
#define D7_GPIO_Port GPIOA
#define KP_C1_Pin GPIO_PIN_15
#define KP_C1_GPIO_Port GPIOA
#define KP_C1_EXTI_IRQn EXTI15_10_IRQn
#define KP_C2_Pin GPIO_PIN_3
#define KP_C2_GPIO_Port GPIOB
#define KP_C2_EXTI_IRQn EXTI3_IRQn
#define KP_C3_Pin GPIO_PIN_4
#define KP_C3_GPIO_Port GPIOB
#define KP_C3_EXTI_IRQn EXTI4_IRQn
#define KP_C4_Pin GPIO_PIN_5
#define KP_C4_GPIO_Port GPIOB
#define KP_C4_EXTI_IRQn EXTI9_5_IRQn
#define KP_F1_Pin GPIO_PIN_6
#define KP_F1_GPIO_Port GPIOB
#define KP_F2_Pin GPIO_PIN_7
#define KP_F2_GPIO_Port GPIOB
#define KP_F3_Pin GPIO_PIN_8
#define KP_F3_GPIO_Port GPIOB
#define KP_F4_Pin GPIO_PIN_9
#define KP_F4_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
