/*
 * Implementacion.c
 *
 *  Created on: Jan 4, 2021
 *      Author: Jorge Cáceres
 */

#include "Implementacion.h"

extern uint16_t contadorEncoder;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern SPI_HandleTypeDef hspi1;
extern char tecla;
extern volatile bool isEncoder;
extern volatile bool incEncoder;
extern mode modo;


void setDac(uint16_t cor){
	HAL_GPIO_WritePin(DAC_CS_GPIO_Port, DAC_CS_Pin, 0);
	HAL_SPI_Transmit(&hspi1, (uint16_t *)&cor, 1, 100);
	HAL_GPIO_WritePin(DAC_CS_GPIO_Port, DAC_CS_Pin, 1);
	return;
}

uint16_t leerADC(uint8_t ch){
	  uint16_t spiDato [20];			// buffer para el SPI_1
	  const uint16_t ADC_canal0 = 0b0000000000000000;
	  const uint16_t ADC_canal1 = 0b0001000000000000;
	  const uint16_t ADC_leer = 0b1101000000000000;
	  extern SPI_HandleTypeDef hspi1;

	  if (ch == 0){
		  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin,0);
		  HAL_SPI_Transmit(&hspi1, (uint16_t *)&ADC_canal0, 1, 100);
		  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, 1);
	  }
	  if (ch == 1){
		  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin,0);
		  HAL_SPI_Transmit(&hspi1, (uint16_t *)&ADC_canal1, 1, 100);
		  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, 1);
	  }
	  HAL_GPIO_WritePin(ADC_CONVST_GPIO_Port, ADC_CONVST_Pin, 0);
	  asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");
	  HAL_GPIO_WritePin(ADC_CONVST_GPIO_Port, ADC_CONVST_Pin, 1);
	  //while(HAL_GPIO_ReadPin(ADC_EOC_GPIO_Port, ADC_EOC_Pin) == 0){}

	  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, 0);
	  HAL_SPI_TransmitReceive(&hspi1, (uint16_t *)&ADC_leer, spiDato, 1, 100);
	  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, 1);
	  return spiDato[0];
 }


void systemInit(){
	uint16_t DAC_CERO = 0b0000000000000000;
	HAL_GPIO_WritePin(DAC_CS_GPIO_Port, DAC_CS_Pin, 1);
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, 1);
	HAL_GPIO_WritePin(LoadOnOff_GPIO_Port, LoadOnOff_Pin, 0);
	HAL_GPIO_WritePin(ADC_CONVST_GPIO_Port, ADC_CONVST_Pin, 1);
	/* Configuracion del ADC*/
	const uint16_t ADC_bitCofig = 0b1110011111111101;
/*	const uint16_t ADC_canal0 = 0b0000000000000000;
	const uint16_t ADC_canal1 = 0b0001000000000000;
	const uint16_t ADC_leer = 0b1101000000000000;
	const uint16_t ADC_leerConfig = 0b1100000000000000;*/
	HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, 0);					// selecciona el ADC para comunicacion SPI
	HAL_SPI_Transmit(&hspi1, (uint16_t *)&ADC_bitCofig, 1, 100);		// se configura ADC

	HAL_GPIO_WritePin(DAC_CS_GPIO_Port, DAC_CS_Pin, 0);
	HAL_SPI_Transmit(&hspi1, (uint16_t *)&DAC_CERO, 1, 100);
	HAL_GPIO_WritePin(DAC_CS_GPIO_Port, DAC_CS_Pin, 1);
	HAL_TIM_Base_Start(&htim1);											//timer para el lcd y retardo de us.
	lcdInit();
	return;
}

void homeDisplay(){
	lcdClear();
	lcdString("Electronic DC Load");
	lcdCursor(3, 11);
	lcdString("Load: OFF");
	lcdLinea4();
	lcdString("Mode: ");
	return;
}

void ccDisplay(){
	lcdClear();
	lcdString("Electronic DC Load");
	lcdCursor(1, 0);
	lcdString("Iset:      mA");
	lcdLinea3();
	lcdString("Iread:     . mA");
	lcdCursor(3, 11);
	lcdString("Load: OFF");
	lcdLinea4();
	lcdString("Mode: CC");
	return;

}
void tranDisplay(){
	lcdClear();
	lcdString("Electronic DC Load");
	lcdLinea2();
	lcdString("Ilow:     . mA");
	lcdLinea3();
	lcdString("Ihigh:     . mA");
	lcdLinea4();
	lcdString("Mode: TRAN");
	lcdCursor(3, 11);
	lcdString("Load: OFF");
	lcdCursor(1, 14);
	lcdString("PW:   ");
	lcdCursor(2, 15);
	lcdString("f:  ");
	return;
}

void batDisplay(){
	lcdClear();
	lcdString("Electronic DC Load");
	lcdLinea2();
	lcdString("Iset:      mA");
	lcdLinea3();
	lcdString("Cap:   mAH");
	lcdLinea4();
	lcdString("Mode: TRAN");
	lcdCursor(3, 11);
	lcdString("Load: OFF");
}

void updateMeas(uint16_t iRead, uint8_t modo){
	uint16_t entera = 0;
	uint8_t decimal = 0;

	switch (modo){

	case CC:
		//iRead = fixedPoint(iRead);
		entera = iRead/10;
		decimal = (10*((double)iRead/10.0f - (int)iRead/10));
		lcdCursor(2, 7);
		lcdString("      ");
		lcdCursor(2, 7);
		lcdEscribeInt(entera);
		lcdCaracter('.');
		lcdEscribeInt(decimal);
		/*usbSendString("Iread: ");
		usbSendInt(entera);
		usbSendString(".");
		usbSendInt(decimal);
		usbSendString("\n\r");*/
		return;
		break;

	case TRAN:
		break;

	}
}

void loadControl(bool loadOn){
	if (loadOn){
		HAL_GPIO_WritePin(LoadOnOff_GPIO_Port, LoadOnOff_Pin, 1);
		lcdCursor(3, 17);
		lcdString("ON ");
		HAL_GPIO_WritePin(Fan_GPIO_Port, Fan_Pin, 1);
	}else{
		HAL_GPIO_WritePin(LoadOnOff_GPIO_Port, LoadOnOff_Pin, 0);
		lcdCursor(3, 17);
		lcdString("OFF");
		HAL_GPIO_WritePin(Fan_GPIO_Port, Fan_Pin, 0);
	}
	return;
}

uint16_t fixedPoint (uint16_t val){
	uint32_t p = 0;
	p = ((val*ADC_LSB)>>ESCALA);
	return (uint16_t)p;
}

void usbReset(){
	/* GPIO Ports Clock Enable */
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOA_CLK_ENABLE();
	HAL_GPIO_WritePin(usbD_GPIO_Port, usbD_Pin, 0);
	/*Configure GPIO pin : usbD_Pin */
	GPIO_InitStruct.Pin = usbD_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(usbD_GPIO_Port, &GPIO_InitStruct);
}

uint8_t CDC_FreeToTransmit()
{
	USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;

	if (hUsbDeviceFS.dev_state != USBD_STATE_CONFIGURED){
		// el dispositivo aun no fue configurado
		return 0;
	}
	if (hcdc->TxState != 0) {
		// ocupado, no esta listo para transmitir
		return 1;
	}
		//dispositivo listo para transmitir
	return 2;
}

void usbSendInt(uint16_t num){
	char buffer[20];
	itoa(num, buffer, 10);
	usbSendString(buffer);
	return;
}

void usbSendString( char *st){
	uint8_t tam = 0;
	int8_t *pos = st;
	switch (CDC_FreeToTransmit()){
	case 0:								// USB no configurado salir sin enviar
		return;
		break;
	case 1:								// USB no esta listo para transmitir
		while(CDC_FreeToTransmit() == 1){}
		break;
	case 2:								// USB listo para transmitir
		break;
	}

	while (*pos){
		tam++;
		pos++;
	}
	CDC_Transmit_FS(st, tam);
	//free(*msg);
	return;
}


void editKeysHandle(char tecla, mode modo){
	bool isNumber = false;
	lcdCursorON();
	switch (tecla){
	case '#':			//flecha derecha
		digitCursorDec(digito);
		cursorMove(modo, isNumber, isEncoder);
		break;

	case '*':			//flecha izquierda
		digitCursorInc(digito);
		cursorMove(modo, isNumber, isEncoder);
		break;
	case 'C':
		return;
		break;
	default:
		isNumber = true;
		cursorMove(modo, isNumber, isEncoder);
		break;
	}

}

void digitCursorInc (digit d){
	if (d == UNIDAD_MIL){
		digito = UNIDAD;
		return;
	}else{
		digito ++;
		return;
	}
}

void digitCursorDec (digit d){
	if (d == UNIDAD){
		digito = UNIDAD_MIL;
		return;
	}else {
		digito -- ;
		return;
	}
}

void cursorMove(mode modo, bool isNumber, bool isEncoder){
	if (modo == CC){
		switch (digito){
		case UNIDAD:
			lcdCursor(1, 9);

			if (isNumber){
				unidad = tecla-'0';
				lcdEscribeInt(unidad);
				lcdCursor(1, 9);
			}
			if (isEncoder){
				if (incEncoder){
					if (unidad == 9){
						unidad = 0;
						lcdEscribeInt(unidad);
						lcdCursor(1, 9);
					}else{
						unidad++;
						lcdEscribeInt(unidad);
						lcdCursor(1, 9);
					}
				}else{
					if (unidad == 0){
						unidad = 9;
						lcdEscribeInt(unidad);
						lcdCursor(1, 9);
					}else{
						unidad --;
						lcdEscribeInt(unidad);
						lcdCursor(1, 9);
					}
				}
				isEncoder = false;
			}

			usbSendString("posicion cursor: ");
			usbSendInt(9);
			usbSendString("\n\r");
			break;

		case DECENA:
			lcdCursor(1, 8);

			if (isNumber){
				decena = tecla-'0';
				lcdEscribeInt(decena);
				digitCursorDec(digito);
				lcdCursor(1, 9);
			}

			if (isEncoder){
				if (incEncoder){
					if (decena == 9){
						decena = 0;
						lcdEscribeInt(decena);
						lcdCursor(1, 8);
					}else{
						decena++;
						lcdEscribeInt(decena);
						lcdCursor(1, 8);
					}
				}else{
					if (decena == 0){
						decena = 9;
						lcdEscribeInt(decena);
						lcdCursor(1, 8);
					}else{
						decena --;
						lcdEscribeInt(decena);
						lcdCursor(1, 8);
					}
				}
				isEncoder = false;
			}

			usbSendString("posicion cursor: ");
			usbSendInt(8);
			usbSendString("\n\r");
			break;

		case CENTENA:
			lcdCursor(1, 7);

			if (isNumber){
				centena = tecla-'0';
				lcdEscribeInt(centena);
				digitCursorDec(digito);
				lcdCursor(1, 8);
			}

			if (isEncoder){
				if (incEncoder){
					if (centena == 9){
						centena = 0;
					}else{
						centena++;
					}
					lcdEscribeInt(centena);
					lcdCursor(1, 7);
				}else{
					if (centena == 0){
						centena = 9;
						lcdEscribeInt(centena);
						lcdCursor(1, 7);
					}else{
						centena --;
						lcdEscribeInt(centena);
						lcdCursor(1, 7);
					}
				}
				isEncoder = false;
			}

			usbSendString("posicion cursor: ");
			usbSendInt(7);
			usbSendString("\n\r");
			break;

		case UNIDAD_MIL:
			lcdCursor(1, 6);

			if (isNumber){
				unidad_mil = tecla-'0';
				if (unidad_mil > 3){
					unidad_mil = 3;
				}
				lcdEscribeInt(unidad_mil);
				digitCursorDec(digito);
				lcdCursor(1, 7);
			}

			if (isEncoder){
				if (incEncoder){
					if (unidad_mil == 3){
						unidad_mil = 0;
					}else{
						unidad_mil++;
					}
					lcdEscribeInt(unidad_mil);
					lcdCursor(1, 6);
				}else{
					if (unidad_mil == 0){
						unidad_mil = 3;
					}else{
						unidad_mil --;
					}
					lcdEscribeInt(unidad_mil);
					lcdCursor(1, 6);
				}
				isEncoder = false;
			}

			usbSendString("posicion cursor: ");
			usbSendInt(6);
			usbSendString("\n\r");
			break;
		}

	}else{

	}
}

/*
 * @brief Ordena N muestras del ADC
 * @param Muestras del ADC a ser ordenadas
 * @param cantidad de muestras a ser ordenadas
 * @retval None
 */
void sort (uint16_t *data, uint16_t lenght){
	uint8_t exchange = 1;
	uint16_t tmp = 0;
	/* Sort tab */
	while(exchange==1)
	{
		exchange=0;
		for(uint16_t l=0; l<lenght-1; l++)
		{
			if( data[l] > data[l+1] )
			{
				tmp = data[l];
				data[l] = data[l+1];
				data[l+1] = tmp;
				exchange=1;
			}
		}
	}
}

/**
 * @brief obtiene el promedio de N-X muestras del ADC
 * @param Cantidad de mustras del ADC a ser eliminadas (N)
 * @param Cantidad de muestras del ADC a ser promediadas (X)
 * @retval El valor promedio
 */
uint16_t promedioN_X(uint16_t N , uint16_t X){
	uint32_t avg_sample = 0;
	uint16_t adc_sample[X];

	//se obtien las muestras del ADC
	for (uint16_t i = 0 ; i < ADC_MUESTRAS ; i++){
		adc_sample[i] = leerADC(1);
		adc_sample[i] = (29990*adc_sample[i])>>ESCALA;
	}

	/* ordenar muestras del ADC */
	sort(adc_sample, X);

	/* sumar las N muestras del ADC */
	uint16_t d = N>>1;              // N/2
	for (uint16_t index=d; index<X-d; index++)
	{
		avg_sample += adc_sample[index];
	}
	/* Calcular el promedio de N-X muestras del ADC */
	avg_sample /= X-N;
	/* Retornar el valor promedio */
	if (avg_sample <= 600){
		return avg_sample+5;
	}else if (avg_sample <=2000 && avg_sample > 600 ) {
		return avg_sample + 8;
	}else{return avg_sample+5;}
}

uint16_t mediaMovil (uint16_t *valores){

}
