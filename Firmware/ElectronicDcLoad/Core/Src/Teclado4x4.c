/*
 * Teclado4x4.c
 *
 *  Created on: 4 oct. 2020
 *      Author: Jorge Cáceres
 */

#include "Teclado4x4.h"


/****************************************************************************
 * 						Orden de las teclas									*
 * 																			*
 * 			c1			c2			c3			c4							*
 *			 																*
 * 			| 1|		| 2|		| 3|		| 4|	f1					*
 *																			*
 * 			| 5|		| 6|		| 7|		| 8|	f2					*
 *																			*
 * 			| 9|		| 10|		|11|		|12|	f3					*
 *																			*
 * 			|13|		| 14|		|15|		|16|	f4					*
 ****************************************************************************/




/******************************************************************************
 * Pimero se define los pines  que se utilizarán con sus respectivos puertos. *
 *          Todas las filas deben ser configuradas como salida				  *
 ******************************************************************************
 */
#define f1_Pin GPIO_PIN_6		//	PB6
#define f1_GPIO_Port GPIOB
#define f2_Pin GPIO_PIN_7		//	PB7
#define f2_GPIO_Port GPIOB
#define f3_Pin GPIO_PIN_8		//	PB8
#define f3_GPIO_Port GPIOB
#define f4_Pin GPIO_PIN_9		//	PB9
#define f4_GPIO_Port GPIOB
#define c1_Pin GPIO_PIN_15		//	PA15
#define c1_GPIO_Port GPIOA
#define c2_Pin GPIO_PIN_3		//	PB3
#define c2_GPIO_Port GPIOB
#define c3_Pin GPIO_PIN_4		//	PB4
#define c3_GPIO_Port GPIOB
#define c4_Pin GPIO_PIN_5		//	PB5
#define c4_GPIO_Port GPIOB


  void highFilas(){
	HAL_GPIO_WritePin(f1_GPIO_Port, f1_Pin, 1);
	HAL_GPIO_WritePin(f2_GPIO_Port, f2_Pin, 1);
	HAL_GPIO_WritePin(f3_GPIO_Port, f3_Pin, 1);
	HAL_GPIO_WritePin(f4_GPIO_Port, f4_Pin, 1);
}

void lowFilas(){
	HAL_GPIO_WritePin(f1_GPIO_Port, f1_Pin, 0);
	HAL_GPIO_WritePin(f2_GPIO_Port, f2_Pin, 0);
	HAL_GPIO_WritePin(f3_GPIO_Port, f3_Pin, 0);
	HAL_GPIO_WritePin(f4_GPIO_Port, f4_Pin, 0);
}

int scanF1(){

	HAL_GPIO_WritePin(f1_GPIO_Port, f1_Pin, 0);
	if (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){
		while (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){}
		return 1;
	}else if (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){
		while (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){}
		return 2;

	}else if(HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){
		while (HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){}
		return 3;
	}else if(HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){
		while (HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){}
		return 4;
	}else{
		highFilas();
		return 32;
	}
}


char scanF2(){

	HAL_GPIO_WritePin(f2_GPIO_Port, f2_Pin, 0);
	if (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){
		while (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){}
		return 5;
	}else if (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){
		while (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){}
		return 6;

	}else if(HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){
		while (HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){}
		return 7;
	}else if(HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){
		while (HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){}
		return 8;
	}else{
		highFilas();
		return 32;
	}
}

uint8_t scanF3(){

	HAL_GPIO_WritePin(f3_GPIO_Port, f3_Pin, 0);
	if (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){
		while (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){}
		return 9;
	}else if (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){
		while (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){}
		return 10;

	}else if(HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){
		while (HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){}
		return 11;
	}else if(HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){
		while (HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){}
		return 12;
	}else{
		highFilas();
		return 32;
	}
}

uint8_t scanF4(){

	HAL_GPIO_WritePin(f4_GPIO_Port, f4_Pin, 0);
	if (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){
		while (HAL_GPIO_ReadPin(c1_GPIO_Port, c1_Pin) == 0){};
		return 13;
	}else if (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){
		while (HAL_GPIO_ReadPin(c2_GPIO_Port, c2_Pin) == 0){}
		return 14;

	}else if(HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){
		while (HAL_GPIO_ReadPin(c3_GPIO_Port, c3_Pin) == 0){}
		return 15;
	}else if(HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){
		while (HAL_GPIO_ReadPin(c4_GPIO_Port, c4_Pin) == 0){}
		return 16;
	}else{
		highFilas();
		return 32;
	}
}


/***********************************************************************************
 * si retorna . no se presiono tecla alguna, cualquier otro valor simboliza		   *
 * una tecla, y se le deberá asignar un valor o función en el programa principal   *
 * 																				   *
 ***********************************************************************************/

uint8_t tecladoScan(){
	uint8_t teclaOrden;

	highFilas();
	teclaOrden = scanF1();
	if (teclaOrden != 32){
		lowFilas();					/* SACAR ESTA FILA SI NO SE USA DEBOUNCING POR INTERRUPCIÓN */
		return teclaOrden;
	}
	teclaOrden = scanF2();
	if (teclaOrden != 32){
		lowFilas();					/* SACAR ESTA FILA SI NO SE USA DEBOUNCING POR INTERRUPCIÓN */
		return teclaOrden;
	}
	teclaOrden = scanF3();
	if (teclaOrden != 32){
		lowFilas();					/* SACAR ESTA FILA SI NO SE USA DEBOUNCING POR INTERRUPCIÓN */
		return teclaOrden;
	}

	teclaOrden = scanF4();
	if (teclaOrden != 32){
		lowFilas();					/* SACAR ESTA FILA SI NO SE USA DEBOUNCING POR INTERRUPCIÓN */
		return teclaOrden;
	}
	lowFilas();						/* SACAR ESTA FILA SI NO SE USA DEBOUNCING POR INTERRUPCIÓN */
	return 32;
}



char tecladoDeco(uint8_t ordenTecla){
	switch (ordenTecla){
		case 1:
			return '1';
			break;
		case 2:
			return '2';
			break;
		case 3:
			return '3';
			break;
		case 4:
			return 'A';
			break;
		case 5:
			return '4';
			break;
		case 6:
			return '5';
			break;
		case 7:
			return '6';
			break;
		case 8:
			return 'B';
			break;
		case 9:
			return '7';
			break;
		case 10:
			return '8';
			break;
		case 11:
			return '9';
			break;
		case 12:
			return 'C';
			break;
		case 13:
			return '*';
			break;
		case 14:
			return '0';
			break;
		case 15:
			return '#';
			break;
		case 16:
			return 'D';
			break;
	}
	return 32;

}

