#include "ftostr.h"


uint8_t Word2Dec(char* dest, uint16_t a,uint8_t left){
 
 uint8_t k,i,dig,n;
 uint16_t b;
   
 k=4;   
 while(a>=10)
  {
   b=a/10; dig = a-10*b;
   dest[k--]=dig+48;
   a=b;
  }
 dest[k]=a+48;
 
 n=5-k; 

 if (left)
  {
   if (n<5) for(i=0;i<n;i++) dest[i]=dest[i+k];   
  }
 else
  {
   for (i=0;i<k;i++) dest[i]=48;  
   n=5; 
  }
 dest[n]=0;  
 return n;
}


uint8_t int2frac(uint16_t frac, unsigned char ndec, char* dest){
 
 unsigned char dig,i,p=12;
 uint16_t mask;
 uint8_t k=0;
 
 dest[k++]=46; // "."

 mask = 0x0FFF; 
 for(i=0;i<ndec;i++)
   {
    frac&=mask;     
    frac*=10; 
    dig = (frac>>p);
    dest[k++]=dig+48;
   }
 dest[k]=0;

#ifdef ROUNDING
 frac&=mask;
 dig=0; if(frac>=2048) dig=1; 
 
 i=k-1;
 while(dig && (i>0))
   {
    dest[i]++; if (dest[i]==58) dest[i]=48; else dig=0;
    i--;
   }
 return dig;
#endif

 return 0;
}


uint8_t float2str(char* dest, float x, unsigned short dec){


 signed char e,p;
 uint8_t E,k; 
 uint32_t F,I;
 uint8_t s,r;


 F = *(uint32_t*)&x;  

 s=0;


#ifdef MIKROE
 if (F & 0x00800000) s=1; // sign bit
 E = (F>>24);             // exponent bits
#else
 if (F & 0x80000000) s=1; // sign bit 
 E = ((F>>23)&0xFF);      // exponent bits         
#endif

 F = F & 0x007FFFFF;  // extracts mantissa bits
 if (E>0) F = F | 0x00800000;  // if not 0, add phantom bit to mantissa 1.xxxxxxx
 e = (E-0x7F);  // correct bias in exponent
 
// Number too large: X
 if (e>=16) { dest[0]='X'; dest[1]=0; return 1;} 
 
// Number too small: 0
 if (e<=-16) { dest[0]=48; dest[1]=0; return k;}

 // Extracts integer part(I) 
 p = 23-(int8_t)e;  //position of "decimal" point when exponent is taken into account
 if (p>24) I=0; else  I=F>>p; // integer part
 
 // First 12 bits of fraction (F)
 p -=12; if (p>=24) F=0; else if (p>=0) F>>=p; else F<<=(-p); 
 F&=0x0FFFL;

#ifdef ROUNDING  // Check if rounding carries into the integer part
   r=int2frac(F,dec,dest); I+=r;
#endif 

 // Write float number: sign + integer part + fractio
   if (s==1){
       dest[0]=45;
       k=1;
   }else{
       k=0; 
   }
 //dest[0]=43+(s<<1);  // Sign 43 = +, 45 = -
 k+=Word2Dec(dest+k,(uint16_t)I,1);   // Integer 
 int2frac(F,dec,dest+k);        // sends 12 MS bit to INt2frac 
 k+=(dec+1);

 return(k);
}



