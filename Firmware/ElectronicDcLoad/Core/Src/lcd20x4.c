/*
 * LCD1602.c
 *
 *  Created on: Sep 30, 2020
 *      Author: Jorge Cáceres
 */

#include <lcd20x4.h>
#include "stm32f1xx_hal.h"
#include "ftostr.h"
#include <stdlib.h>

/*********** Define the LCD PINS below ****************/

#define RS_Pin GPIO_PIN_13
#define RS_GPIO_Port GPIOB
//#define RW_Pin GPIO_PIN_2
//#define RW_GPIO_Port GPIOA
#define EN_Pin GPIO_PIN_14
#define EN_GPIO_Port GPIOB
#define D4_Pin GPIO_PIN_15
#define D4_GPIO_Port GPIOB
#define D5_Pin GPIO_PIN_8
#define D5_GPIO_Port GPIOA
#define D6_Pin GPIO_PIN_9
#define D6_GPIO_Port GPIOA
#define D7_Pin GPIO_PIN_10
#define D7_GPIO_Port GPIOA

/****************** se define el manejador del timer  **************/
#define timer1 htim1


extern TIM_HandleTypeDef timer1;

/* se hace un delay de microsegundos para el lcd con el timer seleccionad0*/
void delay (uint16_t us)
{
	__HAL_TIM_SET_COUNTER(&timer1, 0);
	while (__HAL_TIM_GET_COUNTER(&timer1) < us);
}

/****************************************************************************************************************************************************************/

void lcdHome(){
    lcdCmd(0B00000010);
    return;
}

void lcdOff(){
	lcdCmd(0x08);
	return;
}

void lcdLinea1(){
    lcdCmd(0B10000000);
    return;
}
void lcdLinea2(){
    lcdCmd(0B11000000);
    return;
}

void lcdLinea3(){
    lcdCmd(0b10010100);
    return;
}

void lcdCursorOFF(){
    lcdCmd(0b00001100);
    return;
}

void lcdCursorON(){
    lcdCmd(0b00001110);
    return;
}

void lcdCursorIncr(){
    lcdCmd(0b00000110);
    return;
}

void lcdON(){
    lcdCmd(0b0000001100);
    return;
}
void lcdLinea4(){
    lcdCmd(0b11010100);
    return;
}

void pulsoEnable(){
	HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, 1);
    delay(20);
    HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, 0);
    delay(20);
}

void escribeLcd (char data, int rs)
{
	HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, rs);  // rs = 1 para datos, rs=0 para comandos

	/* ecribir datos a los respectivos pines */
	HAL_GPIO_WritePin(D7_GPIO_Port, D7_Pin, ((data>>3)&0x01));
	HAL_GPIO_WritePin(D6_GPIO_Port, D6_Pin, ((data>>2)&0x01));
	HAL_GPIO_WritePin(D5_GPIO_Port, D5_Pin, ((data>>1)&0x01));
	HAL_GPIO_WritePin(D4_GPIO_Port, D4_Pin, ((data>>0)&0x01));

	/* Toggle PIN EN para enviar dato
	 * si HCLK > 100 MHz, use delay de 20 us
	 * si el LCD sigue sin funcionar, incrementar el delay a 50, 80 o 100..
	 */
	pulsoEnable();
}

void lcdCmd (char cmd)
{
    char dataToSend;

    /* se envia primero el nibble superior */
    dataToSend = ((cmd>>4)&0x0f);
    escribeLcd(dataToSend,0);  // RS must be 0 while sending command

    /* se envia el nibble inferior */
    dataToSend = ((cmd)&0x0f);
	escribeLcd(dataToSend, 0);
}

void lcdDato (char data)
{
	char dataToSend;

	/* se envia primero el nibble superior */
	dataToSend = ((data>>4)&0x0f);
	escribeLcd(dataToSend, 1);  // rs =1 for sending data

	/* se envia el nibble inferior */
	dataToSend = ((data)&0x0f);
	escribeLcd(dataToSend, 1);
}

void lcdClear (void)
{
	lcdCmd(0x01);
	HAL_Delay(2);
	return;
}

void lcdCursor(uint8_t linea, uint8_t caracter){
	if (caracter > 20 || linea > 4 || caracter < 0 || linea < 0)
		return;
	uint8_t pos = 0;
    switch (linea)
    {
        case 0:
            pos = 0x00 + caracter;
            break;
        case 1:
            pos= 0x40+caracter;
            break;
        case 2:
        	pos = 0x14 + caracter;
        break;
        case 3:
        	pos = 0x54 + caracter;
        break;
    }

    lcdCmd (0x80 | pos);
}


void lcdInit (void)
{
	// 4 bit initialization
	HAL_Delay(50);  // esperar por mas de >40ms
	lcdCmd (0x30);
	HAL_Delay(5);  // esperar por mas de >4.1ms
	lcdCmd (0x30);
	HAL_Delay(1);  // esperar por mas de >100us
	lcdCmd (0x30);
	HAL_Delay(10);
	lcdCmd (0x20);  // modo 4bit
	HAL_Delay(10);

  // display initialization
	lcdCmd (0x28); // Function set --> DL=0 (modo 4 bit), N = 1 (2 lineas) F = 0 ( carecteres de 5x8 )
	HAL_Delay(1);
	lcdCmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
	HAL_Delay(1);
	lcdCmd (0x01);  // clear display
	HAL_Delay(1);
	HAL_Delay(1);
	lcdCmd (0x06); //Entry mode set --> I/D = 1 (cursor incremental) & S = 0 (sin shift)
	HAL_Delay(1);
	lcdCmd (0x0C); //Display on/off control --> D = 1, C y B = 0. (Cursor y blink, ultimos dos bits)
	lcdClear();
}

void lcdString (char *msg)
{
	while(*msg){
		lcdDato(*msg);
	    msg++;
	}
	return;
}

void lcdCaracter(char caracter){
    if (caracter!=0x00){
        lcdDato(caracter);
    }
    return;
}

void lcdEscribeInt(uint32_t numero){
    char buffer[20];
    itoa(numero, buffer, 10);
    lcdString(buffer);
    return;
}

void lcdEscribeFloat(long numero){
	char buffer[20];
	float2str(buffer, numero, 5);
	lcdString(buffer);
	return;
}
