/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "Implementacion.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */


// Variables Globales Para uso en Interrupcion

volatile bool processing = false;
volatile unsigned char tecla = 0x20;
volatile bool loadOn = false;
volatile state estadoAnterior;
volatile state estadoActual;
volatile bool newKeyPressed = false;
volatile state estado = SYSTEM_INIT;
volatile uint8_t iProg_unidad = 0;
volatile uint8_t iProg_decena = 0;
volatile uint8_t iProg_centena = 0;
volatile uint8_t iProg_unidad_mil = 0;
volatile bool incEncoder = false;
volatile bool isEncoder = false;
volatile digit digito = UNIDAD_MIL;
volatile uint32_t iSet = 0;


/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */
#define usbD_Pin GPIO_PIN_12
#define usbD_GPIO_Port GPIOA
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint16_t  ADC_MUESTRAS = 1<<ADC_MUESTRAS_BITS;	// numero de mediciones a tomar, se hace potencia de 2 para dividir rapido

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  usbReset();
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_USB_DEVICE_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */

  HAL_Delay(1000);
  estadoAnterior = estado;
  mode modo = CC;
  uint64_t iRead = 0, vRead = 0;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  usbSendString("\t \t #################################### \n\r");
  usbSendString("\t\t#                                     #\n\r");
  usbSendString("\t\t#  ELECTRONIC DC LOAD SERIAL MONITOR  # \n\r");
  usbSendString("\t\t#                                     #\n\r");
  usbSendString("\t\t  ################################### \n\r\n");

  while (1)
  {
	  switch (estado){
	  case SYSTEM_INIT:
		  usbSendString("Estado: SYSTEM_INIT\n\r");
		  usbSendString("Enum Estado: ");
		  usbSendInt(estado);
		  usbSendString("\n\r");
		  systemInit();
		  estado = LCD_HOME_TEMPLATE;
		  estadoActual = estado;
		  break;
	  case LCD_HOME_TEMPLATE:
		  usbSendString("Estado: LCD_HOME_TEMPLATE\n\r");
		  usbSendString("Enum Estado: ");
		  usbSendInt(estado);
		  usbSendString("\n\r");
		  homeDisplay();
		  estado = LCD_TEMPLATE;
		  usbSendString("\n\r");
		  estadoActual = estado;
		  break;

	  case LCD_TEMPLATE:
		  usbSendString("\nEstado: LCD_TEMPLATE\n\r");
		  usbSendString("Enum Estado: ");
		  usbSendInt(estado);
		  usbSendString("\n\r");
		  switch (modo){
		  case CC:
			  ccDisplay();
			  estado = ADC_READ_CURRENT;
			  HAL_TIM_Base_Start_IT(&htim3);
			  break;
		  case TRAN:
			  tranDisplay();
			  estado = ADC_READ_CURRENT;
			  break;

		  }
		  estadoActual = estado;
		  break;

	  case ADC_READ_VOLTAGE:				// FALTA IMPLEMENTAR, decidir el template de Vread
		  vRead = leerADC(0);
		  if (modo == CC){
			  lcdCursor(2, 6);
			  lcdEscribeInt(vRead);
		  }
		  estado = ADC_READ_CURRENT;
		  estadoActual = estado;
		  break;

	  case ADC_READ_CURRENT:
		  //usbSendString("\nEstado: ADC_READ_CURRENT\n\r");
		  //usbSendString("Enum Estado: ");
		  // usbSendInt(estado);
		 /* for (uint16_t i = 0 ; i < ADC_MUESTRAS ; i++){
			  ibuff[i] = leerADC(1);
			  iRead += ibuff[i];
			  //HAL_Delay(1);
		  }*/
		  iRead = promedioN_X(ADC_SAMPLE_DELETION, ADC_MUESTRAS);
		  //estado = ADC_READ_CURRENT;
		  if (estadoAnterior == KEYPAD_DECODE || estadoAnterior == DAC_SET_CURRENT
				  	  	  	  	  	  	  	  || estadoAnterior == CALC_ISET
											  || estadoAnterior == ADC_READ_CURRENT){
			  HAL_TIM_Base_Start_IT(&htim3);
		  }
		  break;

	  case KEYPAD_DECODE:
		  //usbSendString("Estado: KEYPAD_DECODE\n\r");
		  //usbSendString("Enum Estado: ");
		  //usbSendInt(estado);
		  //usbSendString("\n\r");
		  usbSendString("Tecla preisonada :");
		  usbSendString((unsigned char*)&tecla);
		  usbSendString("\n\r");

		  switch (tecla){
		  case 'A':				//cambia a modo corriente constante
			  modo = CC;
			  loadOn = false;
			  loadControl(loadOn);
			  estado = LCD_TEMPLATE;
			  newKeyPressed = false;
			  usbSendString("MODE: CC");
			  usbSendString("\n\r");
			  processing = false;
			  break;

		  case 'B':				//cambia a modo transitorio
			  modo = TRAN;
			  loadOn = false;
			  loadControl(loadOn);
			  estado = LCD_TEMPLATE;
			  newKeyPressed = false;
			  usbSendString("MODE: TRAN");
			  usbSendString("\n\r");
			  processing = false;
			  break;

		  case 'C':				//cambia a modo enter
			  estadoAnterior = estado;
			  estado = CALC_ISET;
			  newKeyPressed = false;
			  lcdCursorOFF();
			  processing = false;
			  iSet = unidad + decena*10 + centena*100 + unidad_mil*1000;
			  estado = CALC_ISET;
			  HAL_TIM_Base_Start_IT(&htim3);
			  break;

		  case 'D':				//habilita o deshabilita la carga dependiendo del estado anterior

			 if (loadOn == true){
				 loadOn = false;
				 usbSendString("Se Deshabilito La Salida\n\r");
				 usbSendString("\n\r");
			 }else{
				 loadOn = true;
				 usbSendString("Se Habilito La Salida");
				 usbSendString("\n\r");
			 }

			  loadControl(loadOn);
			  if (modo == CC || modo == TRAN){
				  estado = ADC_READ_CURRENT;
			  }
			  newKeyPressed = false;
			  processing = false;
			  break;

		  default:
			  HAL_TIM_Base_Stop_IT(&htim3);
			  estado = EDIT;
			  processing = false;
			  break;
		  }
		  break;

	  case DAC_SET_CURRENT:
		  if (modo == CC){

			  setDac(iSet+2);
			  /*if (iSet <= 26214){
				  setDac(iSet+2);
			  }else{
				  setDac(iSet-28);
			  }*/
			  estado = ADC_READ_CURRENT;

		  }

		  break;

	  case CALC_ISET:
		  usbSendString("Iset: ");
		  usbSendInt(iSet);
		  usbSendString(" mA  CODE: ");
		  if (estadoAnterior == KEYPAD_DECODE){
			  if (iSet == 3000){
				  iSet = 65533;
				  usbSendInt(iSet);
			  }else{
				  iSet = ((iSet*10)<<ESCALA)/ADC_LSB;
				  usbSendInt(iSet);
			  }
			  usbSendString("\n\r");
		  }
		  estado = DAC_SET_CURRENT;
		  break;

	  case LCD_UPDATE:
		  updateMeas(iRead, modo);
		  HAL_TIM_Base_Start_IT(&htim3);
		  if (estadoAnterior == LCD_UPDATE){
			  estado = ADC_READ_CURRENT;
		  }else{
			  estado = estadoAnterior;
		  }

		  break;

	  case EDIT:
		  HAL_TIM_Base_Stop_IT(&htim3);

		  if (newKeyPressed){
			  editKeysHandle(tecla, modo);
			  newKeyPressed = false;
		  }
		  if (isEncoder){
			  lcdCursorON();
			  cursorMove(modo, false, isEncoder);
			  isEncoder = false;

		  }
		  estadoAnterior = EDIT;
		  break;
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_USB;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 72-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 100-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 10800-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 1000-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 7200-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ledHeartBeat_GPIO_Port, ledHeartBeat_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, Fan_Pin|D5_Pin|D6_Pin|D7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, ADC_CONVST_Pin|DAC_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ADC_CS_GPIO_Port, ADC_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LoadOnOff_Pin|RS_Pin|E_Pin|D4_Pin
                          |KP_F1_Pin|KP_F2_Pin|KP_F3_Pin|KP_F4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : ledHeartBeat_Pin */
  GPIO_InitStruct.Pin = ledHeartBeat_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ledHeartBeat_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : OCP_Pin */
  GPIO_InitStruct.Pin = OCP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OCP_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Fan_Pin ADC_CONVST_Pin DAC_CS_Pin D5_Pin
                           D6_Pin D7_Pin */
  GPIO_InitStruct.Pin = Fan_Pin|ADC_CONVST_Pin|DAC_CS_Pin|D5_Pin
                          |D6_Pin|D7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : ADC_EOC_Pin */
  GPIO_InitStruct.Pin = ADC_EOC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ADC_EOC_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ADC_CS_Pin LoadOnOff_Pin RS_Pin E_Pin
                           D4_Pin KP_F1_Pin KP_F2_Pin KP_F3_Pin
                           KP_F4_Pin */
  GPIO_InitStruct.Pin = ADC_CS_Pin|LoadOnOff_Pin|RS_Pin|E_Pin
                          |D4_Pin|KP_F1_Pin|KP_F2_Pin|KP_F3_Pin
                          |KP_F4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : ENC_CH_A_Pin */
  GPIO_InitStruct.Pin = ENC_CH_A_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ENC_CH_A_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RVP_Pin ENC_CH_B_Pin */
  GPIO_InitStruct.Pin = RVP_Pin|ENC_CH_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : ENC_BUTTON_Pin */
  GPIO_InitStruct.Pin = ENC_BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ENC_BUTTON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : KP_C1_Pin */
  GPIO_InitStruct.Pin = KP_C1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(KP_C1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : KP_C2_Pin KP_C3_Pin KP_C4_Pin */
  GPIO_InitStruct.Pin = KP_C2_Pin|KP_C3_Pin|KP_C4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);

  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */


//###################### Interrupción externa por cambio de estado/flanco ascendente ##########################

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if (GPIO_Pin == ENC_CH_A_Pin){		// <--- todos los pines de igual numero están mulltiplexados y generan la misma interrupcion
		// si ENC_CH_A esta en alto y ENC_CH_B está en alto, encoder sentido horario
		if (HAL_GPIO_ReadPin(ENC_CH_B_GPIO_Port, ENC_CH_B_Pin) == 1){			// el canal A genera la interrupcion y el canal B determina el sentido

			// aumentar encoder

			incEncoder = true;
			isEncoder = true;
			estado = EDIT;
			return;
		}else{
			// si ENC_CH_A esta en alto y ENC_CH_B está en bajo, encoder sentido antihorario
			incEncoder = false;
			isEncoder = true;
			estado = EDIT;
			return;
		}
	}
	if (GPIO_Pin == ENC_BUTTON_Pin){
		iSet= 0;
		estado = DAC_SET_CURRENT;
		return;
	}

	// interrupcion ocacionada por la pulsacion de teclado
	if (processing == false && (GPIO_Pin == KP_C1_Pin ||
			GPIO_Pin == KP_C2_Pin ||
			GPIO_Pin == KP_C3_Pin ||
			GPIO_Pin == KP_C4_Pin)){

		processing = true;

		// se deshabilitan las interrupciones externas
		HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
		HAL_NVIC_DisableIRQ(EXTI3_IRQn);
		HAL_NVIC_DisableIRQ(EXTI4_IRQn);
		HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);

		// activa timer de anti rebote
		HAL_TIM_Base_Start_IT(&htim2);
		return;
	}
}

/*##################### Interrupción por timer #######################################*/

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){

	if (htim == &htim2){
		HAL_TIM_Base_Stop_IT(&htim2);
		tecla = tecladoDeco(tecladoScan());

		if (tecla != 32){
			newKeyPressed = true;
			estadoAnterior = estado;
			estado = KEYPAD_DECODE;
		}else{
			processing = false;
		}

	// se habilitan las interrupciones externas
		HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
		HAL_NVIC_EnableIRQ(EXTI3_IRQn);
		HAL_NVIC_EnableIRQ(EXTI4_IRQn);
		HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);


	}

	if (htim == &htim3){
		HAL_TIM_Base_Stop_IT(&htim3);
		estadoAnterior = estado;
		if(estado == LCD_UPDATE){
			estado = ADC_READ_CURRENT;
		}else{
			estado = LCD_UPDATE;
		}
		return;

	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
