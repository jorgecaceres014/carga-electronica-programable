EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R20
U 1 1 5F9A3D9B
P 6450 4200
F 0 "R20" V 6350 4200 50  0000 C CNN
F 1 "1k" V 6450 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6380 4200 50  0001 C CNN
F 3 "~" H 6450 4200 50  0001 C CNN
F 4 "1 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 6450 4200 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 6450 4200 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1001V" H 6450 4200 50  0001 C CNN "Manufacturer_Part_Number"
	1    6450 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R21
U 1 1 5F9A4B72
P 6450 4400
F 0 "R21" V 6565 4400 50  0000 C CNN
F 1 "1k" V 6450 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6380 4400 50  0001 C CNN
F 3 "~" H 6450 4400 50  0001 C CNN
F 4 "1 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 6450 4400 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 6450 4400 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1001V" H 6450 4400 50  0001 C CNN "Manufacturer_Part_Number"
	1    6450 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 4200 6150 4200
Wire Wire Line
	6150 3900 6150 4200
Connection ~ 6150 4200
Wire Wire Line
	6150 4200 6300 4200
$Comp
L power:GNDA #PWR021
U 1 1 5F9A9B68
P 6150 4850
F 0 "#PWR021" H 6150 4600 50  0001 C CNN
F 1 "GNDA" H 6155 4677 50  0000 C CNN
F 2 "" H 6150 4850 50  0001 C CNN
F 3 "" H 6150 4850 50  0001 C CNN
	1    6150 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3900 5300 4300
Wire Wire Line
	5300 4300 5400 4300
$Comp
L Device:R R10
U 1 1 5F9B2BD1
P 4800 2000
F 0 "R10" V 4700 2000 50  0000 C CNN
F 1 "100" V 4800 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4730 2000 50  0001 C CNN
F 3 "~" H 4800 2000 50  0001 C CNN
F 4 "100 Ohms ±1% 0.333W, 1/3W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Moisture Resistant, Pulse Withstanding Thick Film" H 4800 2000 50  0001 C CNN "Description"
F 5 "KOA Speer Electronics, Inc." H 4800 2000 50  0001 C CNN "Manufacturer_Name"
F 6 "SG73P1JTTD1000F" H 4800 2000 50  0001 C CNN "Manufacturer_Part_Number"
	1    4800 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R19
U 1 1 5F9B31C9
P 6450 2300
F 0 "R19" H 6400 2300 50  0000 R CNN
F 1 "100k" V 6450 2400 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6380 2300 50  0001 C CNN
F 3 "~" H 6450 2300 50  0001 C CNN
F 4 "100 kOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 6450 2300 50  0001 C CNN "Description"
F 5 "Rohm Semiconductor" H 6450 2300 50  0001 C CNN "Manufacturer_Name"
F 6 "ESR03EZPF1003" H 6450 2300 50  0001 C CNN "Manufacturer_Part_Number"
	1    6450 2300
	-1   0    0    1   
$EndComp
$Comp
L power:GNDA #PWR026
U 1 1 5F9B3B4A
P 6450 2550
F 0 "#PWR026" H 6450 2300 50  0001 C CNN
F 1 "GNDA" H 6455 2377 50  0000 C CNN
F 2 "" H 6450 2550 50  0001 C CNN
F 3 "" H 6450 2550 50  0001 C CNN
	1    6450 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5F9B4D3C
P 3200 2100
F 0 "R6" V 3100 2100 50  0000 C CNN
F 1 "10k" V 3200 2100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3130 2100 50  0001 C CNN
F 3 "~" H 3200 2100 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 3200 2100 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 3200 2100 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 3200 2100 50  0001 C CNN "Manufacturer_Part_Number"
	1    3200 2100
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5F9B5550
P 3200 2400
F 0 "R7" V 3100 2400 50  0000 C CNN
F 1 "10k" V 3200 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3130 2400 50  0001 C CNN
F 3 "~" H 3200 2400 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 3200 2400 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 3200 2400 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 3200 2400 50  0001 C CNN "Manufacturer_Part_Number"
	1    3200 2400
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C14
U 1 1 5F9B5F3A
P 3200 2550
F 0 "C14" V 3337 2550 50  0000 C CNN
F 1 "NP" V 3428 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3200 2550 50  0001 C CNN
F 3 "~" H 3200 2550 50  0001 C CNN
F 4 "C0G, Respaldo, 50v, valor indeterminado, 0603" H 3200 2550 50  0001 C CNN "Description"
	1    3200 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 2550 3450 2550
Wire Wire Line
	3450 2550 3450 2400
Wire Wire Line
	3450 2400 3350 2400
Wire Wire Line
	3350 2100 3450 2100
Wire Wire Line
	3450 2100 3450 2300
Connection ~ 3450 2400
Wire Wire Line
	3100 2550 2950 2550
Wire Wire Line
	2950 2550 2950 2400
Wire Wire Line
	2950 2400 3050 2400
$Comp
L Device:C_Small C21
U 1 1 5F9BA34A
P 4250 1750
F 0 "C21" V 4021 1750 50  0000 C CNN
F 1 "10n" V 4112 1750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4250 1750 50  0001 C CNN
F 3 "~" H 4250 1750 50  0001 C CNN
F 4 "10000pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 4250 1750 50  0001 C CNN "Description"
F 5 "Murata Electronics" H 4250 1750 50  0001 C CNN "Manufacturer_Name"
F 6 "GRM1885C1H103JA01D" H 4250 1750 50  0001 C CNN "Manufacturer_Part_Number"
	1    4250 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 2200 4550 2200
Wire Wire Line
	4550 2200 4550 2000
Wire Wire Line
	4550 1750 4350 1750
Wire Wire Line
	3700 2100 3600 2100
Wire Wire Line
	3600 1750 3700 1750
Wire Wire Line
	3600 1750 3600 1950
Connection ~ 3600 1950
Wire Wire Line
	3600 1950 3600 2100
Wire Wire Line
	3400 1600 3400 1550
$Comp
L power:GNDA #PWR012
U 1 1 5F9BDA46
P 3050 1550
F 0 "#PWR012" H 3050 1300 50  0001 C CNN
F 1 "GNDA" H 3055 1377 50  0000 C CNN
F 2 "" H 3050 1550 50  0001 C CNN
F 3 "" H 3050 1550 50  0001 C CNN
	1    3050 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1550 3400 1550
Wire Wire Line
	3700 2300 3450 2300
Connection ~ 3450 2300
Wire Wire Line
	3450 2300 3450 2400
$Comp
L Device:R R9
U 1 1 5F9B9D26
P 3850 1750
F 0 "R9" V 3750 1750 50  0000 C CNN
F 1 "4.7k" V 3850 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3780 1750 50  0001 C CNN
F 3 "~" H 3850 1750 50  0001 C CNN
F 4 "4.7 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 3850 1750 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 3850 1750 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B4701V" H 3850 1750 50  0001 C CNN "Manufacturer_Part_Number"
	1    3850 1750
	0    1    1    0   
$EndComp
Connection ~ 5300 4300
Text Label 5050 4300 0    50   ~ 0
I1
Wire Wire Line
	5050 4300 5300 4300
Wire Wire Line
	2950 2400 2850 2400
Connection ~ 2950 2400
Text Label 2850 2400 0    50   ~ 0
I1
Text Label 2850 2100 0    50   ~ 0
Vdac
Wire Wire Line
	4650 2000 4550 2000
Connection ~ 4550 2000
Wire Wire Line
	4550 2000 4550 1750
Wire Notes Line
	2800 1950 3500 1950
Wire Notes Line
	3500 1950 3500 2850
Wire Notes Line
	3500 2850 2800 2850
Wire Notes Line
	2800 2850 2800 1950
Wire Wire Line
	3400 1950 3400 1900
Wire Wire Line
	3400 1950 3600 1950
Text Notes 5000 4750 0    50   ~ 0
Todas Las Resistencias 0.1%
Text Notes 2900 3000 0    50   ~ 0
Resistencias \n    0.1%
$Comp
L Device:R R13
U 1 1 5F9D9252
P 5750 3900
F 0 "R13" V 5650 3900 50  0000 C CNN
F 1 "20k" V 5750 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5680 3900 50  0001 C CNN
F 3 "~" H 5750 3900 50  0001 C CNN
F 4 "20 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 5750 3900 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 5750 3900 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B2002V" H 5750 3900 50  0001 C CNN "Manufacturer_Part_Number"
	1    5750 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 5F9DD4C2
P 6150 4600
F 0 "R18" H 6080 4554 50  0000 R CNN
F 1 "20k" V 6150 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6080 4600 50  0001 C CNN
F 3 "~" H 6150 4600 50  0001 C CNN
F 4 "20 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 6150 4600 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 6150 4600 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B2002V" H 6150 4600 50  0001 C CNN "Manufacturer_Part_Number"
	1    6150 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	11650 2100 11500 2100
Wire Wire Line
	12250 2200 12400 2200
Text Notes 5000 5000 0    50   ~ 0
G=20 V/V\n1A=1V\nFondo de escala: 3A=3V
Text Notes 2850 2250 0    50   ~ 0
Nodo de Suma
$Comp
L Device:R R38
U 1 1 5FA0183C
P 9150 2600
F 0 "R38" V 9050 2600 50  0000 C CNN
F 1 "10k" V 9150 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9080 2600 50  0001 C CNN
F 3 "~" H 9150 2600 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 9150 2600 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 9150 2600 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 9150 2600 50  0001 C CNN "Manufacturer_Part_Number"
	1    9150 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 2800 8700 2600
Text Label 8000 2800 0    50   ~ 0
I1
Connection ~ 8700 2800
$Comp
L power:GNDA #PWR037
U 1 1 5FA4031C
P 8400 3500
F 0 "#PWR037" H 8400 3250 50  0001 C CNN
F 1 "GNDA" H 8405 3327 50  0000 C CNN
F 2 "" H 8400 3500 50  0001 C CNN
F 3 "" H 8400 3500 50  0001 C CNN
	1    8400 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5FA887AA
P 5450 1050
F 0 "R11" H 5400 1050 50  0000 R CNN
F 1 "5.1k" V 5450 1100 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5380 1050 50  0001 C CNN
F 3 "~" H 5450 1050 50  0001 C CNN
F 4 "5.1 kOhms ±1% 0.25W, 1/4W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200, Moisture Resistant Thick Film" H 5450 1050 50  0001 C CNN "Description"
F 5 "KOA Speer Electronics, Inc." H 5450 1050 50  0001 C CNN "Manufacturer_Name"
F 6 "RK73H2ATTD5101F" H 5450 1050 50  0001 C CNN "Manufacturer_Part_Number"
	1    5450 1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 1800 5450 1900
Text Label 5450 1900 0    50   ~ 0
-15v
Wire Wire Line
	5450 2200 5450 2100
Text Label 5450 2100 0    50   ~ 0
+15v
Wire Wire Line
	5050 2400 5150 2400
$Comp
L Device:R R12
U 1 1 5FA9110D
P 5450 2950
F 0 "R12" H 5400 2950 50  0000 R CNN
F 1 "5.1k" V 5450 3000 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5380 2950 50  0001 C CNN
F 3 "~" H 5450 2950 50  0001 C CNN
F 4 "5.1 kOhms ±1% 0.25W, 1/4W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200, Moisture Resistant Thick Film" H 5450 2950 50  0001 C CNN "Description"
F 5 "KOA Speer Electronics, Inc." H 5450 2950 50  0001 C CNN "Manufacturer_Name"
F 6 "RK73H2ATTD5101F" H 5450 2950 50  0001 C CNN "Manufacturer_Part_Number"
	1    5450 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 2600 5450 2700
Wire Wire Line
	5450 2700 5650 2700
Connection ~ 5450 2700
Wire Wire Line
	5450 2700 5450 2800
Wire Wire Line
	5450 1200 5450 1300
Wire Wire Line
	5450 1300 5650 1300
Connection ~ 5450 1300
Wire Wire Line
	5450 1300 5450 1400
$Comp
L Device:R R15
U 1 1 5FAA490E
P 5950 2250
F 0 "R15" H 5900 2250 50  0000 R CNN
F 1 "8.06" V 5950 2300 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5880 2250 50  0001 C CNN
F 3 "~" H 5950 2250 50  0001 C CNN
F 4 "8.06 Ohms ±1% 0.25W, 1/4W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200, Moisture Resistant Thick Film" H 5950 2250 50  0001 C CNN "Description"
F 5 "KOA Speer Electronics, Inc." H 5950 2250 50  0001 C CNN "Manufacturer_Name"
F 6 "RK73H2ATTD8R06F" H 5950 2250 50  0001 C CNN "Manufacturer_Part_Number"
	1    5950 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 900  5450 800 
Wire Wire Line
	5450 800  5950 800 
Wire Wire Line
	5950 800  5950 1100
Wire Wire Line
	5950 1500 5950 1600
Wire Wire Line
	5950 2400 5950 2500
Wire Wire Line
	5450 3100 5450 3200
Wire Wire Line
	5450 3200 5950 3200
Wire Wire Line
	5950 3200 5950 2900
Text Label 5600 3200 0    50   ~ 0
-15v
Wire Wire Line
	6600 4400 6700 4400
Wire Wire Line
	6600 4200 6700 4200
Text Label 7100 700  0    50   ~ 0
V+
Wire Wire Line
	6850 700  7100 700 
Wire Wire Line
	6850 800  6850 700 
Wire Wire Line
	6850 1000 6850 1100
$Comp
L Device:Fuse_Small F1
U 1 1 5F9CD48D
P 6850 900
F 0 "F1" V 6804 948 50  0000 L CNN
F 1 "6.3A" V 6895 948 50  0000 L CNN
F 2 "FootPrints:0031.8221" H 6850 900 50  0001 C CNN
F 3 "~" H 6850 900 50  0001 C CNN
F 4 "6.3A 250V AC 150V DC Fuse Cartridge, Ceramic Holder 5mm x 20mm" H 6850 900 50  0001 C CNN "Description"
F 5 "Schurter Inc." H 6850 900 50  0001 C CNN "Manufacturer_Name"
F 6 "0001.2512" H 6850 900 50  0001 C CNN "Manufacturer_Part_Number"
	1    6850 900 
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q6
U 1 1 5F99EA5A
P 6750 2000
F 0 "Q6" H 6955 2046 50  0000 L CNN
F 1 "IXTH30N60L2" H 6955 1955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-247-3_Vertical" H 6950 2100 50  0001 C CNN
F 3 "~" H 6750 2000 50  0001 C CNN
F 4 "N-Channel 600V 30A (Tc) 540W (Tc) Through Hole TO-247 (IXTH)" H 6750 2000 50  0001 C CNN "Description"
F 5 "IXYS" H 6750 2000 50  0001 C CNN "Manufacturer_Name"
F 6 "IXTH30N60L2" H 6750 2000 50  0001 C CNN "Manufacturer_Part_Number"
	1    6750 2000
	1    0    0    -1  
$EndComp
Text Label 7100 4850 0    50   ~ 0
IN-
Wire Wire Line
	6850 4850 7100 4850
$Comp
L power:GNDA #PWR028
U 1 1 5F9C3F4F
P 6850 4900
F 0 "#PWR028" H 6850 4650 50  0001 C CNN
F 1 "GNDA" H 6855 4727 50  0000 C CNN
F 2 "" H 6850 4900 50  0001 C CNN
F 3 "" H 6850 4900 50  0001 C CNN
	1    6850 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1900 5950 2000
$Comp
L Device:R R14
U 1 1 5FAA3A61
P 5950 1750
F 0 "R14" H 5900 1750 50  0000 R CNN
F 1 "8.06" V 5950 1800 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5880 1750 50  0001 C CNN
F 3 "~" H 5950 1750 50  0001 C CNN
F 4 "8.06 Ohms ±1% 0.25W, 1/4W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200, Moisture Resistant Thick Film" H 5950 1750 50  0001 C CNN "Description"
F 5 "KOA Speer Electronics, Inc." H 5950 1750 50  0001 C CNN "Manufacturer_Name"
F 6 "RK73H2ATTD8R06F" H 5950 1750 50  0001 C CNN "Manufacturer_Part_Number"
	1    5950 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5150 1600 5050 1600
Connection ~ 5950 2000
Wire Wire Line
	5950 2000 5950 2100
Wire Wire Line
	5050 1600 5050 2000
Wire Wire Line
	6550 2000 6450 2000
Wire Wire Line
	6450 2000 6450 2150
Wire Wire Line
	5950 2000 6450 2000
Connection ~ 6450 2000
Wire Wire Line
	4950 2000 5050 2000
Connection ~ 5050 2000
Wire Wire Line
	5050 2000 5050 2400
$Comp
L Device:R R33
U 1 1 5FC108B0
P 8600 4600
F 0 "R33" V 8500 4600 50  0000 C CNN
F 1 "1k" V 8600 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8530 4600 50  0001 C CNN
F 3 "~" H 8600 4600 50  0001 C CNN
F 4 "1 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8600 4600 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8600 4600 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1001V" H 8600 4600 50  0001 C CNN "Manufacturer_Part_Number"
	1    8600 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	8750 4350 8600 4350
Wire Wire Line
	8600 4450 8600 4350
Connection ~ 8600 4350
Wire Wire Line
	8600 4350 8450 4350
$Comp
L Device:R R35
U 1 1 5FC0F5A4
P 8900 4350
F 0 "R35" V 8800 4350 50  0000 C CNN
F 1 "10k" V 8900 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 4350 50  0001 C CNN
F 3 "~" H 8900 4350 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8900 4350 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8900 4350 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 8900 4350 50  0001 C CNN "Manufacturer_Part_Number"
	1    8900 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	8600 4750 8600 4800
$Comp
L Device:R R34
U 1 1 5FC34EFB
P 8900 4150
F 0 "R34" V 8800 4150 50  0000 C CNN
F 1 "10k" V 8900 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 4150 50  0001 C CNN
F 3 "~" H 8900 4150 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8900 4150 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8900 4150 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 8900 4150 50  0001 C CNN "Manufacturer_Part_Number"
	1    8900 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 4150 8600 4150
$Comp
L Device:R R25
U 1 1 5FC3E0AB
P 8200 3900
F 0 "R25" V 8100 3900 50  0000 C CNN
F 1 "1k" V 8200 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 3900 50  0001 C CNN
F 3 "~" H 8200 3900 50  0001 C CNN
F 4 "1 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8200 3900 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8200 3900 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1001V" H 8200 3900 50  0001 C CNN "Manufacturer_Part_Number"
	1    8200 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 3900 8600 3900
Wire Wire Line
	8600 3900 8600 4150
Connection ~ 8600 4150
Wire Wire Line
	8600 4150 8450 4150
Wire Wire Line
	8050 3900 7800 3900
Text Label 9450 4150 0    50   ~ 0
VremoteSense-
Text Label 9450 4350 0    50   ~ 0
VremoteSense+
Wire Wire Line
	9050 4350 9100 4350
Wire Wire Line
	9050 4150 9100 4150
Wire Wire Line
	7800 4250 7800 3900
Wire Wire Line
	7800 4250 7850 4250
Connection ~ 7800 4250
Text Label 4750 5950 0    50   ~ 0
Vmon
Wire Wire Line
	7550 4250 7800 4250
$Comp
L REF3430IDBVR:REF3430IDBVR U4
U 1 1 5FA4559F
P 2700 6200
F 0 "U4" H 3200 5600 60  0000 C CNN
F 1 "REF3430IDBVR" H 2700 6782 60  0000 C CNN
F 2 "FootPrints:REF3430IDBVR" H 2700 6140 60  0001 C CNN
F 3 "" H 2700 6200 60  0000 C CNN
F 4 "Series Voltage Reference IC  ±0.05%  SOT-23-6" H 2700 6200 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 2700 6200 50  0001 C CNN "Manufacturer_Name"
F 6 "REF3430IDBVR" H 2700 6200 50  0001 C CNN "Manufacturer_Part_Number"
	1    2700 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 5900 3600 5900
Wire Wire Line
	3600 5900 3600 6000
Wire Wire Line
	3600 6100 3500 6100
Wire Wire Line
	3500 6400 3600 6400
Wire Wire Line
	3600 6400 3600 6500
Wire Wire Line
	3600 6600 3500 6600
Wire Wire Line
	1900 6300 1850 6300
Wire Wire Line
	1850 6300 1850 5900
Wire Wire Line
	1850 5900 1900 5900
Wire Wire Line
	1850 5900 1850 5700
Wire Wire Line
	1850 5700 1750 5700
Connection ~ 1850 5900
$Comp
L Device:CP1_Small C5
U 1 1 5FA7FAF7
P 1600 6150
F 0 "C5" H 1691 6196 50  0000 L CNN
F 1 "1u" H 1691 6105 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 1600 6150 50  0001 C CNN
F 3 "~" H 1600 6150 50  0001 C CNN
F 4 "1µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) " H 1600 6150 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 1600 6150 50  0001 C CNN "Manufacturer_Name"
F 6 "ECS-T1AZ105R" H 1600 6150 50  0001 C CNN "Manufacturer_Part_Number"
	1    1600 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5FA819CC
P 1300 6150
F 0 "C3" H 1208 6196 50  0000 R CNN
F 1 "100n" H 1208 6105 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1300 6150 50  0001 C CNN
F 3 "~" H 1300 6150 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1300 6150 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 1300 6150 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 1300 6150 50  0001 C CNN "Manufacturer_Part_Number"
	1    1300 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6050 1300 6000
Wire Wire Line
	1300 6000 1450 6000
Wire Wire Line
	1600 6000 1600 6050
Wire Wire Line
	1600 6250 1600 6350
Wire Wire Line
	1600 6350 1450 6350
Wire Wire Line
	1300 6350 1300 6250
Wire Wire Line
	1850 5900 1450 5900
Wire Wire Line
	1450 5900 1450 6000
Connection ~ 1450 6000
Wire Wire Line
	1450 6000 1600 6000
$Comp
L Device:CP1_Small C22
U 1 1 5FAAD902
P 4250 6200
F 0 "C22" H 4341 6246 50  0000 L CNN
F 1 "10u" H 4341 6155 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 4250 6200 50  0001 C CNN
F 3 "~" H 4250 6200 50  0001 C CNN
F 4 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 4250 6200 50  0001 C CNN "Description"
F 5 "AVX Corporation" H 4250 6200 50  0001 C CNN "Manufacturer_Name"
F 6 "TAJP106M010RNJ" H 4250 6200 50  0001 C CNN "Manufacturer_Part_Number"
	1    4250 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C17
U 1 1 5FAAD908
P 3950 6200
F 0 "C17" H 3858 6246 50  0000 R CNN
F 1 "100n" H 3858 6155 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3950 6200 50  0001 C CNN
F 3 "~" H 3950 6200 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 3950 6200 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 3950 6200 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 3950 6200 50  0001 C CNN "Manufacturer_Part_Number"
	1    3950 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 6100 3950 6050
Wire Wire Line
	3950 6050 4100 6050
Wire Wire Line
	4250 6050 4250 6100
Wire Wire Line
	4250 6300 4250 6400
Wire Wire Line
	4250 6400 4100 6400
Wire Wire Line
	3950 6400 3950 6300
Connection ~ 4100 6050
Wire Wire Line
	4100 6050 4250 6050
Wire Wire Line
	3600 6000 4100 6000
Connection ~ 3600 6000
Wire Wire Line
	3600 6000 3600 6100
Wire Wire Line
	4100 6000 4100 6050
Wire Wire Line
	3600 6500 4100 6500
Wire Wire Line
	4100 6500 4100 6400
Connection ~ 3600 6500
Wire Wire Line
	3600 6500 3600 6600
Connection ~ 4100 6400
Wire Wire Line
	4100 6400 3950 6400
Wire Wire Line
	1450 6350 1450 6750
Wire Wire Line
	1450 6750 3600 6750
Wire Wire Line
	3600 6750 3600 6600
Connection ~ 1450 6350
Wire Wire Line
	1450 6350 1300 6350
Connection ~ 3600 6600
$Comp
L power:GNDA #PWR014
U 1 1 5FADD0C3
P 3600 6750
F 0 "#PWR014" H 3600 6500 50  0001 C CNN
F 1 "GNDA" H 3605 6577 50  0000 C CNN
F 2 "" H 3600 6750 50  0001 C CNN
F 3 "" H 3600 6750 50  0001 C CNN
	1    3600 6750
	1    0    0    -1  
$EndComp
Connection ~ 3600 6750
Connection ~ 4100 6000
Text Label 4200 5850 0    50   ~ 0
+3vRef
Wire Wire Line
	4100 6000 4100 5850
Wire Wire Line
	4100 5850 4200 5850
$Comp
L Device:C_Small C1
U 1 1 5FA99911
P 850 10300
F 0 "C1" V 1079 10300 50  0000 C CNN
F 1 "100n" V 988 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 850 10300 50  0001 C CNN
F 3 "~" H 850 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 850 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 850 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 850 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    850  10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	950  10850 1100 10850
Wire Wire Line
	1100 10850 1100 10800
$Comp
L power:GNDA #PWR02
U 1 1 5FB16BC2
P 650 10950
F 0 "#PWR02" H 650 10700 50  0001 C CNN
F 1 "GNDA" H 655 10777 50  0000 C CNN
F 2 "" H 650 10950 50  0001 C CNN
F 3 "" H 650 10950 50  0001 C CNN
	1    650  10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  10300 1100 10300
Wire Wire Line
	1100 10300 1100 10350
$Comp
L power:GNDA #PWR01
U 1 1 5FB3F4E3
P 650 10400
F 0 "#PWR01" H 650 10150 50  0001 C CNN
F 1 "GNDA" H 655 10227 50  0000 C CNN
F 2 "" H 650 10400 50  0001 C CNN
F 3 "" H 650 10400 50  0001 C CNN
	1    650  10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  10300 650  10400
Wire Wire Line
	650  10300 750  10300
Wire Wire Line
	650  10850 650  10950
Wire Wire Line
	650  10850 750  10850
$Comp
L Device:C_Small C2
U 1 1 5FBCF285
P 850 10850
F 0 "C2" V 1079 10850 50  0000 C CNN
F 1 "100n" V 988 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 850 10850 50  0001 C CNN
F 3 "~" H 850 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 850 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 850 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 850 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    850  10850
	0    -1   -1   0   
$EndComp
Wire Notes Line
	2700 1100 4600 1100
Wire Notes Line
	4600 3050 2700 3050
Text Notes 3700 2500 0    50   ~ 0
Capacitores C0G
NoConn ~ -9250 10200
Connection ~ 1100 10300
Connection ~ 1100 10850
Text Label 1300 10300 2    50   ~ 0
+15v
Wire Wire Line
	1100 10300 1300 10300
Text Label 1300 10850 2    50   ~ 0
-15v
Wire Wire Line
	1100 10850 1300 10850
$Comp
L Device:C_Small C7
U 1 1 6010B2D6
P 1950 10300
F 0 "C7" V 2179 10300 50  0000 C CNN
F 1 "100n" V 2088 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1950 10300 50  0001 C CNN
F 3 "~" H 1950 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1950 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 1950 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 1950 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    1950 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2050 10850 2200 10850
Wire Wire Line
	2200 10850 2200 10800
$Comp
L power:GNDA #PWR08
U 1 1 6010B2DE
P 1750 10950
F 0 "#PWR08" H 1750 10700 50  0001 C CNN
F 1 "GNDA" H 1755 10777 50  0000 C CNN
F 2 "" H 1750 10950 50  0001 C CNN
F 3 "" H 1750 10950 50  0001 C CNN
	1    1750 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 10300 2200 10300
Wire Wire Line
	2200 10300 2200 10350
$Comp
L power:GNDA #PWR07
U 1 1 6010B2E6
P 1750 10400
F 0 "#PWR07" H 1750 10150 50  0001 C CNN
F 1 "GNDA" H 1755 10227 50  0000 C CNN
F 2 "" H 1750 10400 50  0001 C CNN
F 3 "" H 1750 10400 50  0001 C CNN
	1    1750 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 10300 1750 10400
Wire Wire Line
	1750 10300 1850 10300
Wire Wire Line
	1750 10850 1750 10950
Wire Wire Line
	1750 10850 1850 10850
$Comp
L Device:C_Small C8
U 1 1 6010B2F0
P 1950 10850
F 0 "C8" V 2179 10850 50  0000 C CNN
F 1 "100n" V 2088 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1950 10850 50  0001 C CNN
F 3 "~" H 1950 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1950 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 1950 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 1950 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    1950 10850
	0    -1   -1   0   
$EndComp
Connection ~ 2200 10300
Connection ~ 2200 10850
Text Label 2400 10300 2    50   ~ 0
+15v
Wire Wire Line
	2200 10300 2400 10300
Text Label 2400 10850 2    50   ~ 0
-15v
Wire Wire Line
	2200 10850 2400 10850
$Comp
L Device:C_Small C12
U 1 1 60123088
P 3050 10300
F 0 "C12" V 3279 10300 50  0000 C CNN
F 1 "100n" V 3188 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3050 10300 50  0001 C CNN
F 3 "~" H 3050 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 3050 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 3050 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 3050 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    3050 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 10850 3300 10850
Wire Wire Line
	3300 10850 3300 10800
$Comp
L power:GNDA #PWR011
U 1 1 60123090
P 2850 10950
F 0 "#PWR011" H 2850 10700 50  0001 C CNN
F 1 "GNDA" H 2855 10777 50  0000 C CNN
F 2 "" H 2850 10950 50  0001 C CNN
F 3 "" H 2850 10950 50  0001 C CNN
	1    2850 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 10300 3300 10300
Wire Wire Line
	3300 10300 3300 10350
$Comp
L power:GNDA #PWR010
U 1 1 60123098
P 2850 10400
F 0 "#PWR010" H 2850 10150 50  0001 C CNN
F 1 "GNDA" H 2855 10227 50  0000 C CNN
F 2 "" H 2850 10400 50  0001 C CNN
F 3 "" H 2850 10400 50  0001 C CNN
	1    2850 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 10300 2850 10400
Wire Wire Line
	2850 10300 2950 10300
Wire Wire Line
	2850 10850 2950 10850
$Comp
L Device:C_Small C13
U 1 1 601230A2
P 3050 10850
F 0 "C13" V 3279 10850 50  0000 C CNN
F 1 "100n" V 3188 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3050 10850 50  0001 C CNN
F 3 "~" H 3050 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 3050 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 3050 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 3050 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    3050 10850
	0    -1   -1   0   
$EndComp
Connection ~ 3300 10300
Connection ~ 3300 10850
Text Label 3500 10300 2    50   ~ 0
+15v
Wire Wire Line
	3300 10300 3500 10300
Text Label 3500 10850 2    50   ~ 0
-15v
Wire Wire Line
	3300 10850 3500 10850
$Comp
L Device:C_Small C18
U 1 1 601230B4
P 4150 10300
F 0 "C18" V 4379 10300 50  0000 C CNN
F 1 "100n" V 4288 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4150 10300 50  0001 C CNN
F 3 "~" H 4150 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 4150 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 4150 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 4150 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    4150 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 10850 4400 10850
Wire Wire Line
	4400 10850 4400 10800
$Comp
L power:GNDA #PWR016
U 1 1 601230BC
P 3950 10950
F 0 "#PWR016" H 3950 10700 50  0001 C CNN
F 1 "GNDA" H 3955 10777 50  0000 C CNN
F 2 "" H 3950 10950 50  0001 C CNN
F 3 "" H 3950 10950 50  0001 C CNN
	1    3950 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 10300 4400 10300
Wire Wire Line
	4400 10300 4400 10350
$Comp
L power:GNDA #PWR015
U 1 1 601230C4
P 3950 10400
F 0 "#PWR015" H 3950 10150 50  0001 C CNN
F 1 "GNDA" H 3955 10227 50  0000 C CNN
F 2 "" H 3950 10400 50  0001 C CNN
F 3 "" H 3950 10400 50  0001 C CNN
	1    3950 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 10300 3950 10400
Wire Wire Line
	3950 10300 4050 10300
Wire Wire Line
	3950 10850 3950 10950
Wire Wire Line
	3950 10850 4050 10850
$Comp
L Device:C_Small C19
U 1 1 601230CE
P 4150 10850
F 0 "C19" V 4379 10850 50  0000 C CNN
F 1 "100n" V 4288 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4150 10850 50  0001 C CNN
F 3 "~" H 4150 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 4150 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 4150 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 4150 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    4150 10850
	0    -1   -1   0   
$EndComp
Connection ~ 4400 10300
Connection ~ 4400 10850
Text Label 4600 10300 2    50   ~ 0
+15v
Wire Wire Line
	4400 10300 4600 10300
Text Label 4600 10850 2    50   ~ 0
-15v
Wire Wire Line
	4400 10850 4600 10850
$Comp
L Device:C_Small C23
U 1 1 60142CE4
P 5250 10300
F 0 "C23" V 5479 10300 50  0000 C CNN
F 1 "100n" V 5388 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5250 10300 50  0001 C CNN
F 3 "~" H 5250 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 5250 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 5250 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 5250 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    5250 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 10850 5500 10850
Wire Wire Line
	5500 10850 5500 10800
$Comp
L power:GNDA #PWR020
U 1 1 60142CEC
P 5050 10950
F 0 "#PWR020" H 5050 10700 50  0001 C CNN
F 1 "GNDA" H 5055 10777 50  0000 C CNN
F 2 "" H 5050 10950 50  0001 C CNN
F 3 "" H 5050 10950 50  0001 C CNN
	1    5050 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 10300 5500 10300
Wire Wire Line
	5500 10300 5500 10350
$Comp
L power:GNDA #PWR019
U 1 1 60142CF4
P 5050 10400
F 0 "#PWR019" H 5050 10150 50  0001 C CNN
F 1 "GNDA" H 5055 10227 50  0000 C CNN
F 2 "" H 5050 10400 50  0001 C CNN
F 3 "" H 5050 10400 50  0001 C CNN
	1    5050 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 10300 5050 10400
Wire Wire Line
	5050 10300 5150 10300
Wire Wire Line
	5050 10850 5050 10950
Wire Wire Line
	5050 10850 5150 10850
$Comp
L Device:C_Small C24
U 1 1 60142CFE
P 5250 10850
F 0 "C24" V 5479 10850 50  0000 C CNN
F 1 "100n" V 5388 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5250 10850 50  0001 C CNN
F 3 "~" H 5250 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 5250 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 5250 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 5250 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    5250 10850
	0    -1   -1   0   
$EndComp
Connection ~ 5500 10300
Connection ~ 5500 10850
Text Label 5700 10300 2    50   ~ 0
+15v
Wire Wire Line
	5500 10300 5700 10300
Text Label 5700 10850 2    50   ~ 0
-15v
Wire Wire Line
	5500 10850 5700 10850
$Comp
L Device:C_Small C28
U 1 1 60142D10
P 6350 10300
F 0 "C28" V 6579 10300 50  0000 C CNN
F 1 "100n" V 6488 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6350 10300 50  0001 C CNN
F 3 "~" H 6350 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 6350 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 6350 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 6350 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    6350 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 10850 6600 10850
Wire Wire Line
	6600 10850 6600 10800
$Comp
L power:GNDA #PWR023
U 1 1 60142D18
P 6150 10950
F 0 "#PWR023" H 6150 10700 50  0001 C CNN
F 1 "GNDA" H 6155 10777 50  0000 C CNN
F 2 "" H 6150 10950 50  0001 C CNN
F 3 "" H 6150 10950 50  0001 C CNN
	1    6150 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 10300 6600 10300
Wire Wire Line
	6600 10300 6600 10350
$Comp
L power:GNDA #PWR022
U 1 1 60142D20
P 6150 10400
F 0 "#PWR022" H 6150 10150 50  0001 C CNN
F 1 "GNDA" H 6155 10227 50  0000 C CNN
F 2 "" H 6150 10400 50  0001 C CNN
F 3 "" H 6150 10400 50  0001 C CNN
	1    6150 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 10300 6150 10400
Wire Wire Line
	6150 10300 6250 10300
Wire Wire Line
	6150 10850 6150 10950
Wire Wire Line
	6150 10850 6250 10850
$Comp
L Device:C_Small C29
U 1 1 60142D2A
P 6350 10850
F 0 "C29" V 6579 10850 50  0000 C CNN
F 1 "100n" V 6488 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6350 10850 50  0001 C CNN
F 3 "~" H 6350 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 6350 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 6350 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 6350 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    6350 10850
	0    -1   -1   0   
$EndComp
Connection ~ 6600 10300
Connection ~ 6600 10850
Text Label 6800 10300 2    50   ~ 0
+15v
Wire Wire Line
	6600 10300 6800 10300
Text Label 6800 10850 2    50   ~ 0
-15v
Wire Wire Line
	6600 10850 6800 10850
Wire Wire Line
	1850 2200 1700 2200
Connection ~ 2500 2100
Wire Wire Line
	2500 2100 2450 2100
Wire Wire Line
	2500 1700 2500 2100
Wire Wire Line
	1650 2000 1650 1700
Wire Wire Line
	1850 2000 1650 2000
Wire Wire Line
	1700 4250 2600 4250
Wire Wire Line
	2500 4350 2250 4350
Connection ~ 2500 4350
Wire Wire Line
	2500 4450 2650 4450
Connection ~ 2500 4450
Wire Wire Line
	2500 4450 2500 4350
Wire Wire Line
	2350 4450 2500 4450
Text Notes 2000 4800 0    50   ~ 0
Ceramico\n
Text Notes 2700 4800 0    50   ~ 0
Tantalio
Wire Wire Line
	2500 4850 2350 4850
Connection ~ 2500 4850
$Comp
L power:GNDA #PWR09
U 1 1 604571D9
P 2500 4850
F 0 "#PWR09" H 2500 4600 50  0001 C CNN
F 1 "GNDA" H 2505 4677 50  0000 C CNN
F 2 "" H 2500 4850 50  0001 C CNN
F 3 "" H 2500 4850 50  0001 C CNN
	1    2500 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4850 2500 4850
Wire Wire Line
	2650 4450 2650 4550
Wire Wire Line
	2350 4450 2350 4550
Wire Wire Line
	2350 4850 2350 4750
Wire Wire Line
	2650 4750 2650 4850
$Comp
L Device:C_Small C9
U 1 1 604571CB
P 2350 4650
F 0 "C9" H 2258 4696 50  0000 R CNN
F 1 "100n" H 2258 4605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2350 4650 50  0001 C CNN
F 3 "~" H 2350 4650 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 2350 4650 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 2350 4650 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 2350 4650 50  0001 C CNN "Manufacturer_Part_Number"
	1    2350 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C11
U 1 1 604571C5
P 2650 4650
F 0 "C11" H 2741 4696 50  0000 L CNN
F 1 "10u" H 2741 4605 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 2650 4650 50  0001 C CNN
F 3 "~" H 2650 4650 50  0001 C CNN
F 4 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 2650 4650 50  0001 C CNN "Description"
F 5 "AVX Corporation" H 2650 4650 50  0001 C CNN "Manufacturer_Name"
F 6 "TAJP106M010RNJ" H 2650 4650 50  0001 C CNN "Manufacturer_Part_Number"
	1    2650 4650
	1    0    0    -1  
$EndComp
Text Notes 1050 4600 0    50   ~ 0
Ceramico\n
Text Notes 1750 4600 0    50   ~ 0
Tantalio
Wire Wire Line
	1700 4250 1400 4250
Connection ~ 1700 4250
Wire Wire Line
	1700 4250 1700 4350
Wire Wire Line
	1400 4250 1400 4350
Wire Wire Line
	1400 4650 1400 4550
Wire Wire Line
	1700 4550 1700 4650
$Comp
L Device:C_Small C4
U 1 1 603C84A6
P 1400 4450
F 0 "C4" H 1308 4496 50  0000 R CNN
F 1 "100n" H 1308 4405 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1400 4450 50  0001 C CNN
F 3 "~" H 1400 4450 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1400 4450 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 1400 4450 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 1400 4450 50  0001 C CNN "Manufacturer_Part_Number"
	1    1400 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C6
U 1 1 603C84A0
P 1700 4450
F 0 "C6" H 1791 4496 50  0000 L CNN
F 1 "10u" H 1791 4405 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 1700 4450 50  0001 C CNN
F 3 "~" H 1700 4450 50  0001 C CNN
F 4 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 1700 4450 50  0001 C CNN "Description"
F 5 "AVX Corporation" H 1700 4450 50  0001 C CNN "Manufacturer_Name"
F 6 "TAJP106M010RNJ" H 1700 4450 50  0001 C CNN "Manufacturer_Part_Number"
	1    1700 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3850 2250 3850
Wire Wire Line
	2600 3750 2250 3750
Wire Wire Line
	2250 3650 2600 3650
Text Label 2250 3850 0    50   ~ 0
MOSI
Text Label 2250 3750 0    50   ~ 0
SCLK
Text Label 2250 3650 0    50   ~ 0
DAC_CS
Text Label 2250 4350 0    50   ~ 0
+3vRef
Wire Wire Line
	2600 4350 2500 4350
Wire Wire Line
	4400 4150 4200 4150
Wire Wire Line
	4400 4250 4400 4150
Wire Wire Line
	4650 4050 4200 4050
Wire Wire Line
	4650 4250 4650 4050
$Comp
L power:GNDA #PWR018
U 1 1 602B64FE
P 4650 4250
F 0 "#PWR018" H 4650 4000 50  0001 C CNN
F 1 "GNDA" H 4655 4077 50  0000 C CNN
F 2 "" H 4650 4250 50  0001 C CNN
F 3 "" H 4650 4250 50  0001 C CNN
	1    4650 4250
	1    0    0    -1  
$EndComp
$Comp
L DAC8830ICD:DAC8830ICD U3
U 1 1 60272A74
P 2600 3650
F 0 "U3" H 3400 4037 60  0000 C CNN
F 1 "DAC8830ICD" H 3400 3931 60  0000 C CNN
F 2 "FootPrints:DAC8830ICD" H 3400 3890 60  0001 C CNN
F 3 "" H 2600 3650 60  0000 C CNN
F 4 "16 Bit Digital to Analog Converter 1 8-SOIC" H 2600 3650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 2600 3650 50  0001 C CNN "Manufacturer_Name"
F 6 "DAC8830ICD" H 2600 3650 50  0001 C CNN "Manufacturer_Part_Number"
	1    2600 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D4
U 1 1 5FFD0FA8
P 11950 2850
F 0 "D4" H 11950 2633 50  0000 C CNN
F 1 "VS-10MQ100-M3/5AT" H 11950 2724 50  0000 C CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 11950 2850 50  0001 C CNN
F 3 "~" H 11950 2850 50  0001 C CNN
F 4 "Diode Schottky 100V 1A Surface Mount DO-214AC (SMA)" H 11950 2850 50  0001 C CNN "Description"
F 5 "Vishay Semiconductor Diodes Division" H 11950 2850 50  0001 C CNN "Manufacturer_Name"
F 6 "VS-10MQ100-M3/5AT" H 11950 2850 50  0001 C CNN "Manufacturer_Part_Number"
	1    11950 2850
	1    0    0    -1  
$EndComp
Text Label 5600 800  0    50   ~ 0
+15v
$Comp
L Device:R R50
U 1 1 5FA00F9C
P 13150 3100
F 0 "R50" V 13050 3100 50  0000 C CNN
F 1 "2.2k" V 13150 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13080 3100 50  0001 C CNN
F 3 "~" H 13150 3100 50  0001 C CNN
F 4 "2.2 kOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13150 3100 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 13150 3100 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PA3F2201V" H 13150 3100 50  0001 C CNN "Manufacturer_Part_Number"
	1    13150 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12400 2550 12500 2550
Connection ~ 12400 2550
Wire Wire Line
	12400 2850 12400 2550
Wire Wire Line
	12100 2850 12400 2850
Wire Wire Line
	11500 2550 11400 2550
Connection ~ 11500 2550
Wire Wire Line
	11500 2850 11500 2550
Wire Wire Line
	11800 2850 11500 2850
$Comp
L Transistor_BJT:MMBT3904 Q7
U 1 1 5F9FA539
P 12700 2650
F 0 "Q7" V 12935 2650 50  0000 C CNN
F 1 "MMBT3904" V 12850 2350 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 12900 2575 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 12700 2650 50  0001 L CNN
F 4 "Bipolar (BJT) Transistor NPN 40V 200mA 300MHz 250mW Surface Mount TO-236AB" H 12700 2650 50  0001 C CNN "Description"
F 5 "NexPeria USA" H 12700 2650 50  0001 C CNN "Manufacturer_Name"
F 6 "MMBT3904,215" H 12700 2650 50  0001 C CNN "Manufacturer_Part_Number"
	1    12700 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDA #PWR051
U 1 1 5F9F7C94
P 13050 2550
F 0 "#PWR051" H 13050 2300 50  0001 C CNN
F 1 "GNDA" H 13055 2377 50  0000 C CNN
F 2 "" H 13050 2550 50  0001 C CNN
F 3 "" H 13050 2550 50  0001 C CNN
	1    13050 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	12250 2550 12400 2550
Text Label 11400 2550 2    50   ~ 0
+12v
Wire Wire Line
	11650 2550 11500 2550
Text Label 12400 2200 2    50   ~ 0
V+
Text Label 11500 2100 2    50   ~ 0
IN+
$Comp
L Device:C_Small C35
U 1 1 608E3679
P 7450 10300
F 0 "C35" V 7679 10300 50  0000 C CNN
F 1 "100n" V 7588 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7450 10300 50  0001 C CNN
F 3 "~" H 7450 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 7450 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 7450 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 7450 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    7450 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 10850 7700 10850
Wire Wire Line
	7700 10850 7700 10800
$Comp
L power:GNDA #PWR031
U 1 1 608E3681
P 7250 10950
F 0 "#PWR031" H 7250 10700 50  0001 C CNN
F 1 "GNDA" H 7255 10777 50  0000 C CNN
F 2 "" H 7250 10950 50  0001 C CNN
F 3 "" H 7250 10950 50  0001 C CNN
	1    7250 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 10300 7700 10300
Wire Wire Line
	7700 10300 7700 10350
$Comp
L power:GNDA #PWR030
U 1 1 608E3689
P 7250 10400
F 0 "#PWR030" H 7250 10150 50  0001 C CNN
F 1 "GNDA" H 7255 10227 50  0000 C CNN
F 2 "" H 7250 10400 50  0001 C CNN
F 3 "" H 7250 10400 50  0001 C CNN
	1    7250 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 10300 7250 10400
Wire Wire Line
	7250 10300 7350 10300
Wire Wire Line
	7250 10850 7250 10950
Wire Wire Line
	7250 10850 7350 10850
$Comp
L Device:C_Small C36
U 1 1 608E3693
P 7450 10850
F 0 "C36" V 7679 10850 50  0000 C CNN
F 1 "100n" V 7588 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7450 10850 50  0001 C CNN
F 3 "~" H 7450 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 7450 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 7450 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 7450 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    7450 10850
	0    -1   -1   0   
$EndComp
Connection ~ 7700 10300
Connection ~ 7700 10850
Wire Wire Line
	7700 10300 7900 10300
Text Label 8000 10850 2    50   ~ 0
-0.33v
Wire Wire Line
	5900 3900 6150 3900
Wire Wire Line
	5300 3900 5600 3900
Wire Wire Line
	6000 4400 6150 4400
Wire Wire Line
	6150 4750 6150 4850
Wire Wire Line
	6150 4450 6150 4400
Connection ~ 6150 4400
Wire Wire Line
	6150 4400 6300 4400
$Comp
L ADS8330IBPW:ADS8330IBPW U11
U 1 1 5FA7DE0B
P 8900 6750
F 0 "U11" H 8900 7738 60  0000 C CNN
F 1 "ADS8330IBPW" H 8900 7632 60  0000 C CNN
F 2 "FootPrints:ADS8330IBPW" H 8900 6690 60  0001 C CNN
F 3 "" H 8900 6750 60  0000 C CNN
F 4 "16 Bit Analog to Digital Converter 2 Input 1 SAR 16-TSSOP" H 8900 6750 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 8900 6750 50  0001 C CNN "Manufacturer_Name"
F 6 "ADS8330IBPW" H 8900 6750 50  0001 C CNN "Manufacturer_Part_Number"
	1    8900 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 6150 10150 6150
Text Label 10150 6150 2    50   ~ 0
ADC_CS
Wire Wire Line
	9800 6250 10150 6250
Wire Wire Line
	9800 6350 10150 6350
Wire Wire Line
	9800 6450 10150 6450
Text Label 10150 6250 2    50   ~ 0
SCLK
Text Label 10150 6350 2    50   ~ 0
MOSI
Text Label 10150 6450 2    50   ~ 0
MISO
Wire Wire Line
	9800 6550 10150 6550
Wire Wire Line
	9800 6650 10150 6650
Text Label 10150 6550 2    50   ~ 0
CONVST
Text Label 10150 6650 2    50   ~ 0
EOC_INT
Wire Wire Line
	9800 7350 10000 7350
Wire Wire Line
	10000 7350 10000 7450
Text Label 7450 6150 0    50   ~ 0
+5v_an
$Comp
L power:GNDA #PWR047
U 1 1 5FEEEBF4
P 10250 7450
F 0 "#PWR047" H 10250 7200 50  0001 C CNN
F 1 "GNDA" H 10255 7277 50  0000 C CNN
F 2 "" H 10250 7450 50  0001 C CNN
F 3 "" H 10250 7450 50  0001 C CNN
	1    10250 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 7150 7800 7150
NoConn ~ 8000 7350
$Comp
L power:GNDA #PWR033
U 1 1 5FF67498
P 7800 7150
F 0 "#PWR033" H 7800 6900 50  0001 C CNN
F 1 "GNDA" H 7805 6977 50  0000 C CNN
F 2 "" H 7800 7150 50  0001 C CNN
F 3 "" H 7800 7150 50  0001 C CNN
	1    7800 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C39
U 1 1 60B4EECD
P 7900 7650
F 0 "C39" H 7808 7696 50  0000 R CNN
F 1 "4.7u" H 7808 7605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7900 7650 50  0001 C CNN
F 3 "~" H 7900 7650 50  0001 C CNN
F 4 "4.7µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 7900 7650 50  0001 C CNN "Description"
F 5 "TDK Corporation" H 7900 7650 50  0001 C CNN "Manufacturer_Name"
F 6 "C2012X7R1H475K125AE" H 7900 7650 50  0001 C CNN "Manufacturer_Part_Number"
	1    7900 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 60BDD7E4
P 5950 6050
F 0 "R16" V 5850 6050 50  0000 C CNN
F 1 "20" V 5950 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5880 6050 50  0001 C CNN
F 3 "~" H 5950 6050 50  0001 C CNN
F 4 "20 Ohms ±0.1% 0.167W, 1/6W Chip Resistor 0603 (1608 Metric)  Thin Film" H 5950 6050 50  0001 C CNN "Description"
F 5 "TE Connectivity Passive Product" H 5950 6050 50  0001 C CNN "Manufacturer_Name"
F 6 "RP73PF1J20RBTDF" H 5950 6050 50  0001 C CNN "Manufacturer_Part_Number"
	1    5950 6050
	0    -1   1    0   
$EndComp
$Comp
L Device:C_Small C26
U 1 1 60BDE0AC
P 6200 6250
F 0 "C26" H 6108 6296 50  0000 R CNN
F 1 "470p" H 6108 6205 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6200 6250 50  0001 C CNN
F 3 "~" H 6200 6250 50  0001 C CNN
F 4 "470pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 6200 6250 50  0001 C CNN "Description"
F 5 "KEMET" H 6200 6250 50  0001 C CNN "Manufacturer_Name"
F 6 "C0603C471J5GACTU" H 6200 6250 50  0001 C CNN "Manufacturer_Part_Number"
	1    6200 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 6050 6200 6050
Wire Wire Line
	6200 6050 6200 6150
$Comp
L power:GNDA #PWR024
U 1 1 60C0C02F
P 6200 6400
F 0 "#PWR024" H 6200 6150 50  0001 C CNN
F 1 "GNDA" H 6205 6227 50  0000 C CNN
F 2 "" H 6200 6400 50  0001 C CNN
F 3 "" H 6200 6400 50  0001 C CNN
	1    6200 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 6400 6200 6350
Text Label 4800 7150 0    50   ~ 0
Imon
$Comp
L Device:R R17
U 1 1 60C399F6
P 5950 7250
F 0 "R17" V 5850 7250 50  0000 C CNN
F 1 "20" V 5950 7250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5880 7250 50  0001 C CNN
F 3 "~" H 5950 7250 50  0001 C CNN
F 4 "20 Ohms ±0.1% 0.167W, 1/6W Chip Resistor 0603 (1608 Metric)  Thin Film" H 5950 7250 50  0001 C CNN "Description"
F 5 "TE Connectivity Passive Product" H 5950 7250 50  0001 C CNN "Manufacturer_Name"
F 6 "RP73PF1J20RBTDF" H 5950 7250 50  0001 C CNN "Manufacturer_Part_Number"
	1    5950 7250
	0    -1   1    0   
$EndComp
$Comp
L Device:C_Small C27
U 1 1 60C399FC
P 6200 7450
F 0 "C27" H 6108 7496 50  0000 R CNN
F 1 "470p" H 6108 7405 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6200 7450 50  0001 C CNN
F 3 "~" H 6200 7450 50  0001 C CNN
F 4 "470pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 6200 7450 50  0001 C CNN "Description"
F 5 "KEMET" H 6200 7450 50  0001 C CNN "Manufacturer_Name"
F 6 "C0603C471J5GACTU" H 6200 7450 50  0001 C CNN "Manufacturer_Part_Number"
	1    6200 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 7250 6200 7250
Wire Wire Line
	6200 7250 6200 7350
$Comp
L power:GNDA #PWR025
U 1 1 60C39A04
P 6200 7600
F 0 "#PWR025" H 6200 7350 50  0001 C CNN
F 1 "GNDA" H 6205 7427 50  0000 C CNN
F 2 "" H 6200 7600 50  0001 C CNN
F 3 "" H 6200 7600 50  0001 C CNN
	1    6200 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 7600 6200 7550
Wire Wire Line
	9800 7150 10250 7150
Connection ~ 10250 7150
Wire Notes Line
	7450 3750 10250 3750
Text Notes 9550 4700 0    50   ~ 0
G = 0.1 V/V\n1v=0.1v\nFondo de escala:\n30v=3v
Wire Wire Line
	9400 2900 9500 2900
$Comp
L Device:R R28
U 1 1 5F9F853E
P 8250 2800
F 0 "R28" V 8150 2800 50  0000 C CNN
F 1 "10k" V 8250 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8180 2800 50  0001 C CNN
F 3 "~" H 8250 2800 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8250 2800 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8250 2800 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 8250 2800 50  0001 C CNN "Manufacturer_Part_Number"
	1    8250 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 2600 9000 2600
Wire Wire Line
	9300 2600 9500 2600
Wire Wire Line
	9500 2600 9500 2900
$Comp
L Device:R R24
U 1 1 5FD7601F
P 8200 3300
F 0 "R24" V 8100 3300 50  0000 C CNN
F 1 "10k" V 8200 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 3300 50  0001 C CNN
F 3 "~" H 8200 3300 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8200 3300 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8200 3300 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 8200 3300 50  0001 C CNN "Manufacturer_Part_Number"
	1    8200 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R30
U 1 1 5FD76C20
P 8400 3300
F 0 "R30" V 8300 3300 50  0000 C CNN
F 1 "10k" V 8400 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8330 3300 50  0001 C CNN
F 3 "~" H 8400 3300 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8400 3300 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8400 3300 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 8400 3300 50  0001 C CNN "Manufacturer_Part_Number"
	1    8400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 3450 8200 3500
Wire Wire Line
	8400 3450 8400 3500
Wire Wire Line
	8400 2800 8550 2800
Wire Wire Line
	10250 6850 10250 7150
Wire Wire Line
	5550 6050 5750 6050
Wire Wire Line
	5750 6050 5750 6300
Wire Wire Line
	4950 7350 4800 7350
Wire Wire Line
	4800 7350 4800 7500
Wire Wire Line
	4800 7500 5750 7500
Wire Wire Line
	5750 7500 5750 7250
Wire Wire Line
	5800 6050 5750 6050
Connection ~ 5750 6050
Wire Wire Line
	5750 7250 5800 7250
Wire Wire Line
	5550 7250 5750 7250
Connection ~ 5750 7250
Wire Wire Line
	4950 7150 4800 7150
Wire Wire Line
	4750 5950 4950 5950
Text Label 7550 4250 0    50   ~ 0
Vmon
Text Label 9700 2900 2    50   ~ 0
Imon
Wire Wire Line
	9500 2900 9700 2900
Connection ~ 9500 2900
Wire Wire Line
	6200 6050 6400 6050
Connection ~ 6200 6050
Wire Wire Line
	6200 7250 6400 7250
Connection ~ 6200 7250
Wire Wire Line
	6400 7250 6400 6750
Wire Wire Line
	8700 2800 8800 2800
Wire Wire Line
	6400 6550 6400 6050
Text Label 7200 6950 0    50   ~ 0
+3vRef
$Comp
L Device:C_Small C41
U 1 1 608805C4
P 8550 10300
F 0 "C41" V 8779 10300 50  0000 C CNN
F 1 "100n" V 8688 10300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8550 10300 50  0001 C CNN
F 3 "~" H 8550 10300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 8550 10300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 8550 10300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 8550 10300 50  0001 C CNN "Manufacturer_Part_Number"
	1    8550 10300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 10850 8800 10850
Wire Wire Line
	8800 10850 8800 10800
$Comp
L power:GNDA #PWR036
U 1 1 608805CC
P 8350 10950
F 0 "#PWR036" H 8350 10700 50  0001 C CNN
F 1 "GNDA" H 8355 10777 50  0000 C CNN
F 2 "" H 8350 10950 50  0001 C CNN
F 3 "" H 8350 10950 50  0001 C CNN
	1    8350 10950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 10300 8800 10300
Wire Wire Line
	8800 10300 8800 10350
$Comp
L power:GNDA #PWR035
U 1 1 608805D4
P 8350 10400
F 0 "#PWR035" H 8350 10150 50  0001 C CNN
F 1 "GNDA" H 8355 10227 50  0000 C CNN
F 2 "" H 8350 10400 50  0001 C CNN
F 3 "" H 8350 10400 50  0001 C CNN
	1    8350 10400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 10300 8350 10400
Wire Wire Line
	8350 10300 8450 10300
Wire Wire Line
	8350 10850 8350 10950
Wire Wire Line
	8350 10850 8450 10850
$Comp
L Device:C_Small C42
U 1 1 608805DE
P 8550 10850
F 0 "C42" V 8779 10850 50  0000 C CNN
F 1 "100n" V 8688 10850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8550 10850 50  0001 C CNN
F 3 "~" H 8550 10850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 8550 10850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 8550 10850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 8550 10850 50  0001 C CNN "Manufacturer_Part_Number"
	1    8550 10850
	0    -1   -1   0   
$EndComp
Connection ~ 8800 10300
Connection ~ 8800 10850
Text Label 9000 10300 2    50   ~ 0
+5v_an
Wire Wire Line
	8800 10300 9000 10300
Text Label 9100 10850 2    50   ~ 0
-0.33v
$Comp
L Device:C_Small C20
U 1 1 608A8859
P 4250 1400
F 0 "C20" V 4021 1400 50  0000 C CNN
F 1 "10n" V 4112 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4250 1400 50  0001 C CNN
F 3 "~" H 4250 1400 50  0001 C CNN
F 4 "10000pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 4250 1400 50  0001 C CNN "Description"
F 5 "Murata Electronics" H 4250 1400 50  0001 C CNN "Manufacturer_Name"
F 6 "GRM1885C1H103JA01D" H 4250 1400 50  0001 C CNN "Manufacturer_Part_Number"
	1    4250 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 1400 4550 1400
Wire Wire Line
	4550 1400 4550 1750
Connection ~ 4550 1750
Wire Wire Line
	4150 1400 4100 1400
Wire Wire Line
	4100 1400 4100 1750
Wire Wire Line
	4000 1750 4100 1750
Connection ~ 4100 1750
Wire Wire Line
	4100 1750 4150 1750
Wire Wire Line
	9800 6850 10250 6850
Text Notes 8300 5700 2    50   ~ 0
Ceramico\n
Text Notes 7600 5700 2    50   ~ 0
Tantalio
Wire Wire Line
	7800 5600 7950 5600
Connection ~ 7800 5600
$Comp
L power:GNDA #PWR032
U 1 1 5FBD4BED
P 7800 5600
F 0 "#PWR032" H 7800 5350 50  0001 C CNN
F 1 "GNDA" H 7805 5427 50  0000 C CNN
F 2 "" H 7800 5600 50  0001 C CNN
F 3 "" H 7800 5600 50  0001 C CNN
	1    7800 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	7650 5600 7800 5600
Wire Wire Line
	7950 5600 7950 5700
Wire Wire Line
	7650 5700 7650 5600
$Comp
L Device:C_Small C40
U 1 1 5FBD4BF6
P 7950 5800
F 0 "C40" H 7858 5846 50  0000 R CNN
F 1 "100n" H 7858 5755 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7950 5800 50  0001 C CNN
F 3 "~" H 7950 5800 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 7950 5800 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 7950 5800 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 7950 5800 50  0001 C CNN "Manufacturer_Part_Number"
	1    7950 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1_Small C38
U 1 1 5FBD4BFC
P 7650 5800
F 0 "C38" H 7741 5846 50  0000 L CNN
F 1 "10u" H 7741 5755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 7650 5800 50  0001 C CNN
F 3 "~" H 7650 5800 50  0001 C CNN
F 4 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 7650 5800 50  0001 C CNN "Description"
F 5 "AVX Corporation" H 7650 5800 50  0001 C CNN "Manufacturer_Name"
F 6 "TAJP106M010RNJ" H 7650 5800 50  0001 C CNN "Manufacturer_Part_Number"
	1    7650 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7950 5900 7950 6000
Wire Wire Line
	7650 6000 7650 5900
Text Notes 7250 5700 2    50   ~ 0
Ceramico\n
Text Notes 6600 5650 2    50   ~ 0
Tantalio
Wire Wire Line
	6900 5600 6900 5700
Wire Wire Line
	6600 5700 6600 5600
$Comp
L Device:C_Small C33
U 1 1 5FC215B7
P 6900 5800
F 0 "C33" H 6808 5846 50  0000 R CNN
F 1 "100n" H 6808 5755 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6900 5800 50  0001 C CNN
F 3 "~" H 6900 5800 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 6900 5800 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 6900 5800 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 6900 5800 50  0001 C CNN "Manufacturer_Part_Number"
	1    6900 5800
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1_Small C31
U 1 1 5FC215BD
P 6600 5800
F 0 "C31" H 6691 5846 50  0000 L CNN
F 1 "10u" H 6691 5755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 6600 5800 50  0001 C CNN
F 3 "~" H 6600 5800 50  0001 C CNN
F 4 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 6600 5800 50  0001 C CNN "Description"
F 5 "AVX Corporation" H 6600 5800 50  0001 C CNN "Manufacturer_Name"
F 6 "TAJP106M010RNJ" H 6600 5800 50  0001 C CNN "Manufacturer_Part_Number"
	1    6600 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 5900 6900 6000
Wire Wire Line
	6600 6000 6600 5900
Wire Wire Line
	7950 6000 7800 6000
Wire Wire Line
	7800 6150 7800 6000
Wire Wire Line
	7800 6150 8000 6150
Connection ~ 7800 6000
Wire Wire Line
	7800 6000 7650 6000
Connection ~ 7800 6150
Wire Wire Line
	6900 6000 6750 6000
Wire Wire Line
	6750 6350 6750 6000
Wire Wire Line
	6750 6350 8000 6350
Connection ~ 6750 6000
Wire Wire Line
	6750 6000 6600 6000
$Comp
L Device:C_Small C37
U 1 1 5FF0B888
P 7550 7650
F 0 "C37" H 7458 7696 50  0000 R CNN
F 1 "4.7u" H 7458 7605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7550 7650 50  0001 C CNN
F 3 "~" H 7550 7650 50  0001 C CNN
F 4 "4.7µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 7550 7650 50  0001 C CNN "Description"
F 5 "TDK Corporation" H 7550 7650 50  0001 C CNN "Manufacturer_Name"
F 6 "C2012X7R1H475K125AE" H 7550 7650 50  0001 C CNN "Manufacturer_Part_Number"
	1    7550 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C34
U 1 1 5FF0BBA1
P 7200 7650
F 0 "C34" H 7108 7696 50  0000 R CNN
F 1 "4.7u" H 7108 7605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7200 7650 50  0001 C CNN
F 3 "~" H 7200 7650 50  0001 C CNN
F 4 "4.7µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 7200 7650 50  0001 C CNN "Description"
F 5 "TDK Corporation" H 7200 7650 50  0001 C CNN "Manufacturer_Name"
F 6 "C2012X7R1H475K125AE" H 7200 7650 50  0001 C CNN "Manufacturer_Part_Number"
	1    7200 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C32
U 1 1 5FF0C225
P 6850 7650
F 0 "C32" H 6758 7696 50  0000 R CNN
F 1 "4.7u" H 6758 7605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6850 7650 50  0001 C CNN
F 3 "~" H 6850 7650 50  0001 C CNN
F 4 "4.7µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 6850 7650 50  0001 C CNN "Description"
F 5 "TDK Corporation" H 6850 7650 50  0001 C CNN "Manufacturer_Name"
F 6 "C2012X7R1H475K125AE" H 6850 7650 50  0001 C CNN "Manufacturer_Part_Number"
	1    6850 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 7750 7900 7900
Wire Wire Line
	7900 7900 7550 7900
Wire Wire Line
	6850 7900 6850 7750
Wire Wire Line
	6550 7750 6550 7900
Wire Wire Line
	6550 7900 6850 7900
Connection ~ 6850 7900
Wire Wire Line
	6550 7550 6550 7450
Wire Wire Line
	6550 7450 6850 7450
Wire Wire Line
	7900 7450 7900 7550
Wire Wire Line
	7550 7550 7550 7450
Connection ~ 7550 7450
Wire Wire Line
	7550 7450 7900 7450
Wire Wire Line
	7200 7550 7200 7450
Connection ~ 7200 7450
Wire Wire Line
	7200 7450 7550 7450
Wire Wire Line
	6850 7550 6850 7450
Connection ~ 6850 7450
Wire Wire Line
	6850 7450 7200 7450
Wire Wire Line
	7200 7750 7200 7900
Connection ~ 7200 7900
Wire Wire Line
	7200 7900 6850 7900
Wire Wire Line
	7550 7750 7550 7900
Connection ~ 7550 7900
Wire Wire Line
	7550 7900 7200 7900
Wire Wire Line
	7200 6950 8000 6950
$Comp
L power:GNDA #PWR029
U 1 1 600AFAB6
P 7200 7900
F 0 "#PWR029" H 7200 7650 50  0001 C CNN
F 1 "GNDA" H 7205 7727 50  0000 C CNN
F 2 "" H 7200 7900 50  0001 C CNN
F 3 "" H 7200 7900 50  0001 C CNN
	1    7200 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 6950 7200 7450
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 6025A063
P 15750 650
F 0 "J2" H 15830 642 50  0000 L CNN
F 1 "DUT" H 15650 750 50  0000 L CNN
F 2 "FootPrints:BARRIER_BLOCK_1ROW_2POS" H 15750 650 50  0001 C CNN
F 3 "~" H 15750 650 50  0001 C CNN
F 4 "2 Circuit 0.325\" (8.26mm) Barrier Block Connector, Screws with Captive Plate" H 15750 650 50  0001 C CNN "Description"
F 5 "On Shore Technology Inc." H 15750 650 50  0001 C CNN "Manufacturer_Name"
F 6 "OSTYK42102030" H 15750 650 50  0001 C CNN "Manufacturer_Part_Number"
	1    15750 650 
	1    0    0    -1  
$EndComp
Text Label 15250 650  0    50   ~ 0
IN+
Text Label 15250 750  0    50   ~ 0
IN-
$Comp
L Transistor_BJT:BCP56 Q4
U 1 1 60452380
P 5850 1300
F 0 "Q4" H 6041 1346 50  0000 L CNN
F 1 "BCP56" H 6041 1255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 6050 1225 50  0001 L CIN
F 3 "http://cache.nxp.com/documents/data_sheet/BCP56_BCX56_BC56PA.pdf?pspll=1" H 5850 1300 50  0001 L CNN
F 4 "Nexperia USA Inc." H 5850 1300 50  0001 C CNN "Manufacturer_Name"
F 5 "BCP56HX" H 5850 1300 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "Bipolar (BJT) Transistor NPN 80V 1A 100MHz  Surface Mount SOT-223-4" H 5850 1300 50  0001 C CNN "Description"
	1    5850 1300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BCP56 Q3
U 1 1 60453921
P 5350 2400
F 0 "Q3" H 5541 2446 50  0000 L CNN
F 1 "BCP56" H 5541 2355 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 5550 2325 50  0001 L CIN
F 3 "http://cache.nxp.com/documents/data_sheet/BCP56_BCX56_BC56PA.pdf?pspll=1" H 5350 2400 50  0001 L CNN
F 4 "Nexperia USA Inc." H 5350 2400 50  0001 C CNN "Manufacturer_Name"
F 5 "BCP56HX" H 5350 2400 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "Bipolar (BJT) Transistor NPN 80V 1A 100MHz  Surface Mount SOT-223-4" H 5350 2400 50  0001 C CNN "Description"
	1    5350 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6150 4750 6300
Wire Wire Line
	4750 6300 5750 6300
Wire Wire Line
	4750 6150 4950 6150
Wire Wire Line
	4200 3650 4200 3200
Wire Wire Line
	4200 3200 1700 3200
Wire Wire Line
	1700 3200 1700 2200
Wire Wire Line
	2500 2100 3050 2100
Wire Notes Line
	1000 3100 4800 3100
Wire Notes Line
	4800 3100 4800 5100
Wire Notes Line
	4800 5100 1000 5100
Wire Notes Line
	1000 5100 1000 3100
Text Notes 1100 3400 0    118  ~ 24
DAC - 16bits
Wire Notes Line
	1000 5300 4500 5300
Wire Notes Line
	4500 5300 4500 7000
Wire Notes Line
	4500 7000 1000 7000
Wire Notes Line
	1000 7000 1000 5300
Text Notes 1300 5500 0    118  ~ 24
Tensión de Ref. 3v  +/- 0.05%
Text Notes 4900 5500 0    118  ~ 24
ADC - 16bits
Wire Wire Line
	3400 8550 3550 8550
Wire Wire Line
	3400 8550 3250 8550
Connection ~ 3400 8550
Text Label 3550 8100 0    50   ~ 0
-0.33v
$Comp
L power:GNDA #PWR013
U 1 1 608046DA
P 3400 8550
F 0 "#PWR013" H 3400 8300 50  0001 C CNN
F 1 "GNDA" H 3405 8377 50  0000 C CNN
F 2 "" H 3400 8550 50  0001 C CNN
F 3 "" H 3400 8550 50  0001 C CNN
	1    3400 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 8550 3250 8400
Wire Wire Line
	3550 8400 3550 8550
Wire Wire Line
	3550 8100 3550 8200
Wire Wire Line
	3250 8100 3450 8100
$Comp
L Device:CP1_Small C16
U 1 1 607BD903
P 3550 8300
F 0 "C16" H 3459 8254 50  0000 R CNN
F 1 "10u" H 3459 8345 50  0000 R CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-15_AVX-P_Pad1.30x1.05mm_HandSolder" H 3550 8300 50  0001 C CNN
F 3 "~" H 3550 8300 50  0001 C CNN
F 4 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 3550 8300 50  0001 C CNN "Description"
F 5 "AVX Corporation" H 3550 8300 50  0001 C CNN "Manufacturer_Name"
F 6 "TAJP106M010RNJ" H 3550 8300 50  0001 C CNN "Manufacturer_Part_Number"
	1    3550 8300
	-1   0    0    1   
$EndComp
Connection ~ 3250 8100
Wire Wire Line
	3250 8200 3250 8100
$Comp
L Device:C_Small C15
U 1 1 6079B257
P 3250 8300
F 0 "C15" H 3158 8346 50  0000 R CNN
F 1 "NP" H 3158 8255 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3250 8300 50  0001 C CNN
F 3 "~" H 3250 8300 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 3250 8300 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 3250 8300 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 3250 8300 50  0001 C CNN "Manufacturer_Part_Number"
	1    3250 8300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR03
U 1 1 606E8233
P 1100 7350
F 0 "#PWR03" H 1100 7100 50  0001 C CNN
F 1 "GNDA" H 1105 7177 50  0000 C CNN
F 2 "" H 1100 7350 50  0001 C CNN
F 3 "" H 1100 7350 50  0001 C CNN
	1    1100 7350
	1    0    0    -1  
$EndComp
Text Label 1100 7800 3    50   ~ 0
+5v_an
Text Label 3250 7250 0    50   ~ 0
-15v
Wire Wire Line
	3250 7500 3250 7250
Wire Wire Line
	1650 7800 1750 7800
Wire Wire Line
	3250 8100 3250 7900
$Comp
L Device:R R4
U 1 1 605A5EE3
P 2400 8000
F 0 "R4" V 2300 8000 50  0000 C CNN
F 1 "768" V 2400 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2330 8000 50  0001 C CNN
F 3 "~" H 2400 8000 50  0001 C CNN
F 4 "768 Ohms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 2400 8000 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 2400 8000 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B7680V" H 2400 8000 50  0001 C CNN "Manufacturer_Part_Number"
	1    2400 8000
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 7700 2950 7700
Wire Wire Line
	2350 7700 2500 7700
$Comp
L Device:R R5
U 1 1 60560818
P 2650 7700
F 0 "R5" V 2550 7700 50  0000 C CNN
F 1 "10" V 2650 7700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2580 7700 50  0001 C CNN
F 3 "~" H 2650 7700 50  0001 C CNN
F 4 "10 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 2650 7700 50  0001 C CNN "Description"
F 5 "Rohm Semiconductor" H 2650 7700 50  0001 C CNN "Manufacturer_Name"
F 6 "ESR03EZPF10R0" H 2650 7700 50  0001 C CNN "Manufacturer_Part_Number"
	1    2650 7700
	0    -1   1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60E596E1
P 1400 7800
F 0 "R2" V 1300 7800 50  0000 C CNN
F 1 "11.5k" V 1400 7800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1330 7800 50  0001 C CNN
F 3 "~" H 1400 7800 50  0001 C CNN
F 4 "11.5 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 1400 7800 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 1400 7800 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1152V" H 1400 7800 50  0001 C CNN "Manufacturer_Part_Number"
	1    1400 7800
	0    -1   1    0   
$EndComp
Wire Wire Line
	1650 7800 1550 7800
Connection ~ 1650 7800
Wire Notes Line
	1000 7100 3900 7100
Wire Notes Line
	3900 7100 3900 8850
Wire Notes Line
	3900 8850 1000 8850
Wire Notes Line
	1000 8850 1000 7100
Text Notes 1050 8550 0    118  ~ 24
-0.33V bias
Text Notes 9650 2450 0    118  ~ 24
Imon
Text Notes 8850 3950 0    118  ~ 24
Vmon + Sense
Text Notes 1850 7200 0    50   ~ 0
Todas Las Resistencias 0.1%
$Comp
L Device:R R3
U 1 1 612675E0
P 1650 7400
F 0 "R3" V 1550 7400 50  0000 C CNN
F 1 "698" V 1650 7400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1580 7400 50  0001 C CNN
F 3 "~" H 1650 7400 50  0001 C CNN
F 4 "698 Ohms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 1650 7400 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 1650 7400 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B6980V" H 1650 7400 50  0001 C CNN "Manufacturer_Part_Number"
	1    1650 7400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1750 7600 1650 7600
Wire Wire Line
	1650 7600 1650 7550
Wire Notes Line
	6350 3250 5000 3250
Wire Notes Line
	5000 3250 5000 550 
Wire Notes Line
	5000 550  6350 550 
Wire Notes Line
	6350 550  6350 3250
Text Notes 5050 750  0    118  ~ 24
Driver
Text Notes 2950 1350 0    118  ~ 24
Control P-I
$Comp
L Device:R R1
U 1 1 613C8EC3
P 1350 7200
F 0 "R1" V 1250 7200 50  0000 C CNN
F 1 "14" V 1350 7200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1280 7200 50  0001 C CNN
F 3 "~" H 1350 7200 50  0001 C CNN
F 4 "14 Ohms ±0.1% 0.167W, 1/6W Chip Resistor 0603 (1608 Metric)  Thin Film" H 1350 7200 50  0001 C CNN "Description"
F 5 "TE Connectivity Passive Product" H 1350 7200 50  0001 C CNN "Manufacturer_Name"
F 6 "RP73PF1J14RBTDF" H 1350 7200 50  0001 C CNN "Manufacturer_Part_Number"
	1    1350 7200
	0    1    -1   0   
$EndComp
Wire Wire Line
	1500 7200 1650 7200
Wire Wire Line
	1650 7200 1650 7250
Wire Wire Line
	1200 7200 1100 7200
Wire Wire Line
	1100 7200 1100 7350
Wire Wire Line
	1100 7800 1250 7800
Wire Wire Line
	8800 10850 9100 10850
Wire Wire Line
	7700 10850 8000 10850
$Comp
L power:PWR_FLAG #FLG02
U 1 1 6150AB8F
P 750 750
F 0 "#FLG02" H 750 825 50  0001 C CNN
F 1 "PWR_FLAG" H 750 923 50  0000 C CNN
F 2 "" H 750 750 50  0001 C CNN
F 3 "~" H 750 750 50  0001 C CNN
	1    750  750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  750  750  850 
Wire Wire Line
	750  850  900  850 
Text Label 900  850  0    50   ~ 0
+15v
$Comp
L power:PWR_FLAG #FLG03
U 1 1 6153792D
P 750 1100
F 0 "#FLG03" H 750 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 750 1273 50  0000 C CNN
F 2 "" H 750 1100 50  0001 C CNN
F 3 "~" H 750 1100 50  0001 C CNN
	1    750  1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1100 750  1200
Wire Wire Line
	750  1200 900  1200
Text Label 900  1200 0    50   ~ 0
-15v
Text Label 1700 4250 0    50   ~ 0
+5v_dig
Text Label 1750 5700 0    50   ~ 0
+5v_an
Wire Wire Line
	6450 2450 6450 2550
Wire Wire Line
	2850 10850 2850 10950
$Comp
L power:PWR_FLAG #FLG09
U 1 1 6161E806
P 3450 8100
F 0 "#FLG09" H 3450 8175 50  0001 C CNN
F 1 "PWR_FLAG" H 3450 8273 50  0000 C CNN
F 2 "" H 3450 8100 50  0001 C CNN
F 3 "~" H 3450 8100 50  0001 C CNN
	1    3450 8100
	1    0    0    -1  
$EndComp
Connection ~ 3450 8100
Wire Wire Line
	3450 8100 3550 8100
Wire Wire Line
	11450 4100 11800 4100
Wire Wire Line
	11450 4200 11800 4200
Wire Wire Line
	11450 4300 11800 4300
Wire Wire Line
	11450 4400 11800 4400
Wire Wire Line
	11450 4600 11800 4600
Wire Wire Line
	11450 4500 11800 4500
Wire Wire Line
	11800 4700 11450 4700
Wire Wire Line
	11450 4700 11450 4800
Text Label 11050 4000 2    50   ~ 0
+5v_an
Text Label 11050 4100 2    50   ~ 0
ADC_CS
Text Label 11050 4200 2    50   ~ 0
CONVST
Text Label 11050 4300 2    50   ~ 0
DAC_CS
Text Label 10950 4400 2    50   ~ 0
SCLK
Text Label 10950 4500 2    50   ~ 0
MOSI
Text Label 10950 4600 2    50   ~ 0
MISO
Wire Wire Line
	13150 4100 12900 4100
Wire Wire Line
	13150 4200 12900 4200
Wire Wire Line
	13150 4300 12900 4300
Wire Wire Line
	13150 4400 12900 4400
Wire Wire Line
	13150 4500 12900 4500
Wire Wire Line
	13150 4600 12900 4600
$Comp
L Device:C_Small C51
U 1 1 61CFFEB4
P 13150 3850
F 0 "C51" H 13058 3896 50  0000 R CNN
F 1 "100n" H 13058 3805 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13150 3850 50  0001 C CNN
F 3 "~" H 13150 3850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 13150 3850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 13150 3850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 13150 3850 50  0001 C CNN "Manufacturer_Part_Number"
	1    13150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	13400 3700 13400 3750
Wire Wire Line
	13150 3700 13400 3700
Wire Wire Line
	13150 3750 13150 3700
$Comp
L Device:C_Small C50
U 1 1 61D9D7F0
P 11650 3850
F 0 "C50" H 11558 3896 50  0000 R CNN
F 1 "100n" H 11558 3805 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11650 3850 50  0001 C CNN
F 3 "~" H 11650 3850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 11650 3850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 11650 3850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 11650 3850 50  0001 C CNN "Manufacturer_Part_Number"
	1    11650 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	11650 3700 11400 3700
Wire Wire Line
	11650 3750 11650 3700
Wire Wire Line
	11650 3950 11650 4000
Connection ~ 11650 4000
Wire Wire Line
	11650 4000 11800 4000
Wire Wire Line
	11400 3700 11400 3750
$Comp
L power:PWR_FLAG #FLG01
U 1 1 61E3EC42
P 750 1550
F 0 "#FLG01" H 750 1625 50  0001 C CNN
F 1 "PWR_FLAG" H 750 1723 50  0000 C CNN
F 2 "" H 750 1550 50  0001 C CNN
F 3 "~" H 750 1550 50  0001 C CNN
	1    750  1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1550 750  1650
$Comp
L power:PWR_FLAG #FLG08
U 1 1 61E3EC4A
P 1250 1550
F 0 "#FLG08" H 1250 1625 50  0001 C CNN
F 1 "PWR_FLAG" H 1250 1723 50  0000 C CNN
F 2 "" H 1250 1550 50  0001 C CNN
F 3 "~" H 1250 1550 50  0001 C CNN
	1    1250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1550 1250 1650
Wire Wire Line
	1250 1650 1400 1650
Text Label 800  1650 0    50   ~ 0
+3.3v_DIG
Wire Wire Line
	12900 4700 12950 4700
Wire Wire Line
	12950 4700 12950 4800
Text Notes 12300 3600 0    118  ~ 24
Aislación Entrada - Salida
$Comp
L Device:R R42
U 1 1 62A04943
P 11300 4100
F 0 "R42" V 11250 3900 50  0000 C CNN
F 1 "49.9" V 11300 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11230 4100 50  0001 C CNN
F 3 "~" H 11300 4100 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11300 4100 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11300 4100 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11300 4100 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 4100
	0    -1   1    0   
$EndComp
$Comp
L Device:R R43
U 1 1 62A04949
P 11300 4200
F 0 "R43" V 11250 4000 50  0000 C CNN
F 1 "49.9" V 11300 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11230 4200 50  0001 C CNN
F 3 "~" H 11300 4200 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11300 4200 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11300 4200 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11300 4200 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 4200
	0    -1   1    0   
$EndComp
$Comp
L Device:R R44
U 1 1 62A0494F
P 11300 4300
F 0 "R44" V 11250 4100 50  0000 C CNN
F 1 "49.9" V 11300 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11230 4300 50  0001 C CNN
F 3 "~" H 11300 4300 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11300 4300 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11300 4300 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11300 4300 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 4300
	0    -1   1    0   
$EndComp
$Comp
L Device:R R45
U 1 1 62A04955
P 11300 4400
F 0 "R45" V 11250 4200 50  0000 C CNN
F 1 "49.9" V 11300 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11230 4400 50  0001 C CNN
F 3 "~" H 11300 4400 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11300 4400 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11300 4400 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11300 4400 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 4400
	0    -1   1    0   
$EndComp
$Comp
L Device:R R46
U 1 1 62A0495B
P 11300 4500
F 0 "R46" V 11250 4300 50  0000 C CNN
F 1 "49.9" V 11300 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11230 4500 50  0001 C CNN
F 3 "~" H 11300 4500 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11300 4500 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11300 4500 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11300 4500 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 4500
	0    -1   1    0   
$EndComp
$Comp
L Device:R R47
U 1 1 62A04961
P 11300 4600
F 0 "R47" V 11250 4400 50  0000 C CNN
F 1 "49.9" V 11300 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11230 4600 50  0001 C CNN
F 3 "~" H 11300 4600 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11300 4600 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11300 4600 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11300 4600 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 4600
	0    -1   1    0   
$EndComp
Wire Wire Line
	10950 4600 11150 4600
Wire Wire Line
	10950 4500 11150 4500
Wire Wire Line
	10950 4400 11150 4400
Wire Wire Line
	11050 4200 11150 4200
Wire Wire Line
	11050 4100 11150 4100
Wire Wire Line
	11050 4300 11150 4300
$Comp
L Connector:Conn_01x07_Male J4
U 1 1 62DCDB07
P 15800 2200
F 0 "J4" H 16000 2650 50  0000 R CNN
F 1 "Analog Power Rails" V 15750 2550 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B7B-XH-A_1x07_P2.50mm_Vertical" H 15800 2200 50  0001 C CNN
F 3 "~" H 15800 2200 50  0001 C CNN
	1    15800 2200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	15600 1900 15200 1900
Wire Wire Line
	15600 2000 15200 2000
Wire Wire Line
	15600 2100 15200 2100
Wire Wire Line
	15600 2200 15200 2200
Wire Wire Line
	15600 2300 15200 2300
Wire Wire Line
	15600 2400 15200 2400
Text Label 15200 1900 0    50   ~ 0
+15v
Text Label 15200 2000 0    50   ~ 0
+15v
Wire Wire Line
	15200 2100 15200 2200
Connection ~ 15200 2200
Wire Wire Line
	15200 2200 15200 2300
Text Label 15200 2400 0    50   ~ 0
-15v
Wire Wire Line
	15200 2200 15050 2200
Wire Wire Line
	15050 2200 15050 2250
$Comp
L power:GNDA #PWR060
U 1 1 63041228
P 15050 2250
F 0 "#PWR060" H 15050 2000 50  0001 C CNN
F 1 "GNDA" H 15055 2077 50  0000 C CNN
F 2 "" H 15050 2250 50  0001 C CNN
F 3 "" H 15050 2250 50  0001 C CNN
	1    15050 2250
	1    0    0    -1  
$EndComp
Text Label 7900 10300 2    50   ~ 0
+5v_an
Wire Wire Line
	7450 6150 7800 6150
Text Label 7500 6350 0    50   ~ 0
+5v_dig
Wire Wire Line
	800  1650 750  1650
$Comp
L Device:R R26
U 1 1 63141D1D
P 8250 1550
F 0 "R26" H 8100 1500 50  0000 C CNN
F 1 "1Meg" H 8100 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8180 1550 50  0001 C CNN
F 3 "~" H 8250 1550 50  0001 C CNN
F 4 "1 MOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Pulse Withstanding Thick Film" H 8250 1550 50  0001 C CNN "Description"
F 5 "Bourns Inc." H 8250 1550 50  0001 C CNN "Manufacturer_Name"
F 6 "CMP0603-FX-1004ELF" H 8250 1550 50  0001 C CNN "Manufacturer_Part_Number"
	1    8250 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	8550 2600 8550 2800
Connection ~ 8550 2800
Wire Wire Line
	8550 2800 8700 2800
$Comp
L Device:R_POT_Small RV1
U 1 1 63181F4A
P 8250 1200
F 0 "RV1" H 8190 1246 50  0000 R CNN
F 1 "25k" H 8190 1155 50  0000 R CNN
F 2 "FootPrints:3214W" H 8250 1200 50  0001 C CNN
F 3 "~" H 8250 1200 50  0001 C CNN
F 4 "25 kOhms 0.25W, 1/4W J Lead Surface Mount Trimmer Potentiometer Cermet 5 Turn Top Adjustment" H 8250 1200 50  0001 C CNN "Description"
F 5 "Bourns Inc." H 8250 1200 50  0001 C CNN "Manufacturer_Name"
F 6 "3214W-1-253E" H 8250 1200 50  0001 C CNN "Manufacturer_Part_Number"
	1    8250 1200
	0    1    1    0   
$EndComp
$Comp
L Device:R R31
U 1 1 631825E7
P 8600 1200
F 0 "R31" V 8700 1200 50  0000 C CNN
F 1 "82.5k" V 8500 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8530 1200 50  0001 C CNN
F 3 "~" H 8600 1200 50  0001 C CNN
F 4 "82.5 kOhms ±1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Thick Film" H 8600 1200 50  0001 C CNN "Description"
F 5 "Yageo" H 8600 1200 50  0001 C CNN "Manufacturer_Name"
F 6 "RC0603FR-1382K5L" H 8600 1200 50  0001 C CNN "Manufacturer_Part_Number"
	1    8600 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R23
U 1 1 632053A6
P 7900 1200
F 0 "R23" V 8000 1200 50  0000 C CNN
F 1 "82.5k" V 7800 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7830 1200 50  0001 C CNN
F 3 "~" H 7900 1200 50  0001 C CNN
F 4 "82.5 kOhms ±1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Thick Film" H 7900 1200 50  0001 C CNN "Description"
F 5 "Yageo" H 7900 1200 50  0001 C CNN "Manufacturer_Name"
F 6 "RC0603FR-1382K5L" H 7900 1200 50  0001 C CNN "Manufacturer_Part_Number"
	1    7900 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 1200 8800 1200
Wire Wire Line
	8800 1200 8800 1050
Wire Wire Line
	7750 1200 7700 1200
Wire Wire Line
	7700 1200 7700 1050
Text Label 7700 1050 1    50   ~ 0
-15v
Text Label 8800 1050 1    50   ~ 0
+15v
Wire Wire Line
	8450 1200 8350 1200
Wire Wire Line
	8150 1200 8050 1200
Wire Wire Line
	8100 2800 8000 2800
Wire Wire Line
	8250 1300 8250 1400
$Comp
L Device:R R32
U 1 1 63450585
P 8600 3300
F 0 "R32" V 8500 3300 50  0000 C CNN
F 1 "10k" V 8600 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8530 3300 50  0001 C CNN
F 3 "~" H 8600 3300 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8600 3300 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 8600 3300 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 8600 3300 50  0001 C CNN "Manufacturer_Part_Number"
	1    8600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3150 8600 3150
Connection ~ 8400 3150
Wire Wire Line
	8400 3500 8600 3500
Wire Wire Line
	8600 3500 8600 3450
Connection ~ 8400 3500
Wire Wire Line
	8400 3150 8400 3000
Wire Wire Line
	8200 3500 8400 3500
Wire Wire Line
	8400 3000 8800 3000
Wire Wire Line
	8200 3150 8400 3150
$Comp
L power:GNDA #PWR046
U 1 1 63600FCA
P 10000 7450
F 0 "#PWR046" H 10000 7200 50  0001 C CNN
F 1 "GNDA" H 10005 7277 50  0000 C CNN
F 2 "" H 10000 7450 50  0001 C CNN
F 3 "" H 10000 7450 50  0001 C CNN
	1    10000 7450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR017
U 1 1 636012CF
P 4400 4250
F 0 "#PWR017" H 4400 4000 50  0001 C CNN
F 1 "GNDA" H 4405 4077 50  0000 C CNN
F 2 "" H 4400 4250 50  0001 C CNN
F 3 "" H 4400 4250 50  0001 C CNN
	1    4400 4250
	1    0    0    -1  
$EndComp
$Comp
L SI8661BB-B-IS1R:SI8661BB-B-IS1R AT1
U 1 1 6170A92E
P 12900 4000
F 0 "AT1" H 13450 4265 50  0000 C CNN
F 1 "Si8661EC-B-IS1" H 13450 4174 50  0000 C CNN
F 2 "FootPrints:SOIC127P600X175-16N" H 13850 4100 50  0001 L CNN
F 3 "https://www.silabs.com/documents/public/data-sheets/Si866x.pdf" H 13850 4000 50  0001 L CNN
F 4 "Digital Isolators 2.5 kV 5 fwd & 1 rev 6-channel isolator" H 13850 3900 50  0001 L CNN "Description"
F 5 "1.75" H 13850 3800 50  0001 L CNN "Height"
F 6 "634-SI8661BB-B-IS1R" H 13850 3700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Silicon-Labs/SI8661BB-B-IS1R?qs=j6MGy4L9yX0tVoDzo2LvhA%3D%3D" H 13850 3600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Silicon Labs" H 13850 3500 50  0001 L CNN "Manufacturer_Name"
F 9 "SI8661BB-B-IS1R" H 13850 3400 50  0001 L CNN "Manufacturer_Part_Number"
	1    12900 4000
	-1   0    0    -1  
$EndComp
$Comp
L ‎SI8422BB-D-IS:‎SI8422-D-IS U15
U 1 1 638AF30A
P 12300 5700
F 0 "U15" H 12350 6338 60  0000 C CNN
F 1 "‎SI8422-D-IS" H 12350 6232 60  0000 C CNN
F 2 "FootPrints:‎SI8422BB-D-IS" H 12100 5590 60  0001 C CNN
F 3 "" H 12100 5650 60  0000 C CNN
F 4 "General Purpose Digital Isolator 2500Vrms 2 Channel 150Mbps 20kV/µs CMTI 8-SOIC (0.154\", 3.90mm Width)" H 12300 5700 50  0001 C CNN "Description"
F 5 "Silicon Labs" H 12300 5700 50  0001 C CNN "Manufacturer_Name"
F 6 "SI8422BB-D-IS" H 12300 5700 50  0001 C CNN "Manufacturer_Part_Number"
	1    12300 5700
	1    0    0    -1  
$EndComp
Text Label 13200 5450 0    50   ~ 0
+3.3v_DIG
Text Label 13700 5650 0    50   ~ 0
LoadOnOff_DIG
Wire Wire Line
	12900 5450 13000 5450
Text Label 13700 5850 0    50   ~ 0
EOC_INT_DIG
Wire Wire Line
	12900 6050 13200 6050
Wire Wire Line
	13200 6050 13200 6250
Wire Wire Line
	11800 6050 11500 6050
Wire Wire Line
	11500 6050 11500 6250
Text Label 11350 5450 0    50   ~ 0
+5v_an
Text Label 11200 5650 2    50   ~ 0
LoadOnOff
Text Label 11200 5850 2    50   ~ 0
EOC_INT
Wire Wire Line
	13950 4600 13450 4600
Wire Wire Line
	13950 4500 13450 4500
Wire Wire Line
	13950 4400 13450 4400
Wire Wire Line
	13950 4300 13450 4300
Wire Wire Line
	13950 4200 13450 4200
Wire Wire Line
	13950 4100 13450 4100
$Comp
L Device:R R58
U 1 1 61A44D14
P 13300 4600
F 0 "R58" V 13250 4400 50  0000 C CNN
F 1 "49.9" V 13300 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13230 4600 50  0001 C CNN
F 3 "~" H 13300 4600 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13300 4600 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13300 4600 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13300 4600 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 4600
	0    1    1    0   
$EndComp
$Comp
L Device:R R57
U 1 1 61A44BC7
P 13300 4500
F 0 "R57" V 13250 4300 50  0000 C CNN
F 1 "49.9" V 13300 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13230 4500 50  0001 C CNN
F 3 "~" H 13300 4500 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13300 4500 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13300 4500 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13300 4500 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R56
U 1 1 61A44A0E
P 13300 4400
F 0 "R56" V 13250 4200 50  0000 C CNN
F 1 "49.9" V 13300 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13230 4400 50  0001 C CNN
F 3 "~" H 13300 4400 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13300 4400 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13300 4400 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13300 4400 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 4400
	0    1    1    0   
$EndComp
$Comp
L Device:R R55
U 1 1 61A447DB
P 13300 4300
F 0 "R55" V 13250 4100 50  0000 C CNN
F 1 "49.9" V 13300 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13230 4300 50  0001 C CNN
F 3 "~" H 13300 4300 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13300 4300 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13300 4300 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13300 4300 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R R54
U 1 1 61A44674
P 13300 4200
F 0 "R54" V 13250 4000 50  0000 C CNN
F 1 "49.9" V 13300 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13230 4200 50  0001 C CNN
F 3 "~" H 13300 4200 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13300 4200 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13300 4200 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13300 4200 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R53
U 1 1 61A0E550
P 13300 4100
F 0 "R53" V 13250 3900 50  0000 C CNN
F 1 "49.9" V 13300 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13230 4100 50  0001 C CNN
F 3 "~" H 13300 4100 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13300 4100 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13300 4100 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13300 4100 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 4100
	0    1    1    0   
$EndComp
Text Label 13950 4600 2    50   ~ 0
MISO_DIG
Text Label 13950 4500 2    50   ~ 0
MOSI_DIG
Text Label 13950 4400 2    50   ~ 0
SCLK_DIG
Text Label 13950 4300 2    50   ~ 0
DAC_CS_DIG
Text Label 13950 4200 2    50   ~ 0
CONVST_DIG
Text Label 13950 4100 2    50   ~ 0
ADC_CS_DIG
Text Label 13950 4000 2    50   ~ 0
+3.3v_DIG
$Comp
L Device:Ferrite_Bead_Small FB2
U 1 1 63D9E1AF
P 9550 5550
F 0 "FB2" V 9400 5550 50  0000 C CNN
F 1 "110R @ 100MHz" V 9650 5550 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9480 5550 50  0001 C CNN
F 3 "~" H 9550 5550 50  0001 C CNN
F 4 "110R @ 100MHz, 0805, 5A" H 9550 5550 50  0001 C CNN "Description"
F 5 "Murata" H 9550 5550 50  0001 C CNN "Manufacturer_Name"
F 6 "BLM21SP111SN1D‎" H 9550 5550 50  0001 C CNN "Manufacturer_Part_Number"
	1    9550 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 5550 9100 5550
Wire Wire Line
	9100 5550 9100 5450
Wire Wire Line
	9650 5550 10000 5550
Wire Wire Line
	10000 5550 10000 5450
Text Label 9100 5450 2    50   ~ 0
+5v_an
Text Label 10000 5450 2    50   ~ 0
+5v_dig
Wire Wire Line
	6600 5600 6750 5600
$Comp
L power:GNDA #PWR027
U 1 1 63E6078F
P 6750 5600
F 0 "#PWR027" H 6750 5350 50  0001 C CNN
F 1 "GNDA" H 6755 5427 50  0000 C CNN
F 2 "" H 6750 5600 50  0001 C CNN
F 3 "" H 6750 5600 50  0001 C CNN
	1    6750 5600
	-1   0    0    1   
$EndComp
Connection ~ 6750 5600
Wire Wire Line
	6750 5600 6900 5600
$Comp
L power:GNDA #PWR049
U 1 1 63E9F244
P 11450 4800
F 0 "#PWR049" H 11450 4550 50  0001 C CNN
F 1 "GNDA" H 11455 4627 50  0000 C CNN
F 2 "" H 11450 4800 50  0001 C CNN
F 3 "" H 11450 4800 50  0001 C CNN
	1    11450 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR048
U 1 1 63E9F7F2
P 11400 3750
F 0 "#PWR048" H 11400 3500 50  0001 C CNN
F 1 "GNDA" H 11405 3577 50  0000 C CNN
F 2 "" H 11400 3750 50  0001 C CNN
F 3 "" H 11400 3750 50  0001 C CNN
	1    11400 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR053
U 1 1 63EA4199
P 12950 4800
F 0 "#PWR053" H 12950 4550 50  0001 C CNN
F 1 "GNDD" H 12954 4645 50  0000 C CNN
F 2 "" H 12950 4800 50  0001 C CNN
F 3 "" H 12950 4800 50  0001 C CNN
	1    12950 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR055
U 1 1 63EA460E
P 13200 6250
F 0 "#PWR055" H 13200 6000 50  0001 C CNN
F 1 "GNDD" H 13204 6095 50  0000 C CNN
F 2 "" H 13200 6250 50  0001 C CNN
F 3 "" H 13200 6250 50  0001 C CNN
	1    13200 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR056
U 1 1 63EA498A
P 13400 3750
F 0 "#PWR056" H 13400 3500 50  0001 C CNN
F 1 "GNDD" H 13404 3595 50  0000 C CNN
F 2 "" H 13400 3750 50  0001 C CNN
F 3 "" H 13400 3750 50  0001 C CNN
	1    13400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 8000 2250 8000
Wire Wire Line
	1650 7800 1650 8100
Wire Wire Line
	2300 8200 2150 8200
Wire Wire Line
	2150 8000 2150 8100
Wire Wire Line
	2650 8000 2550 8000
Wire Wire Line
	2650 8200 2650 8100
Wire Wire Line
	2500 8200 2650 8200
$Comp
L Device:C_Small C10
U 1 1 63EA5BDC
P 2400 8200
F 0 "C10" V 2537 8200 50  0000 C CNN
F 1 "NP" V 2628 8200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2400 8200 50  0001 C CNN
F 3 "~" H 2400 8200 50  0001 C CNN
F 4 "C0G, Respaldo, 50v, valor indeterminado, 0603" H 2400 8200 50  0001 C CNN "Description"
	1    2400 8200
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 8100 3250 8100
Connection ~ 2650 8100
Wire Wire Line
	2650 8100 2650 8000
Wire Wire Line
	2150 8100 1650 8100
Connection ~ 2150 8100
Wire Wire Line
	2150 8100 2150 8200
$Comp
L power:PWR_FLAG #FLG04
U 1 1 64179DF2
P 1200 1100
F 0 "#FLG04" H 1200 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 1200 1273 50  0000 C CNN
F 2 "" H 1200 1100 50  0001 C CNN
F 3 "~" H 1200 1100 50  0001 C CNN
	1    1200 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1100 1200 1200
Text Label 1300 1200 0    50   ~ 0
+5v_dig
Wire Wire Line
	1300 1200 1200 1200
$Comp
L power:GNDD #PWR05
U 1 1 642006AD
P 1400 1650
F 0 "#PWR05" H 1400 1400 50  0001 C CNN
F 1 "GNDD" H 1404 1495 50  0000 C CNN
F 2 "" H 1400 1650 50  0001 C CNN
F 3 "" H 1400 1650 50  0001 C CNN
	1    1400 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR050
U 1 1 64201D61
P 11500 6250
F 0 "#PWR050" H 11500 6000 50  0001 C CNN
F 1 "GNDA" H 11505 6077 50  0000 C CNN
F 2 "" H 11500 6250 50  0001 C CNN
F 3 "" H 11500 6250 50  0001 C CNN
	1    11500 6250
	1    0    0    -1  
$EndComp
Text Notes 4450 6050 2    50   ~ 0
Tantalio
$Comp
L g5pz-1a-edc5:G5PZ-1A-EDC5 K1
U 1 1 64291B40
P 11950 2350
F 0 "K1" V 12500 2100 50  0000 C CNN
F 1 "G5PZ-1A-EDC12" V 12400 2350 50  0000 C CNN
F 2 "FootPrints:G5PZ-1A-EDC5" H 13275 2400 50  0001 C CNN
F 3 "https://www.panasonic-electric-works.com/pew/es/downloads/ds_dw_hl_en.pdf" H 11950 2350 50  0001 C CNN
F 4 "General Purpose Relay SPST-NO (1 Form A) 12VDC Coil Through Hole" H 11950 2350 50  0001 C CNN "Description"
F 5 "Omron Electronics Inc-EMC Div" H 11950 2350 50  0001 C CNN "Manufacturer_Name"
F 6 "G5PZ-1A-E DC12" H 11950 2350 50  0001 C CNN "Manufacturer_Part_Number"
	1    11950 2350
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C25
U 1 1 64362056
P 5750 3650
F 0 "C25" V 5521 3650 50  0000 C CNN
F 1 "NP" V 5612 3650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5750 3650 50  0001 C CNN
F 3 "~" H 5750 3650 50  0001 C CNN
F 4 "C0G, Respaldo, 50v, valor indeterminado, 0603" H 5750 3650 50  0001 C CNN "Description"
	1    5750 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 3650 6150 3650
Wire Wire Line
	6150 3650 6150 3900
Connection ~ 6150 3900
Wire Wire Line
	5650 3650 5300 3650
Wire Wire Line
	5300 3650 5300 3900
Connection ~ 5300 3900
Wire Notes Line
	4950 5100 4950 3350
Wire Notes Line
	7200 3350 7200 5100
Wire Wire Line
	11050 4000 11650 4000
Wire Wire Line
	12900 4000 13150 4000
Wire Wire Line
	13150 3950 13150 4000
Connection ~ 13150 4000
Wire Wire Line
	13150 4000 13950 4000
Wire Wire Line
	8250 1700 8250 1850
Wire Wire Line
	8550 2600 8400 2600
Text Label 8000 2600 2    50   ~ 0
ImonOffset
Text Label 8450 1850 0    50   ~ 0
ImonOffset
$Comp
L Device:R_POT_Small RV2
U 1 1 646A7912
P 9500 1200
F 0 "RV2" H 9440 1246 50  0000 R CNN
F 1 "25k" H 9440 1155 50  0000 R CNN
F 2 "FootPrints:3214W" H 9500 1200 50  0001 C CNN
F 3 "~" H 9500 1200 50  0001 C CNN
F 4 "25 kOhms 0.25W, 1/4W J Lead Surface Mount Trimmer Potentiometer Cermet 5 Turn Top Adjustment" H 9500 1200 50  0001 C CNN "Description"
F 5 "Bourns Inc." H 9500 1200 50  0001 C CNN "Manufacturer_Name"
F 6 "3214W-1-253E" H 9500 1200 50  0001 C CNN "Manufacturer_Part_Number"
	1    9500 1200
	0    1    1    0   
$EndComp
$Comp
L Device:R R41
U 1 1 646A7918
P 9850 1200
F 0 "R41" V 9950 1200 50  0000 C CNN
F 1 "82.5k" V 9750 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9780 1200 50  0001 C CNN
F 3 "~" H 9850 1200 50  0001 C CNN
F 4 "82.5 kOhms ±1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Thick Film" H 9850 1200 50  0001 C CNN "Description"
F 5 "Yageo" H 9850 1200 50  0001 C CNN "Manufacturer_Name"
F 6 "RC0603FR-1382K5L" H 9850 1200 50  0001 C CNN "Manufacturer_Part_Number"
	1    9850 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R37
U 1 1 646A791E
P 9150 1200
F 0 "R37" V 9250 1250 50  0000 C CNN
F 1 "82.5k" V 9050 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9080 1200 50  0001 C CNN
F 3 "~" H 9150 1200 50  0001 C CNN
F 4 "82.5 kOhms ±1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Thick Film" H 9150 1200 50  0001 C CNN "Description"
F 5 "Yageo" H 9150 1200 50  0001 C CNN "Manufacturer_Name"
F 6 "RC0603FR-1382K5L" H 9150 1200 50  0001 C CNN "Manufacturer_Part_Number"
	1    9150 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10000 1200 10050 1200
Wire Wire Line
	10050 1200 10050 1050
Wire Wire Line
	9000 1200 8950 1200
Wire Wire Line
	8950 1200 8950 1050
Text Label 8950 1050 1    50   ~ 0
-15v
Text Label 10050 1050 1    50   ~ 0
+15v
Wire Wire Line
	9700 1200 9600 1200
Wire Wire Line
	9400 1200 9300 1200
Wire Wire Line
	9600 1550 9500 1550
Wire Wire Line
	9500 1300 9500 1550
Wire Wire Line
	9600 1750 9500 1750
Wire Wire Line
	9500 1750 9500 2100
Wire Wire Line
	9500 2100 10250 2100
Wire Wire Line
	10250 2100 10250 1650
Wire Wire Line
	10250 1650 10200 1650
Wire Wire Line
	10250 1650 10400 1650
Connection ~ 10250 1650
Text Label 10400 1650 0    50   ~ 0
VmonOffset
Text Label 9250 4800 0    50   ~ 0
VmonOffset
$Comp
L power:GNDA #PWR034
U 1 1 6484EF7D
P 8150 4900
F 0 "#PWR034" H 8150 4650 50  0001 C CNN
F 1 "GNDA" H 8155 4727 50  0000 C CNN
F 2 "" H 8150 4900 50  0001 C CNN
F 3 "" H 8150 4900 50  0001 C CNN
	1    8150 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4800 8150 4800
Wire Wire Line
	8150 4800 8150 4900
Wire Wire Line
	8500 4800 8600 4800
$Comp
L Device:R R27
U 1 1 6492C809
P 8250 2600
F 0 "R27" V 8150 2600 50  0000 C CNN
F 1 "0R" V 8250 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8180 2600 50  0001 C CNN
F 3 "~" H 8250 2600 50  0001 C CNN
F 4 "0 Ohms Jumper 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 8250 2600 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 8250 2600 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS06030000Z0EA" H 8250 2600 50  0001 C CNN "Manufacturer_Part_Number"
	1    8250 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	8100 2600 8000 2600
Wire Wire Line
	8750 4800 8600 4800
Connection ~ 8600 4800
Wire Wire Line
	9050 4800 9250 4800
Wire Notes Line
	7450 550  11000 550 
Wire Notes Line
	11000 550  11000 2200
Wire Notes Line
	7450 2200 11000 2200
Text Notes 8400 750  0    118  ~ 24
Ajuste de Offset
$Comp
L Device:C_Small C45
U 1 1 64B2ED56
P 9150 2350
F 0 "C45" V 9050 2500 50  0000 C CNN
F 1 "NP" V 9200 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9150 2350 50  0001 C CNN
F 3 "~" H 9150 2350 50  0001 C CNN
F 4 "C0G, Respaldo, 50v, valor indeterminado, 0603" H 9150 2350 50  0001 C CNN "Description"
	1    9150 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 2350 8700 2350
Wire Wire Line
	8700 2350 8700 2600
Connection ~ 8700 2600
Wire Wire Line
	9250 2350 9500 2350
Wire Wire Line
	9500 2350 9500 2600
Connection ~ 9500 2600
Wire Notes Line
	10250 5200 7450 5200
Wire Notes Line
	7450 550  7450 5200
Wire Notes Line
	10250 2200 10250 5200
$Comp
L Device:C_Small C46
U 1 1 64C59B85
P 9650 10250
F 0 "C46" V 9879 10250 50  0000 C CNN
F 1 "100n" V 9788 10250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9650 10250 50  0001 C CNN
F 3 "~" H 9650 10250 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 9650 10250 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 9650 10250 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 9650 10250 50  0001 C CNN "Manufacturer_Part_Number"
	1    9650 10250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9750 10800 9900 10800
Wire Wire Line
	9900 10800 9900 10750
$Comp
L power:GNDA #PWR042
U 1 1 64C59B8D
P 9450 10900
F 0 "#PWR042" H 9450 10650 50  0001 C CNN
F 1 "GNDA" H 9455 10727 50  0000 C CNN
F 2 "" H 9450 10900 50  0001 C CNN
F 3 "" H 9450 10900 50  0001 C CNN
	1    9450 10900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 10250 9900 10250
Wire Wire Line
	9900 10250 9900 10300
$Comp
L power:GNDA #PWR041
U 1 1 64C59B95
P 9450 10350
F 0 "#PWR041" H 9450 10100 50  0001 C CNN
F 1 "GNDA" H 9455 10177 50  0000 C CNN
F 2 "" H 9450 10350 50  0001 C CNN
F 3 "" H 9450 10350 50  0001 C CNN
	1    9450 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 10250 9450 10350
Wire Wire Line
	9450 10250 9550 10250
Wire Wire Line
	9450 10800 9450 10900
Wire Wire Line
	9450 10800 9550 10800
$Comp
L Device:C_Small C47
U 1 1 64C59B9F
P 9650 10800
F 0 "C47" V 9879 10800 50  0000 C CNN
F 1 "100n" V 9788 10800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9650 10800 50  0001 C CNN
F 3 "~" H 9650 10800 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 9650 10800 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 9650 10800 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 9650 10800 50  0001 C CNN "Manufacturer_Part_Number"
	1    9650 10800
	0    -1   -1   0   
$EndComp
Connection ~ 9900 10250
Connection ~ 9900 10800
Text Label 10100 10250 2    50   ~ 0
+15v
Wire Wire Line
	9900 10250 10100 10250
Text Label 10100 10800 2    50   ~ 0
-15v
Wire Wire Line
	9900 10800 10100 10800
Text Notes 8950 3650 0    50   ~ 0
Todas Las Resistencias 0.1%\nsustiruir resistencias de entrada\nno inversora por una de 5k si \nse usa el ajuste de offset.
Text Notes 8900 5150 0    50   ~ 0
Todas Las Resistencias 0.1%\nUnir JP1 o JP2 segun se desee\najuste de offset o no.
Text Notes 3850 8450 2    50   ~ 0
Tantalio
Text Notes 6750 7400 0    50   ~ 0
ceramico\n
Wire Wire Line
	10250 7150 10250 7450
Text Notes 7500 2150 0    50   ~ 0
Todas Las Resistencias 0.1%\nPoblar si se desea Ajuste\nde Offset.
Wire Wire Line
	8450 1850 8250 1850
Wire Notes Line
	4600 1100 4600 3050
Wire Notes Line
	2700 1100 2700 3050
Wire Wire Line
	12900 2550 13050 2550
Text Label 13700 3100 2    50   ~ 0
LoadOnOff
Wire Wire Line
	13700 3100 13300 3100
$Comp
L Device:D D2
U 1 1 64F6B981
P 9000 7850
F 0 "D2" H 9000 7633 50  0000 C CNN
F 1 "VS-10MQ100-M3/5AT" H 9000 7724 50  0000 C CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 9000 7850 50  0001 C CNN
F 3 "~" H 9000 7850 50  0001 C CNN
F 4 "Diode Schottky 100V 1A Surface Mount DO-214AC (SMA)" H 9000 7850 50  0001 C CNN "Description"
F 5 "Vishay Semiconductor Diodes Division" H 9000 7850 50  0001 C CNN "Manufacturer_Name"
F 6 "VS-10MQ100-M3/5AT" H 9000 7850 50  0001 C CNN "Manufacturer_Part_Number"
	1    9000 7850
	-1   0    0    1   
$EndComp
$Comp
L Device:D D3
U 1 1 64F6CEE4
P 9000 8000
F 0 "D3" H 9000 7783 50  0000 C CNN
F 1 "VS-10MQ100-M3/5AT" H 9000 7874 50  0000 C CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 9000 8000 50  0001 C CNN
F 3 "~" H 9000 8000 50  0001 C CNN
F 4 "Diode Schottky 100V 1A Surface Mount DO-214AC (SMA)" H 9000 8000 50  0001 C CNN "Description"
F 5 "Vishay Semiconductor Diodes Division" H 9000 8000 50  0001 C CNN "Manufacturer_Name"
F 6 "VS-10MQ100-M3/5AT" H 9000 8000 50  0001 C CNN "Manufacturer_Part_Number"
	1    9000 8000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 8000 9300 8000
Wire Wire Line
	9300 8000 9300 7950
Wire Wire Line
	9300 7850 9150 7850
Wire Wire Line
	8850 7850 8750 7850
Wire Wire Line
	8750 7850 8750 7950
Wire Wire Line
	8750 8000 8850 8000
$Comp
L power:GNDA #PWR043
U 1 1 65008A86
P 9600 7950
F 0 "#PWR043" H 9600 7700 50  0001 C CNN
F 1 "GNDA" H 9605 7777 50  0000 C CNN
F 2 "" H 9600 7950 50  0001 C CNN
F 3 "" H 9600 7950 50  0001 C CNN
	1    9600 7950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 7950 9300 7950
Connection ~ 9300 7950
Wire Wire Line
	9300 7950 9300 7850
$Comp
L power:GNDA #PWR038
U 1 1 6505747F
P 8400 7950
F 0 "#PWR038" H 8400 7700 50  0001 C CNN
F 1 "GNDA" H 8405 7777 50  0000 C CNN
F 2 "" H 8400 7950 50  0001 C CNN
F 3 "" H 8400 7950 50  0001 C CNN
	1    8400 7950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 7950 8400 7950
Connection ~ 8750 7950
Wire Wire Line
	8750 7950 8750 8000
Wire Notes Line
	4700 5250 4700 8300
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 650F751B
P 3800 4850
F 0 "FB1" V 3650 4850 50  0000 C CNN
F 1 "110R @ 100MHz" V 3900 4850 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3730 4850 50  0001 C CNN
F 3 "~" H 3800 4850 50  0001 C CNN
F 4 "110R @ 100MHz, 0805, 5A" H 3800 4850 50  0001 C CNN "Description"
F 5 "Murata" H 3800 4850 50  0001 C CNN "Manufacturer_Name"
F 6 "BLM21SP111SN1D‎" H 3800 4850 50  0001 C CNN "Manufacturer_Part_Number"
	1    3800 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 4850 3350 4850
Wire Wire Line
	3350 4850 3350 4750
Wire Wire Line
	3900 4850 4250 4850
Wire Wire Line
	4250 4850 4250 4750
Text Label 3350 4750 2    50   ~ 0
+5v_an
Text Label 4250 4750 2    50   ~ 0
+5v_dig
Wire Wire Line
	1400 4650 1550 4650
$Comp
L power:GNDA #PWR06
U 1 1 6519A676
P 1550 4650
F 0 "#PWR06" H 1550 4400 50  0001 C CNN
F 1 "GNDA" H 1555 4477 50  0000 C CNN
F 2 "" H 1550 4650 50  0001 C CNN
F 3 "" H 1550 4650 50  0001 C CNN
	1    1550 4650
	1    0    0    -1  
$EndComp
Connection ~ 1550 4650
Wire Wire Line
	1550 4650 1700 4650
Wire Notes Line
	10400 5250 10400 8300
Wire Notes Line
	4700 5250 10400 5250
Wire Notes Line
	4700 8300 10400 8300
Wire Wire Line
	15250 750  15550 750 
Wire Wire Line
	15250 650  15550 650 
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 60367B90
P 15750 950
F 0 "J3" H 15830 942 50  0000 L CNN
F 1 "remoteSense" H 15550 750 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-2-5.08_1x02_P5.08mm_Horizontal" H 15750 950 50  0001 C CNN
F 3 "~" H 15750 950 50  0001 C CNN
F 4 "2 Position Wire to Board Terminal Block Horizontal with Board 0.200\" (5.08mm) Through Hole" H 15750 950 50  0001 C CNN "Description"
F 5 "Adam Tech" H 15750 950 50  0001 C CNN "Manufacturer_Name"
F 6 "EB21A-02-D" H 15750 950 50  0001 C CNN "Manufacturer_Part_Number"
	1    15750 950 
	1    0    0    -1  
$EndComp
Text Label 15500 950  2    50   ~ 0
VremoteSense+
Text Label 15500 1050 2    50   ~ 0
VremoteSense-
Wire Wire Line
	12900 5850 13200 5850
Wire Wire Line
	12900 5650 13200 5650
$Comp
L power:GNDD #PWR057
U 1 1 6160BD71
P 14150 5150
F 0 "#PWR057" H 14150 4900 50  0001 C CNN
F 1 "GNDD" H 14154 4995 50  0000 C CNN
F 2 "" H 14150 5150 50  0001 C CNN
F 3 "" H 14150 5150 50  0001 C CNN
	1    14150 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	15000 5150 15050 5150
Wire Wire Line
	15000 5050 15050 5050
Wire Wire Line
	15000 4950 15050 4950
Wire Wire Line
	15000 4850 15050 4850
Wire Wire Line
	15000 4750 15050 4750
Wire Wire Line
	15000 4650 15050 4650
Wire Wire Line
	15000 4550 15050 4550
Wire Wire Line
	14450 4750 14500 4750
Wire Wire Line
	14450 4650 14500 4650
Wire Wire Line
	14450 4550 14500 4550
Text Label 15050 5150 0    50   ~ 0
LoadOnOff_DIG
Text Label 15050 5050 0    50   ~ 0
ADC_CS_DIG
Text Label 15050 4850 0    50   ~ 0
MOSI_DIG
Text Label 15050 4950 0    50   ~ 0
MISO_DIG
Text Label 15050 4750 0    50   ~ 0
SCLK_DIG
Text Label 15050 4650 0    50   ~ 0
DAC_CS_DIG
Text Label 15050 4550 0    50   ~ 0
CONVST_DIG
Wire Wire Line
	14500 4950 14400 4950
Wire Wire Line
	14400 4850 14400 4900
Wire Wire Line
	14500 4850 14400 4850
Text Label 14450 4750 2    50   ~ 0
EOC_INT_DIG
Text Label 14450 4650 2    50   ~ 0
+3.3v_DIG
Text Label 14450 4550 2    50   ~ 0
+3.3v_DIG
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J1
U 1 1 611FD281
P 14700 4850
F 0 "J1" H 14750 5367 50  0000 C CNN
F 1 "Conector de E/S" H 14750 5276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 14750 5275 50  0001 C CNN
F 3 "~" H 14700 4850 50  0001 C CNN
F 4 "Connector Header Through Hole 14 position 0.100\" (2.54mm)" H 14700 4850 50  0001 C CNN "Description"
F 5 "Adam Tech" H 14700 4850 50  0001 C CNN "Manufacturer_Name"
F 6 "PH2-14-UA" H 14700 4850 50  0001 C CNN "Manufacturer_Part_Number"
	1    14700 4850
	1    0    0    -1  
$EndComp
Wire Notes Line
	10750 3400 15700 3400
$Comp
L Device:R R59
U 1 1 617EF11F
P 13350 5650
F 0 "R59" V 13300 5450 50  0000 C CNN
F 1 "49.9" V 13350 5650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13280 5650 50  0001 C CNN
F 3 "~" H 13350 5650 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13350 5650 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13350 5650 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13350 5650 50  0001 C CNN "Manufacturer_Part_Number"
	1    13350 5650
	0    1    1    0   
$EndComp
$Comp
L Device:R R49
U 1 1 617EF614
P 11500 5850
F 0 "R49" V 11450 5650 50  0000 C CNN
F 1 "49.9" V 11500 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11430 5850 50  0001 C CNN
F 3 "~" H 11500 5850 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11500 5850 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11500 5850 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11500 5850 50  0001 C CNN "Manufacturer_Part_Number"
	1    11500 5850
	0    -1   1    0   
$EndComp
$Comp
L Device:R R60
U 1 1 617F07F0
P 13350 5850
F 0 "R60" V 13300 5650 50  0000 C CNN
F 1 "49.9" V 13350 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13280 5850 50  0001 C CNN
F 3 "~" H 13350 5850 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13350 5850 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 13350 5850 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 13350 5850 50  0001 C CNN "Manufacturer_Part_Number"
	1    13350 5850
	0    1    1    0   
$EndComp
$Comp
L Device:R R48
U 1 1 617F0B45
P 11500 5650
F 0 "R48" V 11450 5450 50  0000 C CNN
F 1 "49.9" V 11500 5650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11430 5650 50  0001 C CNN
F 3 "~" H 11500 5650 50  0001 C CNN
F 4 "49.9 Ohms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11500 5650 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 11500 5650 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS060349R9FKEA" H 11500 5650 50  0001 C CNN "Manufacturer_Part_Number"
	1    11500 5650
	0    -1   1    0   
$EndComp
Wire Wire Line
	11650 5650 11800 5650
Wire Wire Line
	11200 5650 11350 5650
Wire Wire Line
	11200 5850 11350 5850
Wire Wire Line
	11650 5850 11800 5850
Wire Wire Line
	13500 5650 13700 5650
Text Notes 1800 6000 2    50   ~ 0
Tantalio
Wire Wire Line
	12350 7750 12600 7750
Connection ~ 12700 2850
Text Label 13700 2850 2    50   ~ 0
OVCP_DRIVE
Text Label 11450 8600 2    50   ~ 0
OVCP_DRIVE
Text Label 14500 5150 2    50   ~ 0
#OCP
$Comp
L Device:R R39
U 1 1 6167D4F7
P 9250 4150
F 0 "R39" V 9150 4150 50  0000 C CNN
F 1 "10k" V 9250 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9180 4150 50  0001 C CNN
F 3 "~" H 9250 4150 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 9250 4150 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 9250 4150 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 9250 4150 50  0001 C CNN "Manufacturer_Part_Number"
	1    9250 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R40
U 1 1 6167D82A
P 9250 4350
F 0 "R40" V 9150 4350 50  0000 C CNN
F 1 "10k" V 9250 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9180 4350 50  0001 C CNN
F 3 "~" H 9250 4350 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 9250 4350 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 9250 4350 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 9250 4350 50  0001 C CNN "Manufacturer_Part_Number"
	1    9250 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 4150 9400 4150
Wire Wire Line
	9450 4350 9400 4350
$Comp
L Transistor_BJT:BCP53 Q1
U 1 1 618D241E
P 3150 7700
F 0 "Q1" H 3341 7746 50  0000 L CNN
F 1 "BCP53" H 3341 7655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 3350 7625 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/BCP53T1-D.PDF" H 3150 7700 50  0001 L CNN
F 4 "Bipolar (BJT) Transistor PNP 80V 1A 100MHz 2.2W Surface Mount SOT-223" H 3150 7700 50  0001 C CNN "Description"
F 5 "Nexperia USA Inc." H 3150 7700 50  0001 C CNN "Manufacturer_Name"
F 6 "BCP53HX" H 3150 7700 50  0001 C CNN "Manufacturer_Part_Number"
	1    3150 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	13700 5850 13500 5850
Wire Wire Line
	13050 8350 13150 8350
Wire Wire Line
	13050 8350 12950 8350
Connection ~ 13050 8350
Wire Wire Line
	13150 8250 13150 8350
Wire Wire Line
	13150 7950 12950 7950
Connection ~ 13150 7950
Wire Wire Line
	13150 8050 13150 7950
$Comp
L Device:C_Small C52
U 1 1 61354C53
P 13150 8150
F 0 "C52" H 13300 8150 50  0000 C CNN
F 1 "390p" H 13350 8050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13150 8150 50  0001 C CNN
F 3 "~" H 13150 8150 50  0001 C CNN
F 4 "390pF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 13150 8150 50  0001 C CNN "Description"
F 5 "Yageo" H 13150 8150 50  0001 C CNN "Manufacturer_Name"
F 6 "CC0603KRX7R9BB391" H 13150 8150 50  0001 C CNN "Manufacturer_Part_Number"
	1    13150 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	12350 7950 12950 7950
Text Notes 11100 6900 0    118  ~ 24
Protección Sobre Corriente / Polaridad Inversa
Connection ~ 14800 7650
Wire Wire Line
	15100 7650 14800 7650
Text Label 15100 7650 2    50   ~ 0
#OCP
Wire Wire Line
	14800 7850 14800 8000
Wire Wire Line
	14750 7850 14800 7850
$Comp
L power:GNDD #PWR058
U 1 1 609B9649
P 14800 8000
F 0 "#PWR058" H 14800 7750 50  0001 C CNN
F 1 "GNDD" H 14804 7845 50  0000 C CNN
F 2 "" H 14800 8000 50  0001 C CNN
F 3 "" H 14800 8000 50  0001 C CNN
	1    14800 8000
	1    0    0    -1  
$EndComp
Text Label 14950 7200 2    50   ~ 0
+3.3v_DIG
Text Label 14100 7200 2    50   ~ 0
+15v
Wire Wire Line
	13950 7200 14100 7200
Wire Wire Line
	13950 7300 13950 7200
Wire Wire Line
	14800 7200 14950 7200
Wire Wire Line
	14800 7300 14800 7200
Wire Wire Line
	14800 7600 14800 7650
Wire Wire Line
	13950 7600 13950 7650
$Comp
L Device:R R61
U 1 1 5FF41A53
P 13950 7450
F 0 "R61" H 13880 7404 50  0000 R CNN
F 1 "2.2k" V 13950 7450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13880 7450 50  0001 C CNN
F 3 "~" H 13950 7450 50  0001 C CNN
F 4 "2.2 kOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13950 7450 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 13950 7450 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PA3F2201V" H 13950 7450 50  0001 C CNN "Manufacturer_Part_Number"
	1    13950 7450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14150 7650 13950 7650
Wire Wire Line
	12950 8300 12950 8350
$Comp
L power:GNDA #PWR054
U 1 1 602D4552
P 13050 8350
F 0 "#PWR054" H 13050 8100 50  0001 C CNN
F 1 "GNDA" H 13055 8177 50  0000 C CNN
F 2 "" H 13050 8350 50  0001 C CNN
F 3 "" H 13050 8350 50  0001 C CNN
	1    13050 8350
	1    0    0    -1  
$EndComp
Text Label 13100 7450 2    50   ~ 0
-15v
Wire Wire Line
	12950 7450 13100 7450
Wire Wire Line
	12950 7550 12950 7450
Connection ~ 12950 7950
Wire Wire Line
	12950 7950 12950 8000
$Comp
L Device:R R52
U 1 1 6022BCAF
P 12950 8150
F 0 "R52" H 13200 8200 50  0000 R CNN
F 1 "1.2k" H 13200 8100 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 12880 8150 50  0001 C CNN
F 3 "~" H 12950 8150 50  0001 C CNN
F 4 "1.21 kOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 12950 8150 50  0001 C CNN "Description"
F 5 "Vishay Dale" H 12950 8150 50  0001 C CNN "Manufacturer_Name"
F 6 "RCS06031K21FKEA" H 12950 8150 50  0001 C CNN "Manufacturer_Part_Number"
	1    12950 8150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12950 7950 12950 7850
Wire Wire Line
	13350 7950 13150 7950
$Comp
L Device:R R51
U 1 1 601D6EEF
P 12950 7700
F 0 "R51" H 13019 7746 50  0000 L CNN
F 1 "3.3k" H 13019 7655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 12880 7700 50  0001 C CNN
F 3 "~" H 12950 7700 50  0001 C CNN
F 4 "3.3 kOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 12950 7700 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 12950 7700 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PA3F3301V" H 12950 7700 50  0001 C CNN "Manufacturer_Part_Number"
	1    12950 7700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14750 7650 14800 7650
$Comp
L Device:R R63
U 1 1 60183481
P 14800 7450
F 0 "R63" H 14730 7404 50  0000 R CNN
F 1 "10k" V 14800 7450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 14730 7450 50  0001 C CNN
F 3 "~" H 14800 7450 50  0001 C CNN
F 4 "RES SMD 10K OHM 1% 1/4W 0603" H 14800 7450 50  0001 C CNN "Description"
F 5 "Rohm Semiconductor" H 14800 7450 50  0001 C CNN "Manufacturer_Name"
F 6 "ESR03EZPF1002" H 14800 7450 50  0001 C CNN "Manufacturer_Part_Number"
	1    14800 7450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14150 7850 13950 7850
Text Label 13100 7750 0    50   ~ 0
IOC
Wire Wire Line
	13350 7750 13100 7750
Wire Notes Line
	10750 6550 15700 6550
Wire Wire Line
	14800 9300 14800 9450
Wire Wire Line
	14750 9300 14800 9300
$Comp
L power:GNDD #PWR059
U 1 1 6018370B
P 14800 9450
F 0 "#PWR059" H 14800 9200 50  0001 C CNN
F 1 "GNDD" H 14804 9295 50  0000 C CNN
F 2 "" H 14800 9450 50  0001 C CNN
F 3 "" H 14800 9450 50  0001 C CNN
	1    14800 9450
	1    0    0    -1  
$EndComp
Connection ~ 14800 9100
Wire Wire Line
	15100 9100 14800 9100
Text Label 15100 9100 2    50   ~ 0
#RVP
Text Label 14950 8650 2    50   ~ 0
+3.3v_DIG
Wire Wire Line
	14800 8650 14950 8650
Wire Wire Line
	14800 8750 14800 8650
Wire Wire Line
	14800 9050 14800 9100
Wire Wire Line
	14750 9100 14800 9100
$Comp
L Device:R R64
U 1 1 601DD09C
P 14800 8900
F 0 "R64" H 14730 8854 50  0000 R CNN
F 1 "10k" V 14800 8900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 14730 8900 50  0001 C CNN
F 3 "~" H 14800 8900 50  0001 C CNN
F 4 "RES SMD 10K OHM 1% 1/4W 0603" H 14800 8900 50  0001 C CNN "Description"
F 5 "Rohm Semiconductor" H 14800 8900 50  0001 C CNN "Manufacturer_Name"
F 6 "ESR03EZPF1002" H 14800 8900 50  0001 C CNN "Manufacturer_Part_Number"
	1    14800 8900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R62
U 1 1 6023796A
P 13950 8900
F 0 "R62" H 13880 8854 50  0000 R CNN
F 1 "2.2k" V 13950 8900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 13880 8900 50  0001 C CNN
F 3 "~" H 13950 8900 50  0001 C CNN
F 4 "2.2 kOhms ±1% 0.25W, 1/4W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 13950 8900 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 13950 8900 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PA3F2201V" H 13950 8900 50  0001 C CNN "Manufacturer_Part_Number"
	1    13950 8900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14150 9100 13950 9100
Wire Wire Line
	13950 9100 13950 9050
Text Label 14100 8650 2    50   ~ 0
+15v
Wire Wire Line
	13950 8650 14100 8650
Wire Wire Line
	13950 8750 13950 8650
Wire Wire Line
	13950 9300 14150 9300
Wire Wire Line
	12350 9400 12850 9400
Wire Wire Line
	13350 9200 12850 9200
Wire Wire Line
	12850 9400 12850 9500
Connection ~ 12850 9400
Wire Wire Line
	12850 9400 13350 9400
$Comp
L power:GNDA #PWR052
U 1 1 6046A877
P 12850 9500
F 0 "#PWR052" H 12850 9250 50  0001 C CNN
F 1 "GNDA" H 12855 9327 50  0000 C CNN
F 2 "" H 12850 9500 50  0001 C CNN
F 3 "" H 12850 9500 50  0001 C CNN
	1    12850 9500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11500 9300 11500 8600
Wire Wire Line
	11500 7850 11750 7850
Wire Wire Line
	11500 9300 11750 9300
Wire Wire Line
	11450 8600 11500 8600
Connection ~ 11500 8600
Wire Wire Line
	11500 8600 11500 7850
Text Label 12600 7750 2    50   ~ 0
IOC
Text Label 12900 9050 0    50   ~ 0
IOC
Wire Wire Line
	12850 9200 12850 9050
Wire Wire Line
	12850 9050 12900 9050
Connection ~ 12850 9200
Wire Wire Line
	12850 9200 12350 9200
Wire Wire Line
	6850 2200 6850 4100
Wire Notes Line
	15700 3400 15700 9900
Wire Notes Line
	10750 3400 10750 9900
Wire Notes Line
	10750 9900 15700 9900
Wire Wire Line
	14150 4900 14400 4900
Wire Wire Line
	14150 4900 14150 5150
Connection ~ 14400 4900
Wire Wire Line
	14400 4900 14400 4950
Text Label 14500 5050 2    50   ~ 0
#RVP
$Comp
L Isolator:PC817 U16
U 1 1 607CAF09
P 14450 7750
F 0 "U16" H 14450 8075 50  0000 C CNN
F 1 "‎PC81710NIP1B" H 14450 7550 50  0000 C CNN
F 2 "FootPrints:PC81710NIP1B" H 14250 7550 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 14450 7750 50  0001 L CNN
F 4 "Optoisolator Transistor Output 5000Vrms 1 Channel " H 14450 7750 50  0001 C CNN "Description"
F 5 "SHARP/Socle Technology" H 14450 7750 50  0001 C CNN "Manufacturer_Name"
F 6 "PC81710NIP1B" H 14450 7750 50  0001 C CNN "Manufacturer_Part_Number"
	1    14450 7750
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC817 U17
U 1 1 607CD5E9
P 14450 9200
F 0 "U17" H 14450 9525 50  0000 C CNN
F 1 "‎PC81710NIP1B" H 14450 9000 50  0000 C CNN
F 2 "FootPrints:PC81710NIP1B" H 14250 9000 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 14450 9200 50  0001 L CNN
F 4 "Optoisolator Transistor Output 5000Vrms 1 Channel " H 14450 9200 50  0001 C CNN "Description"
F 5 "SHARP/Socle Technology" H 14450 9200 50  0001 C CNN "Manufacturer_Name"
F 6 "PC81710NIP1B" H 14450 9200 50  0001 C CNN "Manufacturer_Part_Number"
	1    14450 9200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C43
U 1 1 6088D255
P 8650 8850
F 0 "C43" V 8879 8850 50  0000 C CNN
F 1 "100n" V 8788 8850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8650 8850 50  0001 C CNN
F 3 "~" H 8650 8850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 8650 8850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 8650 8850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 8650 8850 50  0001 C CNN "Manufacturer_Part_Number"
	1    8650 8850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 8850 8900 8850
Wire Wire Line
	8900 8850 8900 8900
Wire Wire Line
	8450 8850 8450 8950
Wire Wire Line
	8450 8850 8550 8850
Connection ~ 8900 8850
Text Label 9100 8850 2    50   ~ 0
+15v
Wire Wire Line
	8900 8850 9100 8850
$Comp
L Device:C_Small C48
U 1 1 608EC91D
P 9850 8850
F 0 "C48" V 10079 8850 50  0000 C CNN
F 1 "100n" V 9988 8850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9850 8850 50  0001 C CNN
F 3 "~" H 9850 8850 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 9850 8850 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 9850 8850 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 9850 8850 50  0001 C CNN "Manufacturer_Part_Number"
	1    9850 8850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 8850 10100 8850
Wire Wire Line
	10100 8850 10100 8900
Wire Wire Line
	9650 8850 9650 8950
Wire Wire Line
	9650 8850 9750 8850
Connection ~ 10100 8850
Text Label 10300 8850 2    50   ~ 0
+15v
Wire Wire Line
	10100 8850 10300 8850
$Comp
L power:GNDA #PWR044
U 1 1 609AE491
P 9650 8950
F 0 "#PWR044" H 9650 8700 50  0001 C CNN
F 1 "GNDA" H 9655 8777 50  0000 C CNN
F 2 "" H 9650 8950 50  0001 C CNN
F 3 "" H 9650 8950 50  0001 C CNN
	1    9650 8950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR039
U 1 1 609AE9A6
P 8450 8950
F 0 "#PWR039" H 8450 8700 50  0001 C CNN
F 1 "GNDA" H 8455 8777 50  0000 C CNN
F 2 "" H 8450 8950 50  0001 C CNN
F 3 "" H 8450 8950 50  0001 C CNN
	1    8450 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 9550 10100 9550
Wire Wire Line
	10100 9550 10100 9500
$Comp
L power:GNDA #PWR045
U 1 1 60A13108
P 9650 9650
F 0 "#PWR045" H 9650 9400 50  0001 C CNN
F 1 "GNDA" H 9655 9477 50  0000 C CNN
F 2 "" H 9650 9650 50  0001 C CNN
F 3 "" H 9650 9650 50  0001 C CNN
	1    9650 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 9550 9650 9650
Wire Wire Line
	9650 9550 9750 9550
$Comp
L Device:C_Small C49
U 1 1 60A13110
P 9850 9550
F 0 "C49" V 10079 9550 50  0000 C CNN
F 1 "100n" V 9988 9550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9850 9550 50  0001 C CNN
F 3 "~" H 9850 9550 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 9850 9550 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 9850 9550 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 9850 9550 50  0001 C CNN "Manufacturer_Part_Number"
	1    9850 9550
	0    -1   -1   0   
$EndComp
Connection ~ 10100 9550
Text Label 10300 9550 2    50   ~ 0
-15v
Wire Wire Line
	10100 9550 10300 9550
Wire Wire Line
	8750 9550 8900 9550
Wire Wire Line
	8900 9550 8900 9500
$Comp
L power:GNDA #PWR040
U 1 1 60A793F0
P 8450 9650
F 0 "#PWR040" H 8450 9400 50  0001 C CNN
F 1 "GNDA" H 8455 9477 50  0000 C CNN
F 2 "" H 8450 9650 50  0001 C CNN
F 3 "" H 8450 9650 50  0001 C CNN
	1    8450 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 9550 8450 9650
Wire Wire Line
	8450 9550 8550 9550
$Comp
L Device:C_Small C44
U 1 1 60A793F8
P 8650 9550
F 0 "C44" V 8879 9550 50  0000 C CNN
F 1 "100n" V 8788 9550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8650 9550 50  0001 C CNN
F 3 "~" H 8650 9550 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 8650 9550 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 8650 9550 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 8650 9550 50  0001 C CNN "Manufacturer_Part_Number"
	1    8650 9550
	0    -1   -1   0   
$EndComp
Connection ~ 8900 9550
Text Label 9100 9550 2    50   ~ 0
-15v
Wire Wire Line
	8900 9550 9100 9550
Connection ~ 6850 4850
Wire Wire Line
	6850 4850 6850 4900
Wire Wire Line
	6850 4500 6850 4850
Text Label 6600 4200 0    50   ~ 0
S+
Text Label 6600 4400 0    50   ~ 0
S-
$Comp
L Device:D_TVS D1
U 1 1 60BAD4D8
P 7000 1450
F 0 "D1" V 6954 1530 50  0000 L CNN
F 1 "SMCJ30CA" V 7045 1530 50  0000 L CNN
F 2 "Diode_SMD:D_SMC_Handsoldering" H 7000 1450 50  0001 C CNN
F 3 "~" H 7000 1450 50  0001 C CNN
F 4 "48.4V Clamp 31A Ipp Tvs Diode Surface Mount DO-214AB (SMCJ)" H 7000 1450 50  0001 C CNN "Description"
F 5 "Littelfuse Inc." H 7000 1450 50  0001 C CNN "Manufacturer_Name"
F 6 "SMCJ30CA" H 7000 1450 50  0001 C CNN "Manufacturer_Part_Number"
	1    7000 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 1100 7000 1100
Wire Wire Line
	6850 1800 6850 1100
Connection ~ 6850 1100
Wire Wire Line
	7000 1300 7000 1100
Wire Wire Line
	7000 1600 7000 1800
Wire Wire Line
	7000 1800 7250 1800
Text Label 7250 1800 2    50   ~ 0
IN-
$Comp
L Comparator:LM2903 U12
U 1 1 60F8C6F6
P 13650 7850
F 0 "U12" H 13650 8217 50  0000 C CNN
F 1 "LM2903" H 13650 8126 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 13650 7850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 13650 7850 50  0001 C CNN
F 4 "Comparator Differential CMOS, DTL, ECL, Open-Collector, TTL 8-SO" H 13650 7850 50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 13650 7850 50  0001 C CNN "Manufacturer_Name"
F 6 "LM2903WDT" H 13650 7850 50  0001 C CNN "Manufacturer_Part_Number"
	1    13650 7850
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U12
U 2 1 60F8DE7F
P 12050 7850
F 0 "U12" H 12050 8217 50  0000 C CNN
F 1 "LM2903" H 12050 8126 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 12050 7850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 12050 7850 50  0001 C CNN
F 4 "Comparator Differential CMOS, DTL, ECL, Open-Collector, TTL 8-SO" H 12050 7850 50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 12050 7850 50  0001 C CNN "Manufacturer_Name"
F 6 "LM2903WDT" H 12050 7850 50  0001 C CNN "Manufacturer_Part_Number"
	2    12050 7850
	-1   0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U12
U 3 1 60F8FB75
P 9000 9200
F 0 "U12" H 8958 9246 50  0000 L CNN
F 1 "LM2903" H 8958 9155 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9000 9200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 9000 9200 50  0001 C CNN
F 4 "Comparator Differential CMOS, DTL, ECL, Open-Collector, TTL 8-SO" H 9000 9200 50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 9000 9200 50  0001 C CNN "Manufacturer_Name"
F 6 "LM2903WDT" H 9000 9200 50  0001 C CNN "Manufacturer_Part_Number"
	3    9000 9200
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM2903 U14
U 1 1 60F92394
P 13650 9300
F 0 "U14" H 13650 9667 50  0000 C CNN
F 1 "LM2903" H 13650 9576 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 13650 9300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 13650 9300 50  0001 C CNN
F 4 "Comparator Differential CMOS, DTL, ECL, Open-Collector, TTL 8-SO" H 13650 9300 50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 13650 9300 50  0001 C CNN "Manufacturer_Name"
F 6 "LM2903WDT" H 13650 9300 50  0001 C CNN "Manufacturer_Part_Number"
	1    13650 9300
	1    0    0    1   
$EndComp
$Comp
L Comparator:LM2903 U14
U 2 1 60F938F0
P 12050 9300
F 0 "U14" H 12050 9667 50  0000 C CNN
F 1 "LM2903" H 12050 9576 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 12050 9300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 12050 9300 50  0001 C CNN
F 4 "Comparator Differential CMOS, DTL, ECL, Open-Collector, TTL 8-SO" H 12050 9300 50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 12050 9300 50  0001 C CNN "Manufacturer_Name"
F 6 "LM2903WDT" H 12050 9300 50  0001 C CNN "Manufacturer_Part_Number"
	2    12050 9300
	-1   0    0    1   
$EndComp
$Comp
L Comparator:LM2903 U14
U 3 1 60F952C0
P 10200 9200
F 0 "U14" H 10158 9246 50  0000 L CNN
F 1 "LM2903" H 10158 9155 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 10200 9200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 10200 9200 50  0001 C CNN
F 4 "Comparator Differential CMOS, DTL, ECL, Open-Collector, TTL 8-SO" H 10200 9200 50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 10200 9200 50  0001 C CNN "Manufacturer_Name"
F 6 "LM2903WDT" H 10200 9200 50  0001 C CNN "Manufacturer_Part_Number"
	3    10200 9200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1700 2500 1700
$Comp
L OPA189IDR:OPA189IDR U2
U 1 1 6101487B
P 2100 2100
F 0 "U2" H 2150 2300 60  0000 C CNN
F 1 "OPA189IDR" H 2250 2400 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 2400 2290 60  0001 C CNN
F 3 "" H 1800 1250 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 2100 2100 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 2100 2100 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 2100 2100 50  0001 C CNN "Manufacturer_Part_Number"
	1    2100 2100
	1    0    0    1   
$EndComp
$Comp
L OPA189IDR:OPA189IDR U1
U 2 1 61016C4D
P 1100 10650
F 0 "U1" H 1158 10778 60  0000 L CNN
F 1 "OPA189IDR" H 1158 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 1400 10840 60  0001 C CNN
F 3 "" H 800 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 1100 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 1100 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 1100 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    1100 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U5
U 1 1 6101E05B
P 3950 2200
F 0 "U5" H 4100 1950 60  0000 C CNN
F 1 "OPA189IDR" H 4250 2050 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 4250 2390 60  0001 C CNN
F 3 "" H 3650 1350 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 3950 2200 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 3950 2200 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 3950 2200 50  0001 C CNN "Manufacturer_Part_Number"
	1    3950 2200
	1    0    0    1   
$EndComp
$Comp
L OPA189IDR:OPA189IDR U2
U 2 1 6101E9E6
P 2200 10650
F 0 "U2" H 2258 10778 60  0000 L CNN
F 1 "OPA189IDR" H 2258 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 2500 10840 60  0001 C CNN
F 3 "" H 1900 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 2200 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 2200 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 2200 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    2200 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U1
U 1 1 610222FB
P 2000 7700
F 0 "U1" H 2100 8000 60  0000 C CNN
F 1 "OPA189IDR" H 2250 7900 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 2300 7890 60  0001 C CNN
F 3 "" H 1700 6850 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 2000 7700 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 2000 7700 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 2000 7700 50  0001 C CNN "Manufacturer_Part_Number"
	1    2000 7700
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U5
U 2 1 61022A9C
P 3300 10650
F 0 "U5" H 3358 10778 60  0000 L CNN
F 1 "OPA189IDR" H 3358 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 3600 10840 60  0001 C CNN
F 3 "" H 3000 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 3300 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 3300 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 3300 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    3300 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U8
U 1 1 61024B5C
P 5750 4300
F 0 "U8" H 6000 4450 60  0000 C CNN
F 1 "OPA189IDR" H 5900 4550 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 6050 4490 60  0001 C CNN
F 3 "" H 5450 3450 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 5750 4300 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 5750 4300 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 5750 4300 50  0001 C CNN "Manufacturer_Part_Number"
	1    5750 4300
	-1   0    0    1   
$EndComp
$Comp
L OPA189IDR:OPA189IDR U6
U 2 1 61026DDC
P 4400 10650
F 0 "U6" H 4458 10778 60  0000 L CNN
F 1 "OPA189IDR" H 4458 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 4700 10840 60  0001 C CNN
F 3 "" H 4100 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 4400 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 4400 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 4400 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    4400 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U9
U 2 1 610903B8
P 7700 10650
F 0 "U9" H 7758 10778 60  0000 L CNN
F 1 "OPA189IDR" H 7758 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 8000 10840 60  0001 C CNN
F 3 "" H 7400 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 7700 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 7700 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 7700 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    7700 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U10
U 2 1 61092314
P 8800 10650
F 0 "U10" H 8858 10778 60  0000 L CNN
F 1 "OPA189IDR" H 8858 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 9100 10840 60  0001 C CNN
F 3 "" H 8500 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 8800 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 8800 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 8800 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    8800 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U13
U 1 1 610980CB
P 9850 1650
F 0 "U13" H 9950 1400 60  0000 C CNN
F 1 "OPA189IDR" H 9950 1300 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 10150 1840 60  0001 C CNN
F 3 "" H 9550 800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 9850 1650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 9850 1650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 9850 1650 50  0001 C CNN "Manufacturer_Part_Number"
	1    9850 1650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U7
U 2 1 61099775
P 5500 10650
F 0 "U7" H 5558 10778 60  0000 L CNN
F 1 "OPA189IDR" H 5558 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 5800 10840 60  0001 C CNN
F 3 "" H 5200 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 5500 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 5500 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 5500 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    5500 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U8
U 2 1 6109C097
P 6600 10650
F 0 "U8" H 6658 10778 60  0000 L CNN
F 1 "OPA189IDR" H 6658 10672 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 6900 10840 60  0001 C CNN
F 3 "" H 6300 9800 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 6600 10650 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 6600 10650 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 6600 10650 50  0001 C CNN "Manufacturer_Part_Number"
	2    6600 10650
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U7
U 1 1 6109D59D
P 9050 2900
F 0 "U7" H 9250 3050 60  0000 C CNN
F 1 "OPA189IDR" H 9250 3150 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 9350 3090 60  0001 C CNN
F 3 "" H 8750 2050 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 9050 2900 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 9050 2900 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 9050 2900 50  0001 C CNN "Manufacturer_Part_Number"
	1    9050 2900
	1    0    0    1   
$EndComp
$Comp
L OPA189IDR:OPA189IDR U13
U 2 1 6109EEFF
P 9900 10600
F 0 "U13" H 9958 10728 60  0000 L CNN
F 1 "OPA189IDR" H 9958 10622 60  0000 L CNN
F 2 "FootPrints:OPA189IDR" H 10200 10790 60  0001 C CNN
F 3 "" H 9600 9750 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 9900 10600 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 9900 10600 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 9900 10600 50  0001 C CNN "Manufacturer_Part_Number"
	2    9900 10600
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U6
U 1 1 610A50B2
P 8200 4250
F 0 "U6" H 8650 4350 60  0000 C CNN
F 1 "OPA189IDR" H 8500 4450 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 8500 4440 60  0001 C CNN
F 3 "" H 7900 3400 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 8200 4250 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 8200 4250 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 8200 4250 50  0001 C CNN "Manufacturer_Part_Number"
	1    8200 4250
	-1   0    0    1   
$EndComp
$Comp
L OPA189IDR:OPA189IDR U9
U 1 1 61029A73
P 5200 6050
F 0 "U9" H 5250 6400 60  0000 C CNN
F 1 "OPA189IDR" H 5400 6300 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 5500 6240 60  0001 C CNN
F 3 "" H 4900 5200 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 5200 6050 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 5200 6050 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 5200 6050 50  0001 C CNN "Manufacturer_Part_Number"
	1    5200 6050
	1    0    0    -1  
$EndComp
$Comp
L OPA189IDR:OPA189IDR U10
U 1 1 61093E47
P 5200 7250
F 0 "U10" H 5300 7450 60  0000 C CNN
F 1 "OPA189IDR" H 5300 7550 60  0000 C CNN
F 2 "FootPrints:OPA189IDR" H 5500 7440 60  0001 C CNN
F 3 "" H 4900 6400 60  0000 C CNN
F 4 "Zero-Drift Amplifier 1 Circuit Rail-to-Rail 8-SOIC" H 5200 7250 50  0001 C CNN "Description"
F 5 "Texas Instruments" H 5200 7250 50  0001 C CNN "Manufacturer_Name"
F 6 "OPA189IDR" H 5200 7250 50  0001 C CNN "Manufacturer_Part_Number"
	1    5200 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C54
U 1 1 5FDBCF24
P 13000 5200
F 0 "C54" H 12908 5246 50  0000 R CNN
F 1 "100n" H 12908 5155 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13000 5200 50  0001 C CNN
F 3 "~" H 13000 5200 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 13000 5200 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 13000 5200 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 13000 5200 50  0001 C CNN "Manufacturer_Part_Number"
	1    13000 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	13250 5050 13250 5100
Wire Wire Line
	13000 5050 13250 5050
Wire Wire Line
	13000 5100 13000 5050
$Comp
L power:GNDD #PWR062
U 1 1 5FDBCF2D
P 13250 5100
F 0 "#PWR062" H 13250 4850 50  0001 C CNN
F 1 "GNDD" H 13254 4945 50  0000 C CNN
F 2 "" H 13250 5100 50  0001 C CNN
F 3 "" H 13250 5100 50  0001 C CNN
	1    13250 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 5300 13000 5450
Connection ~ 13000 5450
Wire Wire Line
	13000 5450 13200 5450
$Comp
L Device:C_Small C53
U 1 1 5FE8AA8F
P 11700 5200
F 0 "C53" H 11608 5246 50  0000 R CNN
F 1 "100n" H 11608 5155 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11700 5200 50  0001 C CNN
F 3 "~" H 11700 5200 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 11700 5200 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 11700 5200 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 11700 5200 50  0001 C CNN "Manufacturer_Part_Number"
	1    11700 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	11700 5050 11450 5050
Wire Wire Line
	11700 5100 11700 5050
Wire Wire Line
	11450 5050 11450 5100
$Comp
L power:GNDA #PWR061
U 1 1 5FE8AA99
P 11450 5100
F 0 "#PWR061" H 11450 4850 50  0001 C CNN
F 1 "GNDA" H 11455 4927 50  0000 C CNN
F 2 "" H 11450 5100 50  0001 C CNN
F 3 "" H 11450 5100 50  0001 C CNN
	1    11450 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	11700 5300 11700 5450
Connection ~ 11700 5450
Wire Wire Line
	11700 5450 11800 5450
Wire Wire Line
	11350 5450 11700 5450
Wire Notes Line
	4950 3350 7200 3350
Wire Notes Line
	4950 5100 7200 5100
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 6075DD63
P 8350 4800
F 0 "JP1" H 8350 5005 50  0000 C CNN
F 1 "noOffset" H 8350 4914 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8350 4800 50  0001 C CNN
F 3 "~" H 8350 4800 50  0001 C CNN
F 4 "PCB JUMPER" H 8350 4800 50  0001 C CNN "Description"
	1    8350 4800
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP2
U 1 1 6075EB99
P 8900 4800
F 0 "JP2" H 8900 5005 50  0000 C CNN
F 1 "Offset_acvtive" H 9050 4950 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8900 4800 50  0001 C CNN
F 3 "~" H 8900 4800 50  0001 C CNN
F 4 "PCB JUMPER" H 8900 4800 50  0001 C CNN "Description"
	1    8900 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 6550 8000 6550
Wire Wire Line
	6400 6750 8000 6750
$Comp
L Device:C_Small C30
U 1 1 5FF5F88E
P 6550 7650
F 0 "C30" H 6458 7696 50  0000 R CNN
F 1 "4.7u" H 6458 7605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6550 7650 50  0001 C CNN
F 3 "~" H 6550 7650 50  0001 C CNN
F 4 "4.7µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 6550 7650 50  0001 C CNN "Description"
F 5 "TDK Corporation" H 6550 7650 50  0001 C CNN "Manufacturer_Name"
F 6 "C2012X7R1H475K125AE" H 6550 7650 50  0001 C CNN "Manufacturer_Part_Number"
	1    6550 7650
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7805 U19
U 1 1 60FF5E81
P 13500 900
F 0 "U19" H 13500 1142 50  0000 C CNN
F 1 "AS7805ADTR" H 13500 1051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 13525 750 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/AS78XXA.pdf" H 13500 850 50  0001 C CNN
F 4 "" H 13500 900 50  0001 C CNN "Manufacturer"
F 5 "Linear Voltage Regulator IC  1 Output  1A TO-252-2" H 13500 900 50  0001 C CNN "Description"
F 6 "Diodes Incorporated" H 13500 900 50  0001 C CNN "Manufacturer_Name"
F 7 "AS7805ADTR-G1" H 13500 900 50  0001 C CNN "Manufacturer_Part_Number"
	1    13500 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C58
U 1 1 60FF8AB8
P 14000 1100
F 0 "C58" H 14091 1146 50  0000 L CNN
F 1 "100u" H 14091 1055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 14000 1100 50  0001 C CNN
F 3 "~" H 14000 1100 50  0001 C CNN
F 4 "" H 14000 1100 50  0001 C CNN "Voltage"
F 5 "100µF 25V Aluminum Electrolytic Capacitors Radial, Can  2000 Hrs @ 85°C" H 14000 1100 50  0001 C CNN "Description"
F 6 "Würth Elektronik" H 14000 1100 50  0001 C CNN "Manufacturer_Name"
F 7 "860010473007" H 14000 1100 50  0001 C CNN "Manufacturer_Part_Number"
	1    14000 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	13800 900  14000 900 
Wire Wire Line
	14000 900  14000 1000
Wire Wire Line
	13000 1000 13000 900 
Wire Wire Line
	13000 900  13200 900 
Wire Wire Line
	13000 1200 13000 1300
Wire Wire Line
	13000 1300 13500 1300
Wire Wire Line
	14000 1300 14000 1200
Wire Wire Line
	13500 1200 13500 1300
Connection ~ 13500 1300
Wire Wire Line
	13500 1300 14000 1300
$Comp
L power:GNDA #PWR0101
U 1 1 611A6AC1
P 13500 1300
F 0 "#PWR0101" H 13500 1050 50  0001 C CNN
F 1 "GNDA" H 13505 1127 50  0000 C CNN
F 2 "" H 13500 1300 50  0001 C CNN
F 3 "" H 13500 1300 50  0001 C CNN
	1    13500 1300
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7812 U18
U 1 1 611A7089
P 11700 900
F 0 "U18" H 11700 1142 50  0000 C CNN
F 1 "AS7812ADTR" H 11700 1051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 11725 750 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/AS78XXA.pdf" H 11700 850 50  0001 C CNN
F 4 "" H 11700 900 50  0001 C CNN "Manufacturer"
F 5 "Linear Voltage Regulator IC  1 Output  1A TO-252-2" H 11700 900 50  0001 C CNN "Description"
F 6 "Diodes Incorporated" H 11700 900 50  0001 C CNN "Manufacturer_Name"
F 7 "AS7812ADTR-G1" H 11700 900 50  0001 C CNN "Manufacturer_Part_Number"
	1    11700 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C56
U 1 1 611AA4B4
P 12200 1100
F 0 "C56" H 12291 1146 50  0000 L CNN
F 1 "100u" H 12291 1055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 12200 1100 50  0001 C CNN
F 3 "~" H 12200 1100 50  0001 C CNN
F 4 "" H 12200 1100 50  0001 C CNN "Voltage"
F 5 "100µF 25V Aluminum Electrolytic Capacitors Radial, Can  2000 Hrs @ 85°C" H 12200 1100 50  0001 C CNN "Description"
F 6 "Würth Elektronik" H 12200 1100 50  0001 C CNN "Manufacturer_Name"
F 7 "860010473007" H 12200 1100 50  0001 C CNN "Manufacturer_Part_Number"
	1    12200 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	12000 900  12200 900 
Wire Wire Line
	12200 900  12200 1000
Wire Wire Line
	11200 1000 11200 900 
Wire Wire Line
	11200 900  11400 900 
Wire Wire Line
	11200 1200 11200 1300
Wire Wire Line
	11200 1300 11700 1300
Wire Wire Line
	12200 1300 12200 1200
Wire Wire Line
	11700 1200 11700 1300
Connection ~ 11700 1300
Wire Wire Line
	11700 1300 12200 1300
$Comp
L power:GNDA #PWR0102
U 1 1 611AA4CA
P 11700 1300
F 0 "#PWR0102" H 11700 1050 50  0001 C CNN
F 1 "GNDA" H 11705 1127 50  0000 C CNN
F 2 "" H 11700 1300 50  0001 C CNN
F 3 "" H 11700 1300 50  0001 C CNN
	1    11700 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C57
U 1 1 613CFDB3
P 13000 1100
F 0 "C57" H 12908 1146 50  0000 R CNN
F 1 "330n" H 12908 1055 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 13000 1100 50  0001 C CNN
F 3 "~" H 13000 1100 50  0001 C CNN
F 4 "0.33µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 13000 1100 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 13000 1100 50  0001 C CNN "Manufacturer_Name"
F 6 "CL21B334KBFNNNE" H 13000 1100 50  0001 C CNN "Manufacturer_Part_Number"
	1    13000 1100
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C55
U 1 1 613D02E6
P 11200 1100
F 0 "C55" H 11108 1146 50  0000 R CNN
F 1 "330n" H 11108 1055 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11200 1100 50  0001 C CNN
F 3 "~" H 11200 1100 50  0001 C CNN
F 4 "0.33µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 11200 1100 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 11200 1100 50  0001 C CNN "Manufacturer_Name"
F 6 "CL21B334KBFNNNE" H 11200 1100 50  0001 C CNN "Manufacturer_Part_Number"
	1    11200 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12700 2850 13700 2850
Wire Wire Line
	13000 3100 12700 3100
Wire Wire Line
	12700 2850 12700 3100
Text Label 11200 900  2    50   ~ 0
+15v
Wire Wire Line
	15600 2500 15200 2500
Text Label 15200 2500 0    50   ~ 0
-15v
Text Label 12600 900  2    50   ~ 0
+12v
Text Label 14150 900  0    50   ~ 0
+5v_an
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 619F1112
P 800 1950
F 0 "#FLG0101" H 800 2025 50  0001 C CNN
F 1 "PWR_FLAG" H 800 2123 50  0000 C CNN
F 2 "" H 800 1950 50  0001 C CNN
F 3 "~" H 800 1950 50  0001 C CNN
	1    800  1950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0103
U 1 1 619F12FC
P 800 1950
F 0 "#PWR0103" H 800 1700 50  0001 C CNN
F 1 "GNDA" H 805 1777 50  0000 C CNN
F 2 "" H 800 1950 50  0001 C CNN
F 3 "" H 800 1950 50  0001 C CNN
	1    800  1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C60
U 1 1 619FDEB5
P 14400 1100
F 0 "C60" H 14308 1146 50  0000 R CNN
F 1 "100n" H 14308 1055 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 14400 1100 50  0001 C CNN
F 3 "~" H 14400 1100 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 14400 1100 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 14400 1100 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 14400 1100 50  0001 C CNN "Manufacturer_Part_Number"
	1    14400 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14400 1200 14400 1300
Wire Wire Line
	14400 1300 14000 1300
Connection ~ 14000 1300
Wire Wire Line
	14000 900  14400 900 
Wire Wire Line
	14400 900  14400 1000
Connection ~ 14000 900 
$Comp
L Device:C_Small C59
U 1 1 61BB404C
P 12600 1100
F 0 "C59" H 12508 1146 50  0000 R CNN
F 1 "100n" H 12508 1055 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12600 1100 50  0001 C CNN
F 3 "~" H 12600 1100 50  0001 C CNN
F 4 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 12600 1100 50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 12600 1100 50  0001 C CNN "Manufacturer_Name"
F 6 "CL10B104KB8NNNC" H 12600 1100 50  0001 C CNN "Manufacturer_Part_Number"
	1    12600 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12600 1000 12600 900 
Wire Wire Line
	12600 1300 12200 1300
Wire Wire Line
	12600 1200 12600 1300
Connection ~ 12200 1300
Wire Wire Line
	12200 900  12600 900 
Connection ~ 12200 900 
Text Label 13000 900  0    50   ~ 0
+12v
Wire Wire Line
	15550 950  15500 950 
Wire Wire Line
	15550 1050 15500 1050
$Comp
L Transistor_BJT:BCP53 Q5
U 1 1 60451A88
P 5850 2700
F 0 "Q5" H 6041 2746 50  0000 L CNN
F 1 "BCP53" H 6041 2655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 6050 2625 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/BCP53T1-D.PDF" H 5850 2700 50  0001 L CNN
F 4 "Bipolar (BJT) Transistor PNP 80V 1A 100MHz 2.2W Surface Mount SOT-223" H 5850 2700 50  0001 C CNN "Description"
F 5 "Nexperia USA Inc." H 5850 2700 50  0001 C CNN "Manufacturer_Name"
F 6 "BCP53HX" H 5850 2700 50  0001 C CNN "Manufacturer_Part_Number"
	1    5850 2700
	1    0    0    1   
$EndComp
$Comp
L Transistor_BJT:BCP53 Q2
U 1 1 6044E863
P 5350 1600
F 0 "Q2" H 5541 1646 50  0000 L CNN
F 1 "BCP53" H 5541 1555 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 5550 1525 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/BCP53T1-D.PDF" H 5350 1600 50  0001 L CNN
F 4 "Bipolar (BJT) Transistor PNP 80V 1A 100MHz 2.2W Surface Mount SOT-223" H 5350 1600 50  0001 C CNN "Description"
F 5 "Nexperia USA Inc." H 5350 1600 50  0001 C CNN "Manufacturer_Name"
F 6 "BCP53HX" H 5350 1600 50  0001 C CNN "Manufacturer_Part_Number"
	1    5350 1600
	1    0    0    1   
$EndComp
$Comp
L Device:R R29
U 1 1 60221FA7
P 11700 7250
F 0 "R29" V 11600 7250 50  0000 C CNN
F 1 "10k" V 11700 7250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11630 7250 50  0001 C CNN
F 3 "~" H 11700 7250 50  0001 C CNN
F 4 "10 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 11700 7250 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 11700 7250 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B1002V" H 11700 7250 50  0001 C CNN "Manufacturer_Part_Number"
	1    11700 7250
	0    1    1    0   
$EndComp
Wire Wire Line
	11550 7250 11400 7250
Text Label 11400 7250 0    50   ~ 0
I1
Text Label 12000 7250 0    50   ~ 0
IOC
Wire Wire Line
	11850 7250 12000 7250
$Comp
L Device:R_Shunt R22
U 1 1 6037A986
P 6850 4300
F 0 "R22" H 6762 4346 50  0000 R CNN
F 1 "50m" H 6762 4255 50  0000 R CNN
F 2 "FootPrints:LVK24R020DER" V 6780 4300 50  0001 C CNN
F 3 "~" H 6850 4300 50  0001 C CNN
F 4 "50 mOhms ±0.25% 1W Chip Resistor 2512 (6432 Metric) Current Sense, Moisture Resistant Thick Film" H 6850 4300 50  0001 C CNN "Description"
F 5 "Ohmite" H 6850 4300 50  0001 C CNN "Manufacturer_Name"
F 6 "LVK24R050CER" H 6850 4300 50  0001 C CNN "Manufacturer_Part_Number"
	1    6850 4300
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 6038AC74
P 3400 1750
F 0 "R8" H 3300 1750 50  0000 C CNN
F 1 "4.7k" V 3400 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3330 1750 50  0001 C CNN
F 3 "~" H 3400 1750 50  0001 C CNN
F 4 "4.7 kOhms ±0.1% 0.2W, 1/5W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200, Pulse Withstanding Thick Film" H 3400 1750 50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 3400 1750 50  0001 C CNN "Manufacturer_Name"
F 6 "ERJ-PB3B4701V" H 3400 1750 50  0001 C CNN "Manufacturer_Part_Number"
	1    3400 1750
	-1   0    0    1   
$EndComp
$EndSCHEMATC
