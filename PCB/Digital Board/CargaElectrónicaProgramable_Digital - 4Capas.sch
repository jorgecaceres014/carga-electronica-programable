EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U3
U 1 1 5FC86956
P 4100 5900
F 0 "U3" H 4550 7350 50  0000 C CNN
F 1 "STM32F103C8Tx" H 4500 4450 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 3500 4500 50  0001 R CNN
F 3 "https://www.st.com/resource/en/datasheet/stm32f103cb.pdf" H 4100 5900 50  0001 C CNN
F 4 "STMicroelectronics" H 4100 5900 50  0001 C CNN "Manufacturer_Name"
F 5 "‎STM32F103C8T6TR‎" H 4100 5900 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "LQFP-48" H 4100 5900 50  0001 C CNN "Package"
F 7 "ARM® Cortex®-M3 STM32F1 Microcontroller IC 32-Bit 72MHz 64KB (64K x 8) FLASH 48-LQFP (7x7)" H 4100 5900 50  0001 C CNN "Description"
	1    4100 5900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Male J4
U 1 1 5FC89B9E
P 6350 1500
F 0 "J4" H 6322 1524 50  0000 R CNN
F 1 "Power Rails" H 6322 1433 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B7B-XH-A_1x07_P2.50mm_Vertical" H 6350 1500 50  0001 C CNN
F 3 "~" H 6350 1500 50  0001 C CNN
	1    6350 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5850 1200 5850 1300
Wire Wire Line
	5850 1200 6150 1200
Wire Wire Line
	5850 1400 6150 1400
Wire Wire Line
	5850 1300 6150 1300
Connection ~ 5850 1300
Wire Wire Line
	5850 1300 5850 1400
$Comp
L power:GNDD #PWR012
U 1 1 5FC8C558
P 5600 1300
F 0 "#PWR012" H 5600 1050 50  0001 C CNN
F 1 "GNDD" H 5604 1145 50  0000 C CNN
F 2 "" H 5600 1300 50  0001 C CNN
F 3 "" H 5600 1300 50  0001 C CNN
	1    5600 1300
	1    0    0    -1  
$EndComp
Text GLabel 4850 6100 2    50   Input ~ 0
EOC_INT
Text GLabel 4850 6200 2    50   Input ~ 0
SCLK
Text GLabel 4850 6300 2    50   Input ~ 0
MISO
Text GLabel 4850 6400 2    50   Input ~ 0
MOSI
Text GLabel 3250 5700 0    50   Input ~ 0
ADC_CS
Text GLabel 3250 5800 0    50   Input ~ 0
ENC_CH_B
Text GLabel 3250 6700 0    50   Input ~ 0
ENC_CH_A
Text GLabel 3250 6400 0    50   Input ~ 0
KP_F2
Text GLabel 3250 6300 0    50   Input ~ 0
KP_F1
Text GLabel 3250 6200 0    50   Input ~ 0
KP_C4
Text GLabel 3250 6100 0    50   Input ~ 0
KP_C3
Text GLabel 3250 6000 0    50   Input ~ 0
KP_C2
Text GLabel 4850 7200 2    50   Input ~ 0
KP_C1
Text GLabel 3250 6800 0    50   Input ~ 0
ENC_BUTTON
Text GLabel 3250 6900 0    50   Input ~ 0
LoadOnOff
Text GLabel 4850 6700 2    50   Input ~ 0
D7
Wire Wire Line
	3250 6800 3400 6800
Wire Wire Line
	3250 6900 3400 6900
Wire Wire Line
	3250 7000 3400 7000
Wire Wire Line
	3250 5700 3400 5700
Wire Wire Line
	3250 5800 3400 5800
Text GLabel 4850 6600 2    50   Input ~ 0
D6
Wire Wire Line
	3250 7100 3400 7100
Text GLabel 3250 5300 0    50   Input ~ 0
LedHeartBeat
Wire Wire Line
	3250 5300 3400 5300
Text GLabel 4850 7100 2    50   Input ~ 0
SWCLK
Text GLabel 4850 7000 2    50   Input ~ 0
SWDIO
Wire Wire Line
	4850 7000 4700 7000
Wire Wire Line
	4700 7100 4850 7100
Wire Wire Line
	4850 6400 4700 6400
Wire Wire Line
	4700 6300 4850 6300
Wire Wire Line
	4700 6200 4850 6200
Wire Wire Line
	4700 6100 4850 6100
Wire Wire Line
	3900 7400 3900 7550
Wire Wire Line
	4200 7550 4200 7400
Wire Wire Line
	4100 7400 4100 7550
Wire Wire Line
	4000 7400 4000 7550
$Comp
L power:GNDD #PWR08
U 1 1 5FCA0F8B
P 4050 7550
F 0 "#PWR08" H 4050 7300 50  0001 C CNN
F 1 "GNDD" H 4054 7395 50  0000 C CNN
F 2 "" H 4050 7550 50  0001 C CNN
F 3 "" H 4050 7550 50  0001 C CNN
	1    4050 7550
	1    0    0    -1  
$EndComp
Connection ~ 4050 7550
Wire Wire Line
	4000 4400 4000 4300
Wire Wire Line
	4000 4300 4100 4300
Wire Wire Line
	4200 4300 4200 4400
Wire Wire Line
	3900 4400 3900 4300
Wire Wire Line
	3900 4300 4000 4300
Connection ~ 4000 4300
Wire Wire Line
	4100 4400 4100 4300
Connection ~ 4100 4300
Wire Wire Line
	4100 4300 4200 4300
$Comp
L Connector:USB_B J1
U 1 1 5FCBA5FE
P 1200 1150
F 0 "J1" H 970 1139 50  0000 R CNN
F 1 "USB_B" H 970 1048 50  0000 R CNN
F 2 "FootPrints:HARWIN_M701-330442" H 1350 1100 50  0001 C CNN
F 3 " ~" H 1350 1100 50  0001 C CNN
F 4 "M701-330442" H 1200 1150 50  0001 C CNN "Manufacturer_Part_Number"
F 5 "USB-B (USB TYPE-B) USB 2.0 Receptacle Connector 4 Position Through Hole" H 1200 1150 50  0001 C CNN "Description"
F 6 "Harwin Inc." H 1200 1150 50  0001 C CNN "Manufacturer_Name"
F 7 "Vertical" H 1200 1150 50  0001 C CNN "Package"
	1    1200 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1150 1500 1150
Wire Wire Line
	1600 1250 1500 1250
$Comp
L Regulator_Linear:LM1117-3.3 U1
U 1 1 5FCE5340
P 1900 2800
F 0 "U1" H 1900 3042 50  0000 C CNN
F 1 "LM1117-3.3" H 1900 2951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 1900 2800 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/lm1117.pdf?HQS=dis-dk-null-digikeymode-dsf-pf-null-wwe&ts=1608238023526" H 1900 2800 50  0001 C CNN
F 4 "Texas Instruments" H 1900 2800 50  0001 C CNN "Manufacturer_Name"
F 5 "LM1117IMP-3.3/NOPB" H 1900 2800 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "SOT-223" H 1900 2800 50  0001 C CNN "Package"
F 7 "Linear Voltage Regulator IC  1 Output  800mA SOT-223" H 1900 2800 50  0001 C CNN "Description"
	1    1900 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5FCE6828
P 1300 2950
F 0 "C6" H 1208 2996 50  0000 R CNN
F 1 "100n" H 1400 3150 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1300 2950 50  0001 C CNN
F 3 "~" H 1300 2950 50  0001 C CNN
F 4 "0603" H 1300 2950 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1300 2950 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 1300 2950 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 1300 2950 50  0001 C CNN "Manufacturer_Part_Number"
	1    1300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2850 1300 2800
Wire Wire Line
	1300 2800 1600 2800
Wire Wire Line
	1900 3100 1900 3200
Wire Wire Line
	1900 3200 1300 3200
Wire Wire Line
	1300 3200 1300 3050
Wire Wire Line
	950  2850 950  2800
Wire Wire Line
	950  2800 1300 2800
Connection ~ 1300 2800
Wire Wire Line
	950  3200 950  3050
Wire Wire Line
	950  3200 1300 3200
Connection ~ 1300 3200
Wire Wire Line
	950  2800 950  2700
Wire Wire Line
	950  2700 650  2700
Connection ~ 950  2800
Wire Wire Line
	2200 2800 2450 2800
$Comp
L Device:CP_Small C11
U 1 1 5FCFDA88
P 2450 2950
F 0 "C11" H 2538 2996 50  0000 L CNN
F 1 "10u" H 2538 2905 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-12_Kemet-R_Pad1.30x1.05mm_HandSolder" H 2450 2950 50  0001 C CNN
F 3 "~" H 2450 2950 50  0001 C CNN
F 4 "2012-12" H 2450 2950 50  0001 C CNN "Package"
F 5 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 2450 2950 50  0001 C CNN "Description"
F 6 "AVX Corporation" H 2450 2950 50  0001 C CNN "Manufacturer_Name"
F 7 "TAJP106M010RNJ" H 2450 2950 50  0001 C CNN "Manufacturer_Part_Number"
	1    2450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 2800 2450 2850
Wire Wire Line
	2450 3050 2450 3200
Wire Wire Line
	2450 3200 1900 3200
Connection ~ 1900 3200
Wire Wire Line
	2450 2800 2450 2700
Wire Wire Line
	2450 2700 2750 2700
Connection ~ 2450 2800
Wire Wire Line
	3900 4000 3900 4300
Connection ~ 3900 4300
$Comp
L power:GNDD #PWR06
U 1 1 5FD0DDF2
P 1900 3200
F 0 "#PWR06" H 1900 2950 50  0001 C CNN
F 1 "GNDD" H 1904 3045 50  0000 C CNN
F 2 "" H 1900 3200 50  0001 C CNN
F 3 "" H 1900 3200 50  0001 C CNN
	1    1900 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR02
U 1 1 5FD1262B
P 1200 2000
F 0 "#PWR02" H 1200 1750 50  0001 C CNN
F 1 "GNDD" H 1204 1845 50  0000 C CNN
F 2 "" H 1200 2000 50  0001 C CNN
F 3 "" H 1200 2000 50  0001 C CNN
	1    1200 2000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5850 1300 5600 1300
Text Label 5800 1700 0    50   ~ 0
+5vDIG
Text Label 5800 1600 0    50   ~ 0
+12vFAN
Wire Wire Line
	5800 1600 6150 1600
Wire Wire Line
	6150 1500 5800 1500
Text Label 5800 1500 0    50   ~ 0
+12vFAN
Text Label 5800 1800 0    50   ~ 0
+5vDIG
Text Label 2750 2700 2    50   ~ 0
3.3vDIG
Text Label 650  2700 0    50   ~ 0
+5vDIG
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5FD93D32
P 1200 1750
F 0 "FB1" H 1300 1796 50  0000 L CNN
F 1 "110R @ 100MHz" H 1300 1705 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1130 1750 50  0001 C CNN
F 3 "~" H 1200 1750 50  0001 C CNN
F 4 "0805" H 1200 1750 50  0001 C CNN "Package"
F 5 "BLM21SP111SN1D‎" H 1200 1750 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "110R @ 100MHz, 0805, 5A" H 1200 1750 50  0001 C CNN "Description"
F 7 "Murata" H 1200 1750 50  0001 C CNN "Manufacturer_Name"
	1    1200 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1700 6150 1700
Wire Wire Line
	5800 1800 6150 1800
Text Notes 2550 3100 0    50   ~ 0
Tantalio
Text Notes 600  3100 0    50   ~ 0
Tantalio
Text GLabel 3250 7000 0    50   Input ~ 0
RS
Text GLabel 5400 6800 2    50   Input ~ 0
USB_D-
Text GLabel 3250 7100 0    50   Input ~ 0
E
Text GLabel 5400 6900 2    50   Input ~ 0
USB_D+
Text GLabel 3250 7200 0    50   Input ~ 0
D4
Wire Wire Line
	4850 6500 4700 6500
Wire Wire Line
	4700 6600 4850 6600
Wire Wire Line
	4700 6700 4850 6700
Text GLabel 4850 6500 2    50   Input ~ 0
D5
Wire Wire Line
	3250 7200 3400 7200
Wire Wire Line
	3250 6700 3400 6700
Wire Wire Line
	4700 6800 5400 6800
Wire Wire Line
	4700 6900 5250 6900
$Comp
L Device:R R4
U 1 1 5FD2D6CB
P 5250 6600
F 0 "R4" H 5320 6646 50  0000 L CNN
F 1 "1k5" H 5320 6555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5180 6600 50  0001 C CNN
F 3 "~" H 5250 6600 50  0001 C CNN
F 4 "0603" H 5250 6600 50  0001 C CNN "Package"
F 5 "RES SMD 1.5K OHM 1% 1/4W 0603" H 5250 6600 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 5250 6600 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1501" H 5250 6600 50  0001 C CNN "Manufacturer_Part_Number"
	1    5250 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 6750 5250 6900
Connection ~ 5250 6900
Wire Wire Line
	5250 6900 5400 6900
Wire Wire Line
	5250 6450 5250 6350
Text Label 5600 6350 2    50   ~ 0
+5vDIG
Wire Wire Line
	5250 6350 5600 6350
Text GLabel 3250 6600 0    50   Input ~ 0
KP_F4
Wire Wire Line
	4700 7200 4850 7200
Text GLabel 4850 6000 2    50   Input ~ 0
DAC_CS
Wire Wire Line
	4850 6000 4700 6000
Text GLabel 4850 5900 2    50   Input ~ 0
CONVST
Wire Wire Line
	4850 5900 4700 5900
Wire Wire Line
	3250 6000 3400 6000
Wire Wire Line
	3250 6300 3400 6300
Wire Wire Line
	3250 6200 3400 6200
Wire Wire Line
	3250 6100 3400 6100
Wire Wire Line
	3250 6400 3400 6400
Wire Wire Line
	3250 6500 3400 6500
Text GLabel 3250 6500 0    50   Input ~ 0
KP_F3
Wire Wire Line
	3250 6600 3400 6600
$Comp
L Device:R R2
U 1 1 5FD9FA2E
P 2500 4550
F 0 "R2" V 2600 4500 50  0000 L CNN
F 1 "10k" V 2500 4500 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2430 4550 50  0001 C CNN
F 3 "~" H 2500 4550 50  0001 C CNN
F 4 "0603" H 2500 4550 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 2500 4550 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 2500 4550 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 2500 4550 50  0001 C CNN "Manufacturer_Part_Number"
	1    2500 4550
	1    0    0    1   
$EndComp
NoConn ~ 1500 950 
Text Label 4100 4000 2    50   ~ 0
3.3vDIG
Wire Wire Line
	1200 1550 1200 1600
Wire Wire Line
	1200 1850 1200 2000
Wire Wire Line
	1200 1600 1100 1600
Wire Wire Line
	1100 1600 1100 1550
Connection ~ 1200 1600
Wire Wire Line
	1200 1600 1200 1650
Wire Wire Line
	3400 4600 3050 4600
$Comp
L Device:C_Small C1
U 1 1 5FE0A13C
P 1150 5350
F 0 "C1" H 1058 5396 50  0000 R CNN
F 1 "1u" H 1058 5305 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1150 5350 50  0001 C CNN
F 3 "~" H 1150 5350 50  0001 C CNN
F 4 "0603" H 1150 5350 50  0001 C CNN "Package"
F 5 "Ceramico, 0603, 50v, X7R, 10%" H 1150 5350 50  0001 C CNN "Description"
F 6 "Taiyo Yuden" H 1150 5350 50  0001 C CNN "Manufacturer_Name"
F 7 "UMK107AB7105KA-T" H 1150 5350 50  0001 C CNN "Manufacturer_Part_Number"
	1    1150 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	950  5650 950  5600
Wire Wire Line
	950  5200 950  5150
$Comp
L Switch:SW_Push SW1
U 1 1 5FDE953E
P 950 5400
F 0 "SW1" V 996 5548 50  0000 L CNN
F 1 "RST" V 905 5548 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3342" H 950 5600 50  0001 C CNN
F 3 "~" H 950 5600 50  0001 C CNN
F 4 "C&K" H 950 5400 50  0001 C CNN "Manufacturer_Name"
F 5 "PTS526 SK15 SMTR2 LFS" H 950 5400 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "Tactile Switch SPST-NO Top Actuated Surface Mount" H 950 5400 50  0001 C CNN "Description"
	1    950  5400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5FDE88B3
P 1150 4900
F 0 "R1" V 1250 4850 50  0000 L CNN
F 1 "10k" V 1150 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1080 4900 50  0001 C CNN
F 3 "~" H 1150 4900 50  0001 C CNN
F 4 "0603" H 1150 4900 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 1150 4900 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 1150 4900 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 1150 4900 50  0001 C CNN "Manufacturer_Part_Number"
	1    1150 4900
	1    0    0    1   
$EndComp
$Comp
L power:GNDD #PWR04
U 1 1 5FD9F179
P 2500 4800
F 0 "#PWR04" H 2500 4550 50  0001 C CNN
F 1 "GNDD" H 2504 4645 50  0000 C CNN
F 2 "" H 2500 4800 50  0001 C CNN
F 3 "" H 2500 4800 50  0001 C CNN
	1    2500 4800
	1    0    0    -1  
$EndComp
Text Label 3050 4600 0    50   ~ 0
NRST
Wire Wire Line
	1150 5050 1150 5150
Wire Wire Line
	950  5150 1150 5150
Connection ~ 1150 5150
Wire Wire Line
	1150 5150 1150 5250
Wire Wire Line
	950  5650 1050 5650
Wire Wire Line
	1150 5650 1150 5450
$Comp
L power:GNDD #PWR01
U 1 1 5FE3FAAF
P 1050 5650
F 0 "#PWR01" H 1050 5400 50  0001 C CNN
F 1 "GNDD" H 1054 5495 50  0000 C CNN
F 2 "" H 1050 5650 50  0001 C CNN
F 3 "" H 1050 5650 50  0001 C CNN
	1    1050 5650
	1    0    0    -1  
$EndComp
Connection ~ 1050 5650
Wire Wire Line
	1050 5650 1150 5650
Text Label 1400 5150 2    50   ~ 0
NRST
Wire Wire Line
	1150 5150 1400 5150
Wire Wire Line
	1150 4750 1150 4600
Wire Wire Line
	1150 4600 1250 4600
Text Label 1250 4600 2    50   ~ 0
3.3vDIG
Wire Wire Line
	9150 4350 9150 4200
Wire Wire Line
	9050 4350 9050 4300
Wire Wire Line
	9050 5350 9050 5500
$Comp
L power:GNDD #PWR020
U 1 1 5FEA31C2
P 9450 4250
F 0 "#PWR020" H 9450 4000 50  0001 C CNN
F 1 "GNDD" H 9454 4095 50  0000 C CNN
F 2 "" H 9450 4250 50  0001 C CNN
F 3 "" H 9450 4250 50  0001 C CNN
	1    9450 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 4250 9450 4200
Wire Wire Line
	9150 4200 9450 4200
Wire Wire Line
	8600 5500 8500 5500
Wire Wire Line
	8900 5500 9050 5500
$Comp
L Device:R R6
U 1 1 5FED936D
P 8750 5500
F 0 "R6" V 8850 5450 50  0000 L CNN
F 1 "10k" V 8750 5450 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8680 5500 50  0001 C CNN
F 3 "~" H 8750 5500 50  0001 C CNN
F 4 "0603" H 8750 5500 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 8750 5500 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 8750 5500 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 8750 5500 50  0001 C CNN "Manufacturer_Part_Number"
	1    8750 5500
	0    -1   1    0   
$EndComp
Wire Wire Line
	9250 5350 9250 5500
$Comp
L Device:C_Small C14
U 1 1 5FEF6CA4
P 8500 5750
F 0 "C14" H 8408 5796 50  0000 R CNN
F 1 "10n" H 8408 5705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8500 5750 50  0001 C CNN
F 3 "~" H 8500 5750 50  0001 C CNN
F 4 "0603" H 8500 5750 50  0001 C CNN "Package"
F 5 "10000pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 8500 5750 50  0001 C CNN "Description"
F 6 "Murata Electronics" H 8500 5750 50  0001 C CNN "Manufacturer_Name"
F 7 "GRM1885C1H103JA01D" H 8500 5750 50  0001 C CNN "Manufacturer_Part_Number"
	1    8500 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5650 8500 5500
Connection ~ 8500 5500
Wire Wire Line
	8500 5500 8450 5500
Wire Wire Line
	9750 5500 9850 5500
$Comp
L Device:R R11
U 1 1 5FF0FCDA
P 9600 5500
F 0 "R11" V 9700 5450 50  0000 L CNN
F 1 "10k" V 9600 5450 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9530 5500 50  0001 C CNN
F 3 "~" H 9600 5500 50  0001 C CNN
F 4 "0603" H 9600 5500 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 9600 5500 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 9600 5500 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 9600 5500 50  0001 C CNN "Manufacturer_Part_Number"
	1    9600 5500
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C16
U 1 1 5FF0FCE0
P 9850 5750
F 0 "C16" H 9758 5796 50  0000 R CNN
F 1 "10n" H 9758 5705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9850 5750 50  0001 C CNN
F 3 "~" H 9850 5750 50  0001 C CNN
F 4 "0603" H 9850 5750 50  0001 C CNN "Package"
F 5 "10000pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 9850 5750 50  0001 C CNN "Description"
F 6 "Murata Electronics" H 9850 5750 50  0001 C CNN "Manufacturer_Name"
F 7 "GRM1885C1H103JA01D" H 9850 5750 50  0001 C CNN "Manufacturer_Part_Number"
	1    9850 5750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9850 5650 9850 5500
Connection ~ 9850 5500
Wire Wire Line
	9850 5500 9900 5500
Wire Wire Line
	9450 5500 9250 5500
Wire Wire Line
	9850 6250 9150 6250
Connection ~ 9150 6250
Wire Wire Line
	9150 6250 8500 6250
$Comp
L power:GNDD #PWR019
U 1 1 5FF33EE2
P 9150 6250
F 0 "#PWR019" H 9150 6000 50  0001 C CNN
F 1 "GNDD" H 9154 6095 50  0000 C CNN
F 2 "" H 9150 6250 50  0001 C CNN
F 3 "" H 9150 6250 50  0001 C CNN
	1    9150 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5FF3440F
P 8800 4300
F 0 "R7" V 8900 4250 50  0000 L CNN
F 1 "10k" V 8800 4250 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8730 4300 50  0001 C CNN
F 3 "~" H 8800 4300 50  0001 C CNN
F 4 "0603" H 8800 4300 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 8800 4300 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 8800 4300 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 8800 4300 50  0001 C CNN "Manufacturer_Part_Number"
	1    8800 4300
	0    1    -1   0   
$EndComp
Wire Wire Line
	8950 4300 9050 4300
$Comp
L Device:C_Small C13
U 1 1 5FF3CE87
P 8500 4500
F 0 "C13" H 8408 4546 50  0000 R CNN
F 1 "10n" H 8408 4455 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8500 4500 50  0001 C CNN
F 3 "~" H 8500 4500 50  0001 C CNN
F 4 "0603" H 8500 4500 50  0001 C CNN "Package"
F 5 "10000pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 8500 4500 50  0001 C CNN "Description"
F 6 "Murata Electronics" H 8500 4500 50  0001 C CNN "Manufacturer_Name"
F 7 "GRM1885C1H103JA01D" H 8500 4500 50  0001 C CNN "Manufacturer_Part_Number"
	1    8500 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 4300 8500 4300
Wire Wire Line
	8500 4300 8500 4400
$Comp
L power:GNDD #PWR017
U 1 1 5FF44F39
P 8500 4700
F 0 "#PWR017" H 8500 4450 50  0001 C CNN
F 1 "GNDD" H 8504 4545 50  0000 C CNN
F 2 "" H 8500 4700 50  0001 C CNN
F 3 "" H 8500 4700 50  0001 C CNN
	1    8500 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 4700 8500 4600
$Comp
L Device:R R9
U 1 1 5FF4CDEB
P 9050 5700
F 0 "R9" H 9100 5700 50  0000 L CNN
F 1 "10k" V 9050 5650 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8980 5700 50  0001 C CNN
F 3 "~" H 9050 5700 50  0001 C CNN
F 4 "0603" H 9050 5700 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 9050 5700 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 9050 5700 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 9050 5700 50  0001 C CNN "Manufacturer_Part_Number"
	1    9050 5700
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5FF4D1EF
P 9250 5700
F 0 "R10" H 9300 5700 50  0000 L CNN
F 1 "10k" V 9250 5650 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9180 5700 50  0001 C CNN
F 3 "~" H 9250 5700 50  0001 C CNN
F 4 "0603" H 9250 5700 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 9250 5700 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 9250 5700 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 9250 5700 50  0001 C CNN "Manufacturer_Part_Number"
	1    9250 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5550 9050 5500
Connection ~ 9050 5500
Wire Wire Line
	9250 5550 9250 5500
Connection ~ 9250 5500
Wire Wire Line
	9250 5850 9250 5950
Wire Wire Line
	9050 5850 9050 5950
Text Label 9550 5950 2    50   ~ 0
3.3vDIG
Wire Wire Line
	9250 5950 9550 5950
Text Label 8750 5950 0    50   ~ 0
3.3vDIG
Wire Wire Line
	8750 5950 9050 5950
Wire Wire Line
	9050 4300 9050 4100
Connection ~ 9050 4300
$Comp
L Device:R R8
U 1 1 5FF93A5C
P 9050 3950
F 0 "R8" H 9100 3950 50  0000 L CNN
F 1 "10k" V 9050 3900 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8980 3950 50  0001 C CNN
F 3 "~" H 9050 3950 50  0001 C CNN
F 4 "0603" H 9050 3950 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 9050 3950 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 9050 3950 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 9050 3950 50  0001 C CNN "Manufacturer_Part_Number"
	1    9050 3950
	1    0    0    1   
$EndComp
Wire Wire Line
	9050 3800 9050 3750
Text Label 8800 3750 0    50   ~ 0
3.3vDIG
Wire Wire Line
	8800 3750 9050 3750
Wire Wire Line
	8500 5850 8500 6250
Wire Wire Line
	9850 5850 9850 6250
Text GLabel 4850 5800 2    50   Input ~ 0
FAN
Wire Wire Line
	4850 5800 4700 5800
$Comp
L Device:Crystal_GND24 Y1
U 1 1 5FFC822E
P 2050 5400
F 0 "Y1" H 2150 5600 50  0000 L CNN
F 1 "8MHz" H 2200 5500 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_Abracon_ABM3B-4Pin_5.0x3.2mm" H 2050 5400 50  0001 C CNN
F 3 "https://abracon.com/Resonators/abm3b.pdf" H 2050 5400 50  0001 C CNN
F 4 "Abracon LLC" H 2050 5400 50  0001 C CNN "Manufacturer_Name"
F 5 "ABM3B-8.000MHZ-10-1-U-T" H 2050 5400 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "4-SMD" H 2050 5400 50  0001 C CNN "Package"
F 7 "8MHz ±10ppm Crystal 10pF 200 Ohms 4-SMD, No Lead" H 2050 5400 50  0001 C CNN "Description"
	1    2050 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5400 2300 5400
Wire Wire Line
	1900 5400 1850 5400
Text Notes 1450 6150 0    50   ~ 0
CL = 2 * (Cload-  Cstray)
$Comp
L Device:C_Small C3
U 1 1 5FFF09D7
P 1850 5700
F 0 "C3" H 1758 5746 50  0000 R CNN
F 1 "30p" H 1758 5655 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1850 5700 50  0001 C CNN
F 3 "~" H 1850 5700 50  0001 C CNN
F 4 "0603" H 1850 5700 50  0001 C CNN "Package"
F 5 "30pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 1850 5700 50  0001 C CNN "Description"
F 6 "AVX Corporation" H 1850 5700 50  0001 C CNN "Manufacturer_Name"
F 7 "06035A300JAT2A" H 1850 5700 50  0001 C CNN "Manufacturer_Part_Number"
	1    1850 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5FFF15B3
P 2300 5700
F 0 "C7" H 2208 5746 50  0000 R CNN
F 1 "30p" H 2208 5655 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2300 5700 50  0001 C CNN
F 3 "~" H 2300 5700 50  0001 C CNN
F 4 "0603" H 2300 5700 50  0001 C CNN "Package"
F 5 "30pF ±5% 50V Ceramic Capacitor C0G, NP0 0603 (1608 Metric)" H 2300 5700 50  0001 C CNN "Description"
F 6 "AVX Corporation" H 2300 5700 50  0001 C CNN "Manufacturer_Name"
F 7 "06035A300JAT2A" H 2300 5700 50  0001 C CNN "Manufacturer_Part_Number"
	1    2300 5700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2300 5600 2300 5400
Connection ~ 2300 5400
Wire Wire Line
	2300 5400 2400 5400
Wire Wire Line
	1850 5600 1850 5400
Connection ~ 1850 5400
Wire Wire Line
	1850 5400 1750 5400
Wire Wire Line
	1850 5800 1850 5900
Wire Wire Line
	1850 5900 2050 5900
Wire Wire Line
	2300 5900 2300 5800
Wire Wire Line
	2050 5600 2050 5900
Connection ~ 2050 5900
Wire Wire Line
	2050 5900 2300 5900
Wire Wire Line
	2050 5200 2050 5100
Wire Wire Line
	1550 5100 1550 5900
Wire Wire Line
	1550 5900 1850 5900
Wire Wire Line
	1550 5100 2050 5100
Connection ~ 1850 5900
$Comp
L power:GNDD #PWR03
U 1 1 60021ACF
P 2050 5900
F 0 "#PWR03" H 2050 5650 50  0001 C CNN
F 1 "GNDD" H 2054 5745 50  0000 C CNN
F 2 "" H 2050 5900 50  0001 C CNN
F 3 "" H 2050 5900 50  0001 C CNN
	1    2050 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5100 3400 5100
Wire Wire Line
	2400 5100 2400 5400
Wire Wire Line
	1750 5000 1750 5400
Wire Wire Line
	1750 5000 3400 5000
Wire Wire Line
	2500 4700 2500 4800
Wire Wire Line
	3000 4800 3000 4300
Wire Wire Line
	3000 4300 2500 4300
Wire Wire Line
	2500 4300 2500 4400
Wire Wire Line
	3000 4800 3400 4800
Text GLabel 7600 2350 0    50   Input ~ 0
KP_C1
Text GLabel 7600 2450 0    50   Input ~ 0
KP_C2
Text GLabel 7600 2550 0    50   Input ~ 0
KP_C3
Text GLabel 7600 2650 0    50   Input ~ 0
KP_C4
Text GLabel 7600 2750 0    50   Input ~ 0
KP_F1
Text GLabel 7600 2850 0    50   Input ~ 0
KP_F2
Text GLabel 7600 2950 0    50   Input ~ 0
KP_F3
Text GLabel 7600 3050 0    50   Input ~ 0
KP_F4
Wire Wire Line
	7600 2350 7700 2350
Wire Wire Line
	7600 2450 7700 2450
Wire Wire Line
	7600 2550 7700 2550
Wire Wire Line
	7600 2650 7700 2650
Wire Wire Line
	7600 2750 7700 2750
Wire Wire Line
	7600 2850 7700 2850
Wire Wire Line
	7600 3050 7700 3050
Wire Wire Line
	7600 2950 7700 2950
$Comp
L Device:LED D1
U 1 1 600F4E5C
P 4950 4700
F 0 "D1" V 4989 4582 50  0000 R CNN
F 1 "LED" V 4898 4582 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4950 4700 50  0001 C CNN
F 3 "~" H 4950 4700 50  0001 C CNN
F 4 "0805" H 4950 4700 50  0001 C CNN "Package"
F 5 "Amber 605nm LED Indication - Discrete 2V 0805 (2012 Metric)" H 4950 4700 50  0001 C CNN "Description"
F 6 "150080AS75000" H 4950 4700 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "Würth Elektronik" H 4950 4700 50  0001 C CNN "Manufacturer_Name"
	1    4950 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 600F8925
P 4950 5100
F 0 "R3" V 5050 5050 50  0000 L CNN
F 1 "1k" V 4950 5050 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4880 5100 50  0001 C CNN
F 3 "~" H 4950 5100 50  0001 C CNN
F 4 "0603" H 4950 5100 50  0001 C CNN "Package"
F 5 "RES SMD 1K OHM 1% 1/4W 0603" H 4950 5100 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 4950 5100 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1001" H 4950 5100 50  0001 C CNN "Manufacturer_Part_Number"
	1    4950 5100
	1    0    0    1   
$EndComp
Wire Wire Line
	4950 4850 4950 4950
$Comp
L power:GNDD #PWR09
U 1 1 60104489
P 4950 5350
F 0 "#PWR09" H 4950 5100 50  0001 C CNN
F 1 "GNDD" H 4954 5195 50  0000 C CNN
F 2 "" H 4950 5350 50  0001 C CNN
F 3 "" H 4950 5350 50  0001 C CNN
	1    4950 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 5250 4950 5350
Wire Wire Line
	4950 4550 4950 4300
Text GLabel 5000 4300 2    50   Input ~ 0
LedHeartBeat
Wire Wire Line
	5000 4300 4950 4300
Text GLabel 9900 5500 2    50   Input ~ 0
ENC_CH_B
Text GLabel 8450 5500 0    50   Input ~ 0
ENC_CH_A
Text GLabel 8450 4300 0    50   Input ~ 0
ENC_BUTTON
Wire Wire Line
	8500 4300 8450 4300
Connection ~ 8500 4300
$Comp
L Device:C_Small C2
U 1 1 60140031
P 1000 3750
F 0 "C2" H 908 3796 50  0000 R CNN
F 1 "4.7u" H 908 3705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1000 3750 50  0001 C CNN
F 3 "~" H 1000 3750 50  0001 C CNN
F 4 "0805" H 1000 3750 50  0001 C CNN "Package"
F 5 "4.7µF ±10% 50V Ceramic Capacitor X7R 0805 (2012 Metric)" H 1000 3750 50  0001 C CNN "Description"
F 6 "TDK Corporation" H 1000 3750 50  0001 C CNN "Manufacturer_Name"
F 7 "C2012X7R1H475K125AE" H 1000 3750 50  0001 C CNN "Manufacturer_Part_Number"
	1    1000 3750
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 6014056F
P 1350 3750
F 0 "C5" H 1258 3796 50  0000 R CNN
F 1 "100n" H 1258 3705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 3750 50  0001 C CNN
F 3 "~" H 1350 3750 50  0001 C CNN
F 4 "0603" H 1350 3750 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1350 3750 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 1350 3750 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 1350 3750 50  0001 C CNN "Manufacturer_Part_Number"
	1    1350 3750
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 60140ADB
P 1750 3750
F 0 "C8" H 1658 3796 50  0000 R CNN
F 1 "100n" H 1658 3705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1750 3750 50  0001 C CNN
F 3 "~" H 1750 3750 50  0001 C CNN
F 4 "0603" H 1750 3750 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 1750 3750 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 1750 3750 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 1750 3750 50  0001 C CNN "Manufacturer_Part_Number"
	1    1750 3750
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 60140C89
P 2150 3750
F 0 "C9" H 2058 3796 50  0000 R CNN
F 1 "100n" H 2058 3705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2150 3750 50  0001 C CNN
F 3 "~" H 2150 3750 50  0001 C CNN
F 4 "0603" H 2150 3750 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 2150 3750 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 2150 3750 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 2150 3750 50  0001 C CNN "Manufacturer_Part_Number"
	1    2150 3750
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 60140E32
P 2550 3750
F 0 "C10" H 2458 3796 50  0000 R CNN
F 1 "100n" H 2458 3705 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2550 3750 50  0001 C CNN
F 3 "~" H 2550 3750 50  0001 C CNN
F 4 "0603" H 2550 3750 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 2550 3750 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 2550 3750 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 2550 3750 50  0001 C CNN "Manufacturer_Part_Number"
	1    2550 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2550 3850 2550 3950
Wire Wire Line
	2550 3950 2150 3950
Wire Wire Line
	1000 3950 1000 3850
Wire Wire Line
	1000 3650 1000 3550
Wire Wire Line
	2550 3550 2550 3650
Wire Wire Line
	2150 3650 2150 3550
Wire Wire Line
	1000 3550 1350 3550
Connection ~ 2150 3550
Wire Wire Line
	2150 3550 2550 3550
Wire Wire Line
	1750 3650 1750 3550
Connection ~ 1750 3550
Wire Wire Line
	1750 3550 2150 3550
Wire Wire Line
	1350 3550 1350 3650
Connection ~ 1350 3550
Wire Wire Line
	1350 3550 1750 3550
Wire Wire Line
	1350 3850 1350 3950
Connection ~ 1350 3950
Wire Wire Line
	1350 3950 1000 3950
Wire Wire Line
	1750 3850 1750 3950
Connection ~ 1750 3950
Wire Wire Line
	1750 3950 1350 3950
Wire Wire Line
	2150 3850 2150 3950
Connection ~ 2150 3950
Wire Wire Line
	2150 3950 1750 3950
Wire Wire Line
	1000 3550 850  3550
Connection ~ 1000 3550
Text Label 850  3550 2    50   ~ 0
3.3vDIG
$Comp
L power:GNDD #PWR05
U 1 1 601B6E20
P 1750 4000
F 0 "#PWR05" H 1750 3750 50  0001 C CNN
F 1 "GNDD" H 1754 3845 50  0000 C CNN
F 2 "" H 1750 4000 50  0001 C CNN
F 3 "" H 1750 4000 50  0001 C CNN
	1    1750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4000 1750 3950
$Comp
L Device:Ferrite_Bead_Small FB2
U 1 1 601D5AAB
P 4300 4150
F 0 "FB2" H 4400 4196 50  0000 L CNN
F 1 "110R @ 100MHz" H 4400 4105 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4230 4150 50  0001 C CNN
F 3 "~" H 4300 4150 50  0001 C CNN
F 4 "0805" H 4300 4150 50  0001 C CNN "Package"
F 5 "BLM21SP111SN1D‎" H 4300 4150 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "110R @ 100MHz, 0805, 5A" H 4300 4150 50  0001 C CNN "Description"
F 7 "Murata" H 4300 4150 50  0001 C CNN "Manufacturer_Name"
	1    4300 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 4250 4300 4350
Wire Wire Line
	4300 4000 4300 4050
Wire Wire Line
	3900 4000 4300 4000
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 601F4D9B
P 7350 4550
F 0 "J5" H 7430 4542 50  0000 L CNN
F 1 "Cooler" H 7430 4451 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-2-5.08_1x02_P5.08mm_Horizontal" H 7350 4550 50  0001 C CNN
F 3 "~" H 7350 4550 50  0001 C CNN
F 4 "Adam Tech" H 7350 4550 50  0001 C CNN "Manufacturer_Name"
F 5 "EB21A-02-D" H 7350 4550 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "2 Position Wire to Board Terminal Block Horizontal with Board 0.200\" (5.08mm) Through Hole" H 7350 4550 50  0001 C CNN "Description"
	1    7350 4550
	1    0    0    -1  
$EndComp
Text Label 6800 4550 0    50   ~ 0
+12vFAN
Wire Wire Line
	6800 4550 6850 4550
Wire Wire Line
	7150 4650 7050 4650
$Comp
L power:GNDD #PWR015
U 1 1 6023471D
P 7050 6050
F 0 "#PWR015" H 7050 5800 50  0001 C CNN
F 1 "GNDD" H 7054 5895 50  0000 C CNN
F 2 "" H 7050 6050 50  0001 C CNN
F 3 "" H 7050 6050 50  0001 C CNN
	1    7050 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5900 7050 6050
$Comp
L Device:D_Schottky D2
U 1 1 6024408E
P 6850 4800
F 0 "D2" V 6804 4720 50  0000 R CNN
F 1 "‎VS-10MQ100-M3/5AT" V 6895 4720 50  0000 R CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 6850 4800 50  0001 C CNN
F 3 "~" H 6850 4800 50  0001 C CNN
F 4 "D-SMA" H 6850 4800 50  0001 C CNN "Package"
F 5 "DIODO SCHOTTKY 100V 1A DO214AC" H 6850 4800 50  0001 C CNN "Description"
F 6 "‎VS-10MQ100-M3/5AT‎" H 6850 4800 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "Vishay Semiconductor" H 6850 4800 50  0001 C CNN "Manufacturer_Name"
	1    6850 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 4950 6850 5000
Wire Wire Line
	6850 5000 7050 5000
Wire Wire Line
	7050 4650 7050 5000
Wire Wire Line
	7050 5100 7050 5000
Connection ~ 7050 5000
Wire Wire Line
	6850 4650 6850 4550
Connection ~ 6850 4550
Wire Wire Line
	6850 4550 7150 4550
$Comp
L Device:R R5
U 1 1 60282AB5
P 6000 5450
F 0 "R5" V 5900 5400 50  0000 L CNN
F 1 "10k" V 6000 5400 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5930 5450 50  0001 C CNN
F 3 "~" H 6000 5450 50  0001 C CNN
F 4 "0603" H 6000 5450 50  0001 C CNN "Package"
F 5 "RES SMD 10K OHM 1% 1/4W 0603" H 6000 5450 50  0001 C CNN "Description"
F 6 "Rohm Semiconductor" H 6000 5450 50  0001 C CNN "Manufacturer_Name"
F 7 "ESR03EZPF1002" H 6000 5450 50  0001 C CNN "Manufacturer_Part_Number"
	1    6000 5450
	0    1    1    0   
$EndComp
Text GLabel 5750 5450 0    50   Input ~ 0
FAN
Wire Wire Line
	5750 5450 5850 5450
$Comp
L Device:Q_NPN_ECB Q2
U 1 1 602B3E51
P 6950 5700
F 0 "Q2" H 7141 5746 50  0000 L CNN
F 1 "PBSS4250X" H 7141 5655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3_Handsoldering" H 7150 5800 50  0001 C CNN
F 3 "~" H 6950 5700 50  0001 C CNN
F 4 "PBSS4250X,135" H 6950 5700 50  0001 C CNN "Manufacturer_Part_Number"
F 5 "NexPeria USA" H 6950 5700 50  0001 C CNN "Manufacturer_Name"
F 6 "SOT-89" H 6950 5700 50  0001 C CNN "Package"
F 7 "Bipolar (BJT) Transistor NPN 50V 2A 100MHz 1W Surface Mount SOT-89" H 6950 5700 50  0001 C CNN "Description"
	1    6950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5500 7050 5100
Connection ~ 7050 5100
Wire Wire Line
	6150 5450 6250 5450
$Comp
L Transistor_BJT:MMBT3904 Q1
U 1 1 6033914D
P 6450 5450
F 0 "Q1" H 6641 5496 50  0000 L CNN
F 1 "MMBT3904" H 6641 5405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6650 5375 50  0001 L CIN
F 3 "" H 6450 5450 50  0001 L CNN
F 4 "NexPeria USA" H 6450 5450 50  0001 C CNN "Manufacturer_Name"
F 5 "MMBT3904,215" H 6450 5450 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "SOT-23" H 6450 5450 50  0001 C CNN "Package"
F 7 "Bipolar (BJT) Transistor NPN 40V 200mA 300MHz 250mW Surface Mount TO-236AB" H 6450 5450 50  0001 C CNN "Description"
	1    6450 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 5100 6550 5250
Wire Wire Line
	6550 5700 6550 5650
Wire Wire Line
	6550 5700 6750 5700
Wire Wire Line
	6550 5100 7050 5100
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 6036CBC0
P 4650 1450
F 0 "J2" H 4700 1867 50  0000 C CNN
F 1 "Prog/Debugger" H 4700 1776 50  0000 C CNN
F 2 "FootPrints:3020-10-0300-00-TR" H 4650 1450 50  0001 C CNN
F 3 "~" H 4650 1450 50  0001 C CNN
F 4 "3020-10-0300-00" H 4650 1450 50  0001 C CNN "Manufacturer_Part_Number"
F 5 "Connector Header Surface Mount 10 position 0.100\" (2.54mm)" H 4650 1450 50  0001 C CNN "Description"
F 6 "CNC Tech" H 4650 1450 50  0001 C CNN "Manufacturer_Name"
F 7 "IDC" H 4650 1450 50  0001 C CNN "Package"
	1    4650 1450
	1    0    0    -1  
$EndComp
NoConn ~ 4450 1250
NoConn ~ 4450 1650
NoConn ~ 4950 1650
NoConn ~ 4950 1550
NoConn ~ 4450 1550
Wire Wire Line
	4950 1450 5050 1450
$Comp
L power:GNDD #PWR010
U 1 1 603DDB54
P 4700 2050
F 0 "#PWR010" H 4700 1800 50  0001 C CNN
F 1 "GNDD" H 4704 1895 50  0000 C CNN
F 2 "" H 4700 2050 50  0001 C CNN
F 3 "" H 4700 2050 50  0001 C CNN
	1    4700 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 1450 5050 1800
Wire Wire Line
	4350 1800 4350 1450
Wire Wire Line
	4350 1800 4700 1800
Wire Wire Line
	4350 1450 4450 1450
$Comp
L Device:Ferrite_Bead_Small FB3
U 1 1 603F03BD
P 4700 1900
F 0 "FB3" H 4800 1946 50  0000 L CNN
F 1 "110R @ 100MHz" H 4800 1855 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 1900 50  0001 C CNN
F 3 "~" H 4700 1900 50  0001 C CNN
F 4 "0805" H 4700 1900 50  0001 C CNN "Package"
F 5 "BLM21SP111SN1D‎" H 4700 1900 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "110R @ 100MHz, 0805, 5A" H 4700 1900 50  0001 C CNN "Description"
F 7 "Murata" H 4700 1900 50  0001 C CNN "Manufacturer_Name"
	1    4700 1900
	1    0    0    -1  
$EndComp
Connection ~ 4700 1800
Wire Wire Line
	4700 1800 5050 1800
Wire Wire Line
	4700 2050 4700 2000
Text GLabel 5050 1250 2    50   Input ~ 0
SWCLK
Wire Wire Line
	5050 1250 4950 1250
Text GLabel 5050 1350 2    50   Input ~ 0
SWDIO
Wire Wire Line
	4950 1350 5050 1350
NoConn ~ 4450 1350
$Comp
L PEC11L-4115F-S0020:PEC11L-4115F-S0020 IC1
U 1 1 604593B4
P 8650 4850
F 0 "IC1" H 9694 4896 50  0000 L CNN
F 1 "PEC11L-4115F-S0020" H 9694 4805 50  0000 L CNN
F 2 "FootPrints:PEC11L4115FS0020" H 9500 5150 50  0001 L CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/PEC11L.pdf" H 9500 5050 50  0001 L CNN
F 4 "ROTARY ENCODER MECHANICAL 20PPR" H 9500 4950 50  0001 L CNN "Description"
F 5 "25" H 9500 4850 50  0001 L CNN "Height"
F 6 "652-PEC11L4115FS0020" H 9500 4750 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Bourns/PEC11L-4115F-S0020?qs=gk21WLQFtgRAgrgJZfpkWw%3D%3D" H 9500 4650 50  0001 L CNN "Mouser Price/Stock"
F 8 "Bourns" H 9500 4550 50  0001 L CNN "Manufacturer_Name"
F 9 "PEC11L-4115F-S0020" H 9500 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 10 "Vertical" H 8650 4850 50  0001 C CNN "Package"
	1    8650 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 5350 9150 6250
NoConn ~ 8650 4850
NoConn ~ 9650 4850
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J6
U 1 1 604FA7A3
P 7600 1600
F 0 "J6" H 7650 2117 50  0000 C CNN
F 1 "I/O" H 7650 2026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 7600 1600 50  0001 C CNN
F 3 "~" H 7600 1600 50  0001 C CNN
F 4 "Adam Tech" H 7600 1600 50  0001 C CNN "Manufacturer_Name"
F 5 "PH2-14-UA" H 7600 1600 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "Connector Header Through Hole 14 position 0.100\" (2.54mm)" H 7600 1600 50  0001 C CNN "Description"
	1    7600 1600
	1    0    0    -1  
$EndComp
Text Label 7050 1300 0    50   ~ 0
3.3vDIG
Wire Wire Line
	7400 1300 7050 1300
Text Label 7050 1400 0    50   ~ 0
3.3vDIG
Wire Wire Line
	7400 1400 7050 1400
Text GLabel 7900 1800 2    50   Input ~ 0
ADC_CS
Text GLabel 7900 1300 2    50   Input ~ 0
CONVST
Text GLabel 7900 1400 2    50   Input ~ 0
DAC_CS
Text GLabel 7900 1500 2    50   Input ~ 0
SCLK
Text GLabel 7900 1600 2    50   Input ~ 0
MOSI
Text GLabel 7900 1700 2    50   Input ~ 0
MISO
Text GLabel 7900 1900 2    50   Input ~ 0
LoadOnOff
Text GLabel 7400 1500 0    50   Input ~ 0
EOC_INT
$Comp
L power:GNDD #PWR016
U 1 1 60557054
P 7050 1950
F 0 "#PWR016" H 7050 1700 50  0001 C CNN
F 1 "GNDD" H 7054 1795 50  0000 C CNN
F 2 "" H 7050 1950 50  0001 C CNN
F 3 "" H 7050 1950 50  0001 C CNN
	1    7050 1950
	1    0    0    -1  
$EndComp
Text Notes 6300 3500 0    50   ~ 0
A
Text Notes 6300 3600 0    50   ~ 0
k
$Comp
L Device:R_POT_TRIM RV1
U 1 1 605F1140
P 5100 2500
F 0 "RV1" H 5030 2546 50  0000 R CNN
F 1 "10k" H 5030 2455 50  0000 R CNN
F 2 "FootPrints:TC33X-2_1" H 5100 2500 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/TC33.pdf" H 5100 2500 50  0001 C CNN
F 4 "Bourns" H 5100 2500 50  0001 C CNN "Manufacturer_Name"
F 5 "TC33X-2-103E" H 5100 2500 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "TRIMMER 10K OHM 0.1W J LEAD TOP" H 5100 2500 50  0001 C CNN "Description"
	1    5100 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2350 5100 2250
Wire Wire Line
	5100 2250 5250 2250
$Comp
L power:GNDD #PWR011
U 1 1 6060E516
P 5100 2750
F 0 "#PWR011" H 5100 2500 50  0001 C CNN
F 1 "GNDD" H 5104 2595 50  0000 C CNN
F 2 "" H 5100 2750 50  0001 C CNN
F 3 "" H 5100 2750 50  0001 C CNN
	1    5100 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2650 5100 2750
Text GLabel 5250 2500 2    50   Input ~ 0
V0
Text Label 5250 2250 2    50   ~ 0
+5vDIG
$Comp
L Connector:Conn_01x08_Male J7
U 1 1 6062CE8A
P 7900 2650
F 0 "J7" H 7872 2624 50  0000 R CNN
F 1 "keyPad" H 7872 2533 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 7900 2650 50  0001 C CNN
F 3 "~" H 7900 2650 50  0001 C CNN
F 4 "Adam Tech" H 7900 2650 50  0001 C CNN "Manufacturer_Name"
F 5 "PH1-16-UA‎" H 7900 2650 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "Connector Header Through Hole 16 position 0.100\" (2.54mm)" H 7900 2650 50  0001 C CNN "Description"
	1    7900 2650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3900 7550 4000 7550
Wire Wire Line
	4050 7550 4100 7550
Text GLabel 4850 5700 2    50   Input ~ 0
Temp
Wire Wire Line
	4850 5700 4700 5700
Text GLabel 9900 2400 2    50   Input ~ 0
Temp
$Comp
L Device:C_Small C12
U 1 1 6078891F
P 8600 2300
F 0 "C12" H 8508 2346 50  0000 R CNN
F 1 "100n" H 8508 2255 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8600 2300 50  0001 C CNN
F 3 "~" H 8600 2300 50  0001 C CNN
F 4 "0603" H 8600 2300 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 8600 2300 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 8600 2300 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 8600 2300 50  0001 C CNN "Manufacturer_Part_Number"
	1    8600 2300
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Temperature:LM35-LP U4
U 1 1 6079870B
P 9300 2400
F 0 "U4" H 8971 2446 50  0000 R CNN
F 1 "LM35-LP" H 8971 2355 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92" H 9350 2150 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 9300 2400 50  0001 C CNN
F 4 "Texas Instruments" H 9300 2400 50  0001 C CNN "Manufacturer_Name"
F 5 "LM35DZ/NOPB‎" H 9300 2400 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "TO92-3" H 9300 2400 50  0001 C CNN "Package"
F 7 "Temperature Sensor Analog, Local 0°C ~ 100°C 10mV/°C TO-92-3" H 9300 2400 50  0001 C CNN "Description"
	1    9300 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 1950 9300 2100
Wire Wire Line
	8600 1950 9300 1950
Connection ~ 4000 7550
Wire Wire Line
	4000 7550 4050 7550
Connection ~ 4100 7550
Wire Wire Line
	4100 7550 4200 7550
$Comp
L power:GNDD #PWR018
U 1 1 607B998B
P 9300 2900
F 0 "#PWR018" H 9300 2650 50  0001 C CNN
F 1 "GNDD" H 9304 2745 50  0000 C CNN
F 2 "" H 9300 2900 50  0001 C CNN
F 3 "" H 9300 2900 50  0001 C CNN
	1    9300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2900 9300 2800
Wire Wire Line
	9300 2800 8600 2800
Wire Wire Line
	8600 2800 8600 2400
Connection ~ 9300 2800
Wire Wire Line
	9300 2800 9300 2700
Wire Wire Line
	8600 1950 8600 2200
Text Label 8950 1950 0    50   ~ 0
+5vDIG
$Comp
L Device:C_Small C15
U 1 1 607E96FA
P 9800 2650
F 0 "C15" H 9708 2696 50  0000 R CNN
F 1 "100n" H 9708 2605 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9800 2650 50  0001 C CNN
F 3 "~" H 9800 2650 50  0001 C CNN
F 4 "0603" H 9800 2650 50  0001 C CNN "Package"
F 5 "0.1µF ±10% 50V Ceramic Capacitor X7R 0603 (1608 Metric)" H 9800 2650 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 9800 2650 50  0001 C CNN "Manufacturer_Name"
F 7 "CL10B104KB8NNNC‎" H 9800 2650 50  0001 C CNN "Manufacturer_Part_Number"
	1    9800 2650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9800 2400 9700 2400
Wire Wire Line
	9800 2400 9900 2400
Connection ~ 9800 2400
Wire Wire Line
	9800 2800 9300 2800
Wire Wire Line
	9800 2550 9800 2400
Wire Wire Line
	9800 2750 9800 2800
$Comp
L Connector:Conn_01x16_Female J3
U 1 1 60866815
P 6250 2750
F 0 "J3" H 6278 2726 50  0000 L CNN
F 1 "LCD" H 6278 2635 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x16_P2.54mm_Vertical" H 6250 2750 50  0001 C CNN
F 3 "~" H 6250 2750 50  0001 C CNN
F 4 "Adam Tech" H 6250 2750 50  0001 C CNN "Manufacturer_Name"
F 5 "PH1-16-UA‎" H 6250 2750 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "Connector Header Through Hole 16 position 0.100\" (2.54mm)" H 6250 2750 50  0001 C CNN "Description"
F 7 "Vertical" H 6250 2750 50  0001 C CNN "Package"
	1    6250 2750
	1    0    0    -1  
$EndComp
Text Label 6050 2150 2    50   ~ 0
+5vDIG
Text GLabel 6050 2250 0    50   Input ~ 0
V0
Text GLabel 6050 2350 0    50   Input ~ 0
RS
Text GLabel 6050 2550 0    50   Input ~ 0
E
NoConn ~ 6050 2650
NoConn ~ 6050 2750
NoConn ~ 6050 2850
NoConn ~ 6050 2950
Text GLabel 6050 3050 0    50   Input ~ 0
D4
Text GLabel 6050 3150 0    50   Input ~ 0
D5
Text GLabel 6050 3250 0    50   Input ~ 0
D6
Text GLabel 6050 3350 0    50   Input ~ 0
D7
$Comp
L power:GNDD #PWR014
U 1 1 608B92B8
P 6050 3550
F 0 "#PWR014" H 6050 3300 50  0001 C CNN
F 1 "GNDD" H 6054 3395 50  0000 C CNN
F 2 "" H 6050 3550 50  0001 C CNN
F 3 "" H 6050 3550 50  0001 C CNN
	1    6050 3550
	1    0    0    -1  
$EndComp
Text Label 5900 3450 2    50   ~ 0
+5vDIG
Wire Wire Line
	5900 3450 6050 3450
Wire Wire Line
	6050 2050 5750 2050
Wire Wire Line
	5650 2050 5650 2200
$Comp
L power:GNDD #PWR013
U 1 1 608FDE90
P 5650 2200
F 0 "#PWR013" H 5650 1950 50  0001 C CNN
F 1 "GNDD" H 5654 2045 50  0000 C CNN
F 2 "" H 5650 2200 50  0001 C CNN
F 3 "" H 5650 2200 50  0001 C CNN
	1    5650 2200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 60920C76
P 10400 1050
F 0 "#FLG03" H 10400 1125 50  0001 C CNN
F 1 "PWR_FLAG" H 10400 1223 50  0000 C CNN
F 2 "" H 10400 1050 50  0001 C CNN
F 3 "~" H 10400 1050 50  0001 C CNN
	1    10400 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 1050 10400 1150
Text Label 10700 1150 2    50   ~ 0
+5vDIG
Wire Wire Line
	10400 1150 10700 1150
$Comp
L power:PWR_FLAG #FLG02
U 1 1 60965AA5
P 9950 1450
F 0 "#FLG02" H 9950 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 9950 1623 50  0000 C CNN
F 2 "" H 9950 1450 50  0001 C CNN
F 3 "~" H 9950 1450 50  0001 C CNN
	1    9950 1450
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG04
U 1 1 60965AAB
P 10400 1450
F 0 "#FLG04" H 10400 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 10400 1623 50  0000 C CNN
F 2 "" H 10400 1450 50  0001 C CNN
F 3 "~" H 10400 1450 50  0001 C CNN
	1    10400 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 1450 9950 1550
Wire Wire Line
	10400 1450 10400 1550
Wire Wire Line
	10400 1550 10700 1550
Text Label 10300 1550 2    50   ~ 0
+12vFAN
Wire Wire Line
	9950 1550 10300 1550
$Comp
L power:GNDD #PWR021
U 1 1 60987269
P 10700 1550
F 0 "#PWR021" H 10700 1300 50  0001 C CNN
F 1 "GNDD" H 10704 1395 50  0000 C CNN
F 2 "" H 10700 1550 50  0001 C CNN
F 3 "" H 10700 1550 50  0001 C CNN
	1    10700 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 4350 4850 4350
Wire Wire Line
	4850 4350 4850 4000
Connection ~ 4300 4350
Wire Wire Line
	4300 4350 4300 4400
$Comp
L power:PWR_FLAG #FLG01
U 1 1 60999869
P 4850 4000
F 0 "#FLG01" H 4850 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 4850 4173 50  0000 C CNN
F 2 "" H 4850 4000 50  0001 C CNN
F 3 "~" H 4850 4000 50  0001 C CNN
	1    4850 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C4
U 1 1 609D1171
P 950 2950
F 0 "C4" H 1200 3000 50  0000 R CNN
F 1 "10u" H 1200 2900 50  0000 R CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-2012-12_Kemet-R_Pad1.30x1.05mm_HandSolder" H 950 2950 50  0001 C CNN
F 3 "~" H 950 2950 50  0001 C CNN
F 4 "2012-12" H 950 2950 50  0001 C CNN "Package"
F 5 "10µF Molded Tantalum Capacitors 10V 0805 (2012 Metric) 6Ohm" H 950 2950 50  0001 C CNN "Description"
F 6 "AVX Corporation" H 950 2950 50  0001 C CNN "Manufacturer_Name"
F 7 "TAJP106M010RNJ" H 950 2950 50  0001 C CNN "Manufacturer_Part_Number"
	1    950  2950
	-1   0    0    -1  
$EndComp
$Comp
L Power_Protection:USBLC6-2SC6 U2
U 1 1 60A4D2CA
P 3200 1200
F 0 "U2" H 3600 900 50  0000 C CNN
F 1 "USBLC6-2SC6" H 3750 800 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 3200 700 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/06/1d/48/9c/6c/20/4a/b2/CD00050750.pdf/files/CD00050750.pdf/jcr:content/translations/en.CD00050750.pdf" H 3400 1550 50  0001 C CNN
F 4 "STMicroelectronics" H 3200 1200 50  0001 C CNN "Manufacturer_Name"
F 5 "‎USBLC6-2SC6‎" H 3200 1200 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "SOT-23-6" H 3200 1200 50  0001 C CNN "Package"
F 7 "17V Clamp 5A (8/20µs) Ipp Tvs Diode Surface Mount SOT-23-6" H 3200 1200 50  0001 C CNN "Description"
	1    3200 1200
	1    0    0    -1  
$EndComp
Text GLabel 1600 1150 2    50   Input ~ 0
USB_COND+
Text GLabel 1600 1250 2    50   Input ~ 0
USB_COND-
$Comp
L power:GNDD #PWR07
U 1 1 60AC877B
P 3200 1600
F 0 "#PWR07" H 3200 1350 50  0001 C CNN
F 1 "GNDD" H 3204 1445 50  0000 C CNN
F 2 "" H 3200 1600 50  0001 C CNN
F 3 "" H 3200 1600 50  0001 C CNN
	1    3200 1600
	1    0    0    -1  
$EndComp
Text Label 3350 700  2    50   ~ 0
+5vDIG
Wire Wire Line
	3350 700  3200 700 
Wire Wire Line
	3200 700  3200 800 
Text GLabel 2800 1300 0    50   Input ~ 0
USB_COND-
Text GLabel 2800 1100 0    50   Input ~ 0
USB_D-
Text GLabel 3600 1300 2    50   Input ~ 0
USB_COND+
Text GLabel 3600 1100 2    50   Input ~ 0
USB_D+
Wire Wire Line
	6050 2450 5750 2450
Wire Wire Line
	5750 2450 5750 2050
Connection ~ 5750 2050
Wire Wire Line
	5750 2050 5650 2050
Wire Wire Line
	7400 1600 7300 1600
Wire Wire Line
	7300 1600 7300 1650
Wire Wire Line
	7400 1700 7300 1700
Wire Wire Line
	7050 1650 7300 1650
Wire Wire Line
	7050 1650 7050 1950
Text GLabel 3250 5500 0    50   Input ~ 0
#OCP
Wire Wire Line
	3250 5500 3400 5500
Text GLabel 7400 1900 0    50   Input ~ 0
#OCP
NoConn ~ 3400 5400
Connection ~ 7300 1650
Wire Wire Line
	7300 1650 7300 1700
Text GLabel 7400 1800 0    50   Input ~ 0
#RVP
Text GLabel 3250 5900 0    50   Input ~ 0
#RVP
Wire Wire Line
	3400 5900 3250 5900
$EndSCHEMATC
